PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Таблица: service_messages
CREATE TABLE IF NOT EXISTS service_messages (
    id              INTEGER   PRIMARY KEY AUTOINCREMENT,
    event_date_time TIMESTAMP DEFAULT (datetime('now', 'localtime') )
                              NOT NULL,
    message_type    TEXT,
    description     TEXT
);

COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
