--
-- Файл сгенерирован с помощью SQLiteStudio v3.1.1 в Вс июн 10 19:47:21 2018
--
-- Использованная кодировка текста: System
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Таблица: access_groups

INSERT INTO access_groups (id, number_of_order, number_of_access_group, order_date, last_test_date, next_test_date, director_id, person_id, description) VALUES (1, '34df', 'V', '2020-07-01', '2019-07-20', '2017-07-26', 5, 4, 'первый');
INSERT INTO access_groups (id, number_of_order, number_of_access_group, order_date, last_test_date, next_test_date, director_id, person_id, description) VALUES (2, 'dfgdfg', 'V', '2020-07-14', '2019-07-23', '2017-07-08', 5, 4, 'ураааа');
INSERT INTO access_groups (id, number_of_order, number_of_access_group, order_date, last_test_date, next_test_date, director_id, person_id, description) VALUES (3, '34 о', 'VI', '2020-07-21', '2019-07-15', '2017-07-29', 1, 4, 'тест');
INSERT INTO access_groups (id, number_of_order, number_of_access_group, order_date, last_test_date, next_test_date, director_id, person_id, description) VALUES (5, 'ffff', 'V', '2020-07-14', '2019-07-01', '2019-07-01', 1, 1, NULL);
INSERT INTO access_groups (id, number_of_order, number_of_access_group, order_date, last_test_date, next_test_date, director_id, person_id, description) VALUES (7, '344', 'VI', '2020-07-14', '2019-06-29', '2017-07-01', 4, 5, 'ура! случилось');

-- Таблица: document_status

INSERT INTO document_status (id, name, description) VALUES (1, 'plan', 'Запланировано');
INSERT INTO document_status (id, name, description) VALUES (2, 'active', 'В работе');
INSERT INTO document_status (id, name, description) VALUES (3, 'completed', 'Завершено');
INSERT INTO document_status (id, name, description) VALUES (4, 'canceled', 'Отменено');

-- Таблица: electrical_equipments

INSERT INTO electrical_equipments (id, inventory_number, electrical_equipment_type_id, name, el_install_id, vt_id, description) VALUES (1, '000', 1, 'ячейка 15', 4, 2, 'прим6');
INSERT INTO electrical_equipments (id, inventory_number, electrical_equipment_type_id, name, el_install_id, vt_id, description) VALUES (2, '001', 2, 'фку-15', 1, 1, 'фкушачка');
INSERT INTO electrical_equipments (id, inventory_number, electrical_equipment_type_id, name, el_install_id, vt_id, description) VALUES (3, '002', 3, 'Ячейка 8', 3, 3, 'Описание');
INSERT INTO electrical_equipments (id, inventory_number, electrical_equipment_type_id, name, el_install_id, vt_id, description) VALUES (4, '003', 1, 'ячейка 23', 2, 3, NULL);
INSERT INTO electrical_equipments (id, inventory_number, electrical_equipment_type_id, name, el_install_id, vt_id, description) VALUES (5, '004', 2, 'электОб2', 2, 3, NULL);
INSERT INTO electrical_equipments (id, inventory_number, electrical_equipment_type_id, name, el_install_id, vt_id, description) VALUES (6, '005', 3, '1Т', 4, 3, NULL);
INSERT INTO electrical_equipments (id, inventory_number, electrical_equipment_type_id, name, el_install_id, vt_id, description) VALUES (7, '006', 1, '2Т', 4, 3, NULL);

-- Таблица: electrical_equipment_types

INSERT INTO electrical_equipment_types (id, name, description) VALUES (1, 'Скат-T50', 'Блок высоковльтный');
INSERT INTO electrical_equipment_types (id, name, description) VALUES (2, 'ПН-20', 'Устройство испытательное');
INSERT INTO electrical_equipment_types (id, name, description) VALUES (3, 'АВИМ-65', 'Аппарат испытания жидких диэлектриков до 70кВ');
-- Таблица: electrical_installations

INSERT INTO electrical_installations (id, inventory_number, name, vt_id, shift_id, substat_id, description) VALUES (1, NULL, 'тп-99', 3, NULL, 6, 'прим6');
INSERT INTO electrical_installations (id, inventory_number, name, vt_id, shift_id, substat_id, description) VALUES (2, NULL, 'ТП-201', 3, NULL, 2, NULL);
INSERT INTO electrical_installations (id, inventory_number, name, vt_id, shift_id, substat_id, description) VALUES (3, NULL, 'РП-45', 3, NULL, 2, 'нет');
INSERT INTO electrical_installations (id, inventory_number, name, vt_id, shift_id, substat_id, description) VALUES (4, NULL, 'ТП-12', 2, NULL, 6, NULL);

-- Таблица: extinguisher_doc_exting

INSERT INTO extinguisher_doc_exting (id, ext_doc_id, ext_id, last_test, next_test, status, description) VALUES (1, 1, 2, '2008-06-13', '2008-06-30', 'fsdfdsf', 'dfffffff');
INSERT INTO extinguisher_doc_exting (id, ext_doc_id, ext_id, last_test, next_test, status, description) VALUES (2, 1, 3, '2008-08-13', '2008-09-13', 'ffffffffffffffff', 'eeeeeeeeeeeeee');
INSERT INTO extinguisher_doc_exting (id, ext_doc_id, ext_id, last_test, next_test, status, description) VALUES (3, 2, 2, '2018-08-13', '2018-09-13', 'статус', 'описание');

-- Таблица: extinguisher_docs

INSERT INTO extinguisher_docs (id, extinguisher_doc_date_time, description) VALUES (1, '2007-01-24 00:00:00', 'hfp');
INSERT INTO extinguisher_docs (id, extinguisher_doc_date_time, description) VALUES (2, '2017-01-24 00:00:00', 'семнадцЫть');

-- Таблица: extinguisher_types

INSERT INTO extinguisher_types (id, name, description) VALUES (1, 'ОУ-5', 'пятилитровый');
INSERT INTO extinguisher_types (id, name, description) VALUES (2, 'ОУ-3', 'трехлитровый');
INSERT INTO extinguisher_types (id, name, description) VALUES (3, 'ОУ-10', 'ДесЯтилитровый');

-- Таблица: extinguishers

INSERT INTO extinguishers (id, inventory_number, ext_type_id, el_install_id) VALUES (1, '5566dfg', 1, 1);
INSERT INTO extinguishers (id, inventory_number, ext_type_id, el_install_id) VALUES (2, '653рп', 3, 2);
INSERT INTO extinguishers (id, inventory_number, ext_type_id, el_install_id) VALUES (3, '5563332211', 3, 4);

-- Таблица: ipm_doc_ipm

INSERT INTO ipm_doc_ipm (id, ipm_doc_id, ipm_id, last_test, next_test, status, description) VALUES (1, 1, 1, NULL, '2018-05-16', 'изьято', NULL);
INSERT INTO ipm_doc_ipm (id, ipm_doc_id, ipm_id, last_test, next_test, status, description) VALUES (2, 2, 1, '2018-05-16', '2018-05-25', 'x3gu', NULL);
INSERT INTO ipm_doc_ipm (id, ipm_doc_id, ipm_id, last_test, next_test, status, description) VALUES (3, 2, 2, '2018-05-14', '2018-05-25', 'p85t', NULL);
INSERT INTO ipm_doc_ipm (id, ipm_doc_id, ipm_id, last_test, next_test, status, description) VALUES (4, 1, 2, NULL, '2018-05-14', 'брак', NULL);
INSERT INTO ipm_doc_ipm (id, ipm_doc_id, ipm_id, last_test, next_test, status, description) VALUES (5, 2, 7, '2018-05-02', '2018-05-25', 'rixc', NULL);
INSERT INTO ipm_doc_ipm (id, ipm_doc_id, ipm_id, last_test, next_test, status, description) VALUES (22, 1, 7, NULL, '2018-05-02', 'изъято', NULL);
INSERT INTO ipm_doc_ipm (id, ipm_doc_id, ipm_id, last_test, next_test, status, description) VALUES (23, 2, 3, NULL, '2018-04-01', 'шутка', NULL);
INSERT INTO ipm_doc_ipm (id, ipm_doc_id, ipm_id, last_test, next_test, status, description) VALUES (30, 11, 1, '2017-12-11', '2017-12-11', 'Брак', 'брак');
INSERT INTO ipm_doc_ipm (id, ipm_doc_id, ipm_id, last_test, next_test, status, description) VALUES (31, 11, 7, '2017-06-17', '2017-12-06', 'Годен', 'годен');
INSERT INTO ipm_doc_ipm (id, ipm_doc_id, ipm_id, last_test, next_test, status, description) VALUES (43, 13, 2, '2018-12-19', '2019-06-07', 'Изьято', '');
INSERT INTO ipm_doc_ipm (id, ipm_doc_id, ipm_id, last_test, next_test, status, description) VALUES (44, 14, 2, '2018-12-19', '2019-01-01', 'Годен', 'годен!!!!!');
INSERT INTO ipm_doc_ipm (id, ipm_doc_id, ipm_id, last_test, next_test, status, description) VALUES (45, 13, 7, '2018-05-25', '2018-06-09', 'Годен', 'шуперпупер');
INSERT INTO ipm_doc_ipm (id, ipm_doc_id, ipm_id, last_test, next_test, status, description) VALUES (47, 14, 1, '2018-05-25', NULL, 'Изьят', 'изЪятиускис!');
INSERT INTO ipm_doc_ipm (id, ipm_doc_id, ipm_id, last_test, next_test, status, description) VALUES (48, 15, 8, NULL, '2018-06-24', 'Годен', 'новые перчи');

-- Таблица: ipm_docs

INSERT INTO ipm_docs (id, ipm_date_time, description) VALUES (1, '2018-01-17 17:29:26', 'мухахах');
INSERT INTO ipm_docs (id, ipm_date_time, description) VALUES (2, '2018-05-17 20:29:27', 'хахахму');
INSERT INTO ipm_docs (id, ipm_date_time, description) VALUES (13, '2018-06-06 16:27:30', NULL);
INSERT INTO ipm_docs (id, ipm_date_time, description) VALUES (14, '2018-06-06 16:27:51', '3300012 годен до 19,01,01');
INSERT INTO ipm_docs (id, ipm_date_time, description) VALUES (15, '2018-06-08 17:37:01', 'вот такой документище');

-- Таблица: ipm_types

INSERT INTO ipm_types (id, name, description) VALUES (1, 'Перчатки', NULL);
INSERT INTO ipm_types (id, name, description) VALUES (2, 'УВН', NULL);

-- Таблица: ipms

INSERT INTO ipms (id, inventory_number, ipm_type_id, vt_id, el_install_id) VALUES (1, '55562', 2, 1, 1);
INSERT INTO ipms (id, inventory_number, ipm_type_id, vt_id, el_install_id) VALUES (2, '3300012', 1, 2, 4);
INSERT INTO ipms (id, inventory_number, ipm_type_id, vt_id, el_install_id) VALUES (3, '500005', 2, 3, 3);
INSERT INTO ipms (id, inventory_number, ipm_type_id, vt_id, el_install_id) VALUES (7, '000000', 2, 2, 4);
INSERT INTO ipms (id, inventory_number, ipm_type_id, vt_id, el_install_id) VALUES (8, '19925', 1, 1, 1);

-- Таблица: order_doc_electrical_equipment

INSERT INTO order_doc_electrical_equipment (id, order_doc_id, ee_id, additional) VALUES (1, 1, 2, 'верная');
INSERT INTO order_doc_electrical_equipment (id, order_doc_id, ee_id, additional) VALUES (2, 1, 1, 'ошибочная');
INSERT INTO order_doc_electrical_equipment (id, order_doc_id, ee_id, additional) VALUES (3, 1, 3, 'верная 2');

-- Таблица: order_doc_person

INSERT INTO order_doc_person (id, order_doc_id, status_id, person_id, additional) VALUES (42, 40, 1, 4, NULL);
INSERT INTO order_doc_person (id, order_doc_id, status_id, person_id, additional) VALUES (43, 40, 2, 5, NULL);
INSERT INTO order_doc_person (id, order_doc_id, status_id, person_id, additional) VALUES (44, 40, 3, 5, NULL);
INSERT INTO order_doc_person (id, order_doc_id, status_id, person_id, additional) VALUES (45, 40, 5, 5, NULL);
INSERT INTO order_doc_person (id, order_doc_id, status_id, person_id, additional) VALUES (46, 40, 4, 2, NULL);
INSERT INTO order_doc_person (id, order_doc_id, status_id, person_id, additional) VALUES (47, 40, 6, 4, 'тест');
INSERT INTO order_doc_person (id, order_doc_id, status_id, person_id, additional) VALUES (48, 40, 6, 5, 'текст');
INSERT INTO order_doc_person (id, order_doc_id, status_id, person_id, additional) VALUES (49, 41, 1, 1, NULL);
INSERT INTO order_doc_person (id, order_doc_id, status_id, person_id, additional) VALUES (50, 41, 2, 1, NULL);
INSERT INTO order_doc_person (id, order_doc_id, status_id, person_id, additional) VALUES (51, 41, 3, 2, NULL);
INSERT INTO order_doc_person (id, order_doc_id, status_id, person_id, additional) VALUES (52, 41, 5, 4, NULL);
INSERT INTO order_doc_person (id, order_doc_id, status_id, person_id, additional) VALUES (53, 41, 4, 5, NULL);
INSERT INTO order_doc_person (id, order_doc_id, status_id, person_id, additional) VALUES (54, 41, 6, 5, 'петррррро');

-- Таблица: order_doc_person_status

INSERT INTO order_doc_person_status (id, status, description) VALUES (1, 'Выдающий наряд', NULL);
INSERT INTO order_doc_person_status (id, status, description) VALUES (2, 'Ответственный руководитель работ', NULL);
INSERT INTO order_doc_person_status (id, status, description) VALUES (3, 'Допускающий', NULL);
INSERT INTO order_doc_person_status (id, status, description) VALUES (4, 'Наблюдающий', NULL);
INSERT INTO order_doc_person_status (id, status, description) VALUES (5, 'Производитель работ', NULL);
INSERT INTO order_doc_person_status (id, status, description) VALUES (6, 'Член бригады', NULL);

-- Таблица: order_docs

INSERT INTO order_docs (id, number, subdiv_id, substat_id, date_time_of_issue, date_time_start, date_time_end, instruct_to, description, status_id) VALUES (40, '3558', 3, 2, '2018-02-07 00:00:00', '2018-02-22 00:00:00', '2018-02-28 00:00:00', 'направление', 'описание', 3);

-- Таблица: organizations

INSERT INTO organizations (id, name, description) VALUES (1, 'ЯмалСтройАгроМех', 'далеко и холодно');
INSERT INTO organizations (id, name, description) VALUES (2, 'ТехКомплектМонтаж', NULL);
INSERT INTO organizations (id, name, description) VALUES (3, 'ТюменьКомплектМонтаж', 'рядом и удобно');

-- Таблица: persons

INSERT INTO persons (id, personnel_number, surname, name, middle_name, birthday_date, hire_date, dismissal_date, position_id, phone, salary, subdiv_id, shift_id, description) VALUES (1, 435, 'Иванов', 'Иван', 'Иванович', '1989-06-05', '2017-05-31', '2017-07-01', 2, '89224568962', 0, 2, NULL, NULL);
INSERT INTO persons (id, personnel_number, surname, name, middle_name, birthday_date, hire_date, dismissal_date, position_id, phone, salary, subdiv_id, shift_id, description) VALUES (2, 3652, 'Сидоренко', 'Степан', 'Станиславович', '1993-07-09', '2017-07-08', '2017-07-30', 1, '46-28-62', 0, 1, NULL, NULL);
INSERT INTO persons (id, personnel_number, surname, name, middle_name, birthday_date, hire_date, dismissal_date, position_id, phone, salary, subdiv_id, shift_id, description) VALUES (4, 266332, 'Захарченко', 'Владислав', 'Сергеев', '1992-06-18', '2017-07-14', '2017-07-22', 1, '92563541', 0, 2, NULL, NULL);
INSERT INTO persons (id, personnel_number, surname, name, middle_name, birthday_date, hire_date, dismissal_date, position_id, phone, salary, subdiv_id, shift_id, description) VALUES (5, 4444, 'Петров', 'Петр', 'Петрович', '1968-07-02', '2017-07-12', NULL, 2, '3665', 0, 2, NULL, NULL);

-- Таблица: positions

INSERT INTO positions (id, name, description) VALUES (1, 'ВАхтер', 'суперРемонтниты');
INSERT INTO positions (id, name, description) VALUES (2, 'Электриг', 'ВАХы');


-- Таблица: shifts

INSERT INTO shifts (id, name, description) VALUES (1, 'первая смена', 'уууух');
INSERT INTO shifts (id, name, description) VALUES (2, 'вторая смена', 'эээээ');
INSERT INTO shifts (id, name, description) VALUES (3, 'третья смена', 'пук');
INSERT INTO shifts (id, name, description) VALUES (4, 'четверая смена', 'бздык');

-- Таблица: subdivisions

INSERT INTO subdivisions (id, name, org_id, description) VALUES (1, 'Основное', 1, NULL);
INSERT INTO subdivisions (id, name, org_id, description) VALUES (2, 'Техническое', 1, NULL);
INSERT INTO subdivisions (id, name, org_id, description) VALUES (3, 'Административное', 2, NULL);
INSERT INTO subdivisions (id, name, org_id, description) VALUES (4, 'Хозяйственное', 1, NULL);

-- Таблица: substations

INSERT INTO substations (id, name, org_id, description) VALUES (1, 'Южная', 1, NULL);
INSERT INTO substations (id, name, org_id, description) VALUES (2, 'Северная', 3, NULL);
INSERT INTO substations (id, name, org_id, description) VALUES (6, 'Транспортная', 3, NULL);

-- Таблица: voltage_types

INSERT INTO voltage_types (id, name, description) VALUES (1, '0.4 кВ.', ' нульЧетыреКиловольта');
INSERT INTO voltage_types (id, name, description) VALUES (2, '330 кВ.', 'ух как мосчно');
INSERT INTO voltage_types (id, name, description) VALUES (3, '10 кВ.', 'мощненько');

COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
