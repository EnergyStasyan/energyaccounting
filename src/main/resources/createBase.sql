--
-- Файл сгенерирован с помощью SQLiteStudio v3.1.1 в Вс июн 10 19:44:43 2018
--
-- Использованная кодировка текста: System
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

CREATE TABLE IF NOT EXISTS migration_history(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    major_version TEXT,
    minor_version TEXT,
    file_number TEXT,
    comment TEXT,
    date_applied DATE
);


-- Таблица: access_groups
CREATE TABLE IF NOT EXISTS access_groups (
    id                     INTEGER PRIMARY KEY AUTOINCREMENT,
    number_of_order        TEXT,
    number_of_access_group TEXT,
    order_date             DATE,
    last_test_date         DATE,
    next_test_date         DATE,
    director_id            INTEGER REFERENCES persons (id) ON DELETE SET NULL
                                                           ON UPDATE CASCADE,
    person_id              INTEGER REFERENCES persons (id) ON DELETE SET NULL
                                                           ON UPDATE CASCADE,
    description            TEXT
);


-- Таблица: document_status
CREATE TABLE IF NOT EXISTS document_status (
    id          INTEGER PRIMARY KEY AUTOINCREMENT,
    name        TEXT,
    description TEXT
);


-- Таблица: electrical_equipments
CREATE TABLE IF NOT EXISTS electrical_equipments (
    id               INTEGER PRIMARY KEY AUTOINCREMENT,
    inventory_number TEXT,
    name             TEXT    NOT NULL,
    electrical_equipment_type_id      INTEGER REFERENCES electrical_equipment_types (id) ON DELETE SET NULL
                                                           ON UPDATE CASCADE,
    el_install_id       INTEGER NOT NULL
                             REFERENCES electrical_installations (id) ON DELETE SET NULL
                                                                      ON UPDATE CASCADE,
    vt_id            INTEGER REFERENCES voltage_types (id) ON DELETE SET NULL
                                                           ON UPDATE CASCADE,
    description      TEXT
);

-- Таблица: electrical_equipment_types
CREATE TABLE IF NOT EXISTS electrical_equipment_types (
    id          INTEGER PRIMARY KEY AUTOINCREMENT,
    name        TEXT,
    description TEXT
);

-- Таблица: electrical_equipment_docs
CREATE TABLE IF NOT EXISTS electrical_equipment_docs (
    id            INTEGER  PRIMARY KEY AUTOINCREMENT,
    electrical_equipment_date_time DATETIME NOT NULL,
    description   TEXT
);

-- Таблица: electrical_equipment_doc_electrical_equipment
CREATE TABLE IF NOT EXISTS electrical_equipment_doc_electrical_equipment (
    id          INTEGER PRIMARY KEY AUTOINCREMENT,
    electrical_equipment_doc_id  INTEGER REFERENCES electrical_equipment_docs (id) ON DELETE CASCADE
                                                 ON UPDATE CASCADE,
    electrical_equipment_id      INTEGER REFERENCES electrical_equipments (id) ON DELETE NO ACTION
                                             ON UPDATE CASCADE,
    last_test   DATE,
    next_test   DATE,
    status      TEXT,
    description TEXT
);

-- Таблица: electrical_installations
CREATE TABLE IF NOT EXISTS electrical_installations (
    id               INTEGER PRIMARY KEY AUTOINCREMENT
                             NOT NULL,
    inventory_number TEXT,
    name             TEXT    NOT NULL,
    vt_id            INTEGER REFERENCES voltage_types (id) ON DELETE SET NULL
                                                           ON UPDATE CASCADE,
    shift_id         INTEGER REFERENCES shifts (id) ON DELETE SET NULL
                                                    ON UPDATE CASCADE,
    substat_id       INTEGER REFERENCES substations (id) ON DELETE CASCADE
                                                         ON UPDATE CASCADE,
    description      TEXT
);


-- Таблица: extinguisher_doc_exting
CREATE TABLE IF NOT EXISTS extinguisher_doc_exting (
    id          INTEGER PRIMARY KEY AUTOINCREMENT,
    ext_doc_id  INTEGER REFERENCES extinguisher_docs (id) ON DELETE CASCADE
                                                          ON UPDATE CASCADE,
    ext_id      INTEGER REFERENCES extinguishers (id) ON DELETE NO ACTION
                                                      ON UPDATE CASCADE,
    last_test   DATE,
    next_test   DATE,
    status      TEXT,
    description TEXT
);


-- Таблица: extinguisher_docs
CREATE TABLE IF NOT EXISTS extinguisher_docs (
    id                         INTEGER  PRIMARY KEY AUTOINCREMENT,
    extinguisher_doc_date_time DATETIME NOT NULL,
    description                TEXT
);


-- Таблица: extinguisher_types
CREATE TABLE IF NOT EXISTS extinguisher_types (
    id          INTEGER PRIMARY KEY AUTOINCREMENT,
    name        TEXT,
    description TEXT
);


-- Таблица: extinguishers
CREATE TABLE IF NOT EXISTS extinguishers (
    id               INTEGER PRIMARY KEY AUTOINCREMENT,
    inventory_number TEXT,
    ext_type_id      INTEGER REFERENCES extinguisher_types (id),
    el_install_id       INTEGER REFERENCES electrical_installations (id) ON DELETE SET NULL
                                                                      ON UPDATE CASCADE
);


-- Таблица: ipm_doc_ipm
CREATE TABLE IF NOT EXISTS ipm_doc_ipm (
    id          INTEGER PRIMARY KEY AUTOINCREMENT,
    ipm_doc_id  INTEGER REFERENCES ipm_docs (id) ON DELETE CASCADE
                                                 ON UPDATE CASCADE,
    ipm_id      INTEGER REFERENCES ipms (id) ON DELETE NO ACTION
                                             ON UPDATE CASCADE,
    last_test   DATE,
    next_test   DATE,
    status      TEXT,
    description TEXT
);


-- Таблица: ipm_docs
CREATE TABLE IF NOT EXISTS ipm_docs (
    id            INTEGER  PRIMARY KEY AUTOINCREMENT,
    ipm_date_time DATETIME NOT NULL,
    description   TEXT
);


-- Таблица: ipm_types
CREATE TABLE IF NOT EXISTS ipm_types (
    id          INTEGER PRIMARY KEY AUTOINCREMENT,
    name        TEXT,
    description TEXT
);


-- Таблица: ipms
CREATE TABLE IF NOT EXISTS ipms (
    id               INTEGER PRIMARY KEY AUTOINCREMENT,
    inventory_number TEXT,
    ipm_type_id      INTEGER REFERENCES ipm_types (id) ON DELETE SET NULL
                                                       ON UPDATE CASCADE,
    vt_id            INTEGER REFERENCES voltage_types (id) ON DELETE SET NULL
                                                           ON UPDATE CASCADE,
    el_install_id       INTEGER REFERENCES electrical_installations (id) ON DELETE SET NULL
                                                                      ON UPDATE CASCADE
);

-- Таблица: order_doc_electrical_equipment
CREATE TABLE IF NOT EXISTS order_doc_electrical_equipment (
    id                   INTEGER PRIMARY KEY AUTOINCREMENT,
    order_doc_id         INTEGER REFERENCES order_docs (id) ON DELETE SET NULL
                                                            ON UPDATE CASCADE,
    ee_id INTEGER REFERENCES electrical_equipments (id) ON DELETE SET NULL
                                                                     ON UPDATE CASCADE,
    additional           TEXT
);


-- Таблица: order_doc_person
CREATE TABLE IF NOT EXISTS order_doc_person (
    id           INTEGER PRIMARY KEY AUTOINCREMENT,
    order_doc_id INTEGER REFERENCES order_docs (id) ON DELETE CASCADE
                                                    ON UPDATE CASCADE,
    status_id    INTEGER REFERENCES order_doc_person_status (id) ON DELETE SET NULL
                                                                 ON UPDATE CASCADE,
    person_id    INTEGER REFERENCES persons (id) ON DELETE SET NULL
                                                 ON UPDATE CASCADE,
    additional   TEXT
);


-- Таблица: order_doc_person_status
CREATE TABLE IF NOT EXISTS order_doc_person_status (
    id          INTEGER PRIMARY KEY AUTOINCREMENT,
    status      TEXT,
    description TEXT
);


-- Таблица: order_docs
CREATE TABLE IF NOT EXISTS order_docs (
    id                 INTEGER  PRIMARY KEY AUTOINCREMENT,
    number             TEXT,
    subdiv_id          INTEGER  REFERENCES subdivisions (id) ON DELETE SET NULL
                                                             ON UPDATE CASCADE,
    substat_id         INTEGER  REFERENCES substations (id) ON DELETE SET NULL
                                                            ON UPDATE CASCADE,
    date_time_of_issue DATETIME,
    date_time_start    DATETIME,
    date_time_end      DATETIME,
    instruct_to        TEXT,
    description        TEXT,
    status_id          INTEGER  REFERENCES document_status (id) ON UPDATE CASCADE
);


-- Таблица: organizations
CREATE TABLE IF NOT EXISTS organizations (
    id          INTEGER PRIMARY KEY AUTOINCREMENT
                        NOT NULL,
    name        TEXT,
    description TEXT
);


-- Таблица: persons
CREATE TABLE IF NOT EXISTS persons (
    id               INTEGER PRIMARY KEY AUTOINCREMENT,
    personnel_number INTEGER,
    surname          TEXT    NOT NULL,
    name             TEXT    NOT NULL,
    middle_name      TEXT,
    birthday_date    DATE,
    hire_date        DATE,
    dismissal_date   DATE,
    position_id      INTEGER REFERENCES positions (id) ON DELETE SET NULL
                                                       ON UPDATE CASCADE,
    phone            TEXT,
    salary           DOUBLE,
    subdiv_id        INTEGER REFERENCES subdivisions (id) ON DELETE SET NULL
                                                          ON UPDATE CASCADE,
    shift_id         INTEGER REFERENCES shifts (id) ON DELETE SET NULL
                                                    ON UPDATE CASCADE,
    description      TEXT
);


-- Таблица: positions
CREATE TABLE IF NOT EXISTS positions (
    id          INTEGER PRIMARY KEY AUTOINCREMENT,
    name        TEXT,
    description TEXT
);


-- Таблица: shifts
CREATE TABLE IF NOT EXISTS shifts (
    id          INTEGER PRIMARY KEY AUTOINCREMENT,
    name        TEXT,
    description TEXT
);


-- Таблица: subdivisions
CREATE TABLE IF NOT EXISTS subdivisions (
    id          INTEGER PRIMARY KEY AUTOINCREMENT,
    name        TEXT,
    org_id      INTEGER REFERENCES organizations (id) ON DELETE SET NULL
                                                      ON UPDATE CASCADE,
    description TEXT
);


-- Таблица: substations
CREATE TABLE IF NOT EXISTS substations (
    id          INTEGER PRIMARY KEY AUTOINCREMENT,
    name        TEXT,
    org_id      INTEGER REFERENCES organizations (id) ON DELETE SET NULL
                                                      ON UPDATE CASCADE,
    description TEXT
);


-- Таблица: voltage_types
CREATE TABLE IF NOT EXISTS voltage_types (
    id          INTEGER PRIMARY KEY AUTOINCREMENT,
    name        TEXT,
    description TEXT
);


-- Представление: access_groups_view
CREATE VIEW IF NOT EXISTS access_groups_view AS
    SELECT *,
           p_dir.surname || ' ' || p_dir.name || ' ' || p_dir.middle_name director,
           p.surname || ' ' || p.name || ' ' || p.middle_name person
      FROM access_groups a
           LEFT OUTER JOIN
           persons p_dir ON a.director_id = p_dir.id
           LEFT OUTER JOIN
           persons p ON a.person_id = p.id;


-- Представление: electrical_equipment_view
CREATE VIEW IF NOT EXISTS electrical_equipment_view AS
    SELECT ee.*,
           eet.name electricalEquipmentTypes,
           vt.name voltageType,
           ei.name electricalInstallations
      FROM electrical_equipments ee
           INNER JOIN
           electrical_installations ei ON ee.el_install_id = ei.id
           INNER JOIN
           electrical_equipment_types eet ON ee.electrical_equipment_type_id = eet.id
           LEFT OUTER JOIN
           voltage_types vt ON ee.vt_id = vt.id;


-- Представление: electrical_installations_view
CREATE VIEW IF NOT EXISTS electrical_installations_view AS
    SELECT e.*,
           v.name voltageType,
           s.name substation
      FROM electrical_installations e
           LEFT OUTER JOIN
           substations s ON e.substat_id = s.id
           LEFT OUTER JOIN
           voltage_types v ON e.vt_id = v.id;


-- Представление: extinguisher_doc_exting_view
CREATE VIEW IF NOT EXISTS extinguisher_doc_exting_view AS
    SELECT ede.id,
           ed.id doc_id,
           ede.ext_id,
           (ev.extType || ', ' || ev.electricalInstallation) AS name,
           ev.inventory_number,
           ede.last_test,
           ede.next_test,
           ede.status,
           ede.description
      FROM extinguisher_doc_exting ede
           INNER JOIN
           extinguisher_docs ed ON ed.id = ede.ext_doc_id
           INNER JOIN
           extinguishers_view ev ON ede.ext_id = ev.id;


-- Представление: extinguisher_from_doc_last_view
CREATE VIEW IF NOT EXISTS extinguisher_from_doc_last_view AS
    SELECT ed.id,
           MAX(ed.extinguisher_doc_date_time) last_rec,
           ede.ext_id,
           ede.last_test,
           ede.next_test,
           ede.status,
           ede.description
      FROM extinguisher_docs ed
           INNER JOIN
           extinguisher_doc_exting ede ON ede.ext_doc_id = ed.id
     GROUP BY ede.ext_id;


-- Представление: extinguisher_particular_view
CREATE VIEW IF NOT EXISTS extinguisher_particular_view AS
    SELECT e.id,
           e.inventory_number,
           efd.id doc_id,
           efd.last_rec,
           efd.last_test last_test_date,
           efd.next_test next_test_date,
           efd.status,
           efd.description,
           e.el_install_id,
           ei.name electricalInstallation,
           e.ext_type_id,
           et.name extType
      FROM extinguishers e
           LEFT OUTER JOIN
           electrical_installations ei ON e.el_install_id = ei.id
           INNER JOIN
           extinguisher_types et ON e.ext_type_id = et.id
           LEFT OUTER JOIN
           extinguisher_from_doc_last_view efd ON e.id = efd.ext_id;


-- Представление: extinguishers_view
CREATE VIEW IF NOT EXISTS extinguishers_view AS
    SELECT *
      FROM extinguisher_particular_view;


-- Представление: ipm_doc_ipm_view
CREATE VIEW IF NOT EXISTS ipm_doc_ipm_view AS
    SELECT ipm_di.id,
           ipm_di.ipm_doc_id doc_id,
           iv.id ipm_id,
           iv.inventory_number,
           (iv.electricalInstallation || ', ' || iv.ipmType || ', ' || iv.voltageType) AS name,
           ipm_di.last_test,
           ipm_di.next_test,
           ipm_di.status,
           ipm_di.description
      FROM ipm_doc_ipm ipm_di
           INNER JOIN
           ipm_docs ipm_d ON ipm_d.id = ipm_di.ipm_doc_id
           INNER JOIN
           ipms_view iv ON iv.id = ipm_di.ipm_id;


-- Представление: ipm_from_doc_last_view
CREATE VIEW IF NOT EXISTS ipm_from_doc_last_view AS
    SELECT ip.id,
           MAX(ip.ipm_date_time) last_rec,
           idi.ipm_id,
           idi.last_test,
           idi.next_test,
           idi.status,
           idi.description
      FROM ipm_docs ip
           INNER JOIN
           ipm_doc_ipm idi ON idi.ipm_doc_id = ip.id
     GROUP BY idi.ipm_id;

-- Представление: electrical_equipment_from_doc_last_view
CREATE VIEW IF NOT EXISTS electrical_equipment_from_doc_last_view AS
    SELECT eed.id,
           MAX(eed.electrical_equipment_date_time) last_rec,
           eedee.electrical_equipment_id,
           eedee.last_test,
           eedee.next_test,
           eedee.status,
           eedee.description
      FROM electrical_equipment_docs eed
           INNER JOIN
           electrical_equipment_doc_electrical_equipment eedee ON eedee.electrical_equipment_doc_id = eed.id
     GROUP BY eedee.electrical_equipment_id;

-- Представление: electrical_equipment_doc_electrical_equipment_view
CREATE VIEW IF NOT EXISTS electrical_equipment_doc_electrical_equipment_view AS
    SELECT ee_d_ee.id,
           ee_d_ee.electrical_equipment_doc_id doc_id,
           e_e_v.id electrical_equipment_id,
           e_e_v.inventory_number,
           (e_e_v.electricalInstallation || ', ' || e_e_v.electricalEquipmentType || ', ' || e_e_v.voltageType) AS name,
           ee_d_ee.last_test,
           ee_d_ee.next_test,
           ee_d_ee.status,
           ee_d_ee.description
      FROM electrical_equipment_doc_electrical_equipment ee_d_ee
           INNER JOIN
           electrical_equipment_docs ee_d ON ee_d.id = ee_d_ee.electrical_equipment_doc_id
           INNER JOIN
           electrical_equipments_view e_e_v ON e_e_v.id = ee_d_ee.electrical_equipment_id;



-- Представление: electrical_equipments_view
CREATE VIEW IF NOT EXISTS electrical_equipments_view AS
    SELECT *
      FROM electrical_equipments_particular_view;

-- Представление: electrical_equipments_particular_view
CREATE VIEW IF NOT EXISTS electrical_equipments_particular_view AS
    SELECT ee.id,
           ee.name,
           ee.inventory_number,
           eed.id doc_id,
           eed.last_rec,
           eed.last_test last_test_date,
           eed.next_test next_test_date,
           eed.status,
           eed.description,
           ee.el_install_id,
           ei.name electricalInstallation,
           ee.vt_id,
           vt.name voltageType,
           ee.electrical_equipment_type_id,
           eet.name electricalEquipmentType
      FROM electrical_equipments ee
           LEFT OUTER JOIN
           electrical_installations ei ON ee.el_install_id = ei.id
           INNER JOIN
           voltage_types vt ON ee.vt_id = vt.id
           INNER JOIN
           electrical_equipment_types eet ON ee.electrical_equipment_type_id = eet.id
           LEFT OUTER JOIN
           electrical_equipment_from_doc_last_view eed ON ee.id = eed.electrical_equipment_id;


-- Представление: ipms_view
CREATE VIEW IF NOT EXISTS ipms_view AS
    SELECT *
      FROM ipm_particular_view;

-- Представление: ipm_particular_view
CREATE VIEW IF NOT EXISTS ipm_particular_view AS
    SELECT i.id,
           i.inventory_number,
           ifd.id doc_id,
           ifd.last_rec,
           ifd.last_test last_test_date,
           ifd.next_test next_test_date,
           ifd.status,
           ifd.description,
           i.el_install_id,
           ei.name electricalInstallation,
           i.vt_id,
           vt.name voltageType,
           i.ipm_type_id,
           it.name ipmType
      FROM ipms i
           LEFT OUTER JOIN
           electrical_installations ei ON i.el_install_id = ei.id
           INNER JOIN
           voltage_types vt ON i.vt_id = vt.id
           INNER JOIN
           ipm_types it ON i.ipm_type_id = it.id
           LEFT OUTER JOIN
           ipm_from_doc_last_view ifd ON i.id = ifd.ipm_id;



-- Представление: mf_access_group_view
CREATE VIEW IF NOT EXISTS mf_access_group_view AS
    SELECT next_test_date dates,
           'Истекает группа допуска через ' || (strftime('%j', next_test_date) - strftime('%j', 'now') ) || ' дн.' type,
           person || ' ' || number_of_access_group || 'гр.' AS description
      FROM access_groups_view;


-- Представление: mf_extinguish_view
CREATE VIEW IF NOT EXISTS mf_extinguish_view AS
    SELECT ex.next_test_date dates,
           'Истекает срок огнетушителя через ' || (strftime('%j', next_test_date) - strftime('%j', 'now') ) || ' дн.' type,
           ex.electricalInstallation || ', ' || 'Тип: ' || ex.extType || ', ' || '№инв: ' || ex.inventory_number description
      FROM extinguishers_view ex;


-- Представление: mf_ipms_view
CREATE VIEW IF NOT EXISTS mf_ipms_view AS
    SELECT iv.next_test_date dates,
           'Истекает срок СИЗ через ' || (strftime('%j', next_test_date) - strftime('%j', 'now') ) || ' дн.' type,
           iv.electricalInstallation || ', ' || 'Тип: ' || iv.ipmType || ', ' || 'инв№: ' || iv.inventory_number description
      FROM ipms_view iv;

-- Представление: mf_electrical_equipments_view
CREATE VIEW IF NOT EXISTS mf_electrical_equipments_view AS
    SELECT eev.next_test_date dates,
           'Истекает срок эл.оборудования через ' || (strftime('%j', next_test_date) - strftime('%j', 'now') ) || ' дн.' type,
           eev.electricalInstallation || ', ' || 'Тип: ' || eev.electricalEquipmentType || ', ' || 'инв№: ' || eev.inventory_number description
      FROM electrical_equipments_view eev;

-- Представление: mf_order_doc_view
CREATE VIEW IF NOT EXISTS mf_order_doc_view AS
    SELECT date(date_time_of_issue) dates,
           'Незакрытый наряд ' || - (strftime('%j', date_time_of_issue) - strftime('%j', 'now') ) || ' дн.' type,
           '№' || number || ' c ' || strftime('%Y-%m-%d %H:%H', date_time_start) || ' по ' || strftime('%Y-%m-%d %H:%H', date_time_end) || ', поручается' || instruct_to AS description
      FROM order_docs_view
     WHERE status_id = 1 OR
           status_id = 2;


-- Представление: mf_person_view
CREATE VIEW IF NOT EXISTS mf_person_view AS
    SELECT date(strftime('%Y', 'now') || strftime('-%m-%d', p.birthday_date) ) dates,
           'День Рождения через ' || (strftime('%j', date(strftime('%Y', 'now') || strftime('-%m-%d', p.birthday_date) ) ) - strftime('%j', 'now') ) || ' дн.' type,
           p.surname || ' ' || p.name || ' ' || p.middle_name || ' ' || (strftime('%Y', 'now') - strftime('%Y', p.birthday_date) ) || ' лет' description
      FROM persons p
     WHERE dates BETWEEN DATE('now', '-5 day') AND DATE('now', '+14 day');


-- Представление: mf_view
CREATE VIEW IF NOT EXISTS mf_view (
    dates,
    type,
    description
)
AS
    SELECT *
      FROM (
               SELECT *
                 FROM mf_extinguish_view
               UNION
               SELECT *
                 FROM mf_access_group_view
               UNION
               SELECT *
                 FROM mf_ipms_view
               UNION
               SELECT *
                 FROM mf_electrical_equipments_view
               UNION
               SELECT *
                 FROM mf_order_doc_view
               UNION
               SELECT *
                 FROM mf_person_view
           )
     WHERE dates BETWEEN DATE('now', '-5 years') AND DATE('now', '+14 day')
     ORDER BY dates DESC;


-- Представление: order_doc_electrical_equipment_view
CREATE VIEW IF NOT EXISTS order_doc_electrical_equipment_view AS
    SELECT od.id,
           odee.order_doc_id,
           odee.additional
      FROM order_doc_electrical_equipment odee
           INNER JOIN
           order_docs od ON od.id = odee.order_doc_id;


-- Представление: order_doc_person_view
CREATE VIEW IF NOT EXISTS order_doc_person_view AS
    SELECT odp.id od_person_numb,
           od.id doc_id,
           od.number,
           p.id person_id,
           p.surname || ' ' || substr(p.name, 1, 1) || '.' || substr(p.middle_name, 1, 1) || '. ' || p.access_group || ' гр.' person,
           odp.additional,
           odps.id status_id,
           odps.status
      FROM order_doc_person odp
           INNER JOIN
           persons_view p ON odp.person_id = p.id
           INNER JOIN
           order_doc_person_status odps ON odps.id = odp.status_id
           INNER JOIN
           order_docs od ON od.id = odp.order_doc_id;


-- Представление: order_docs_view
CREATE VIEW IF NOT EXISTS order_docs_view AS
    SELECT od.id,
           od.number,
           od.date_time_of_issue,
           od.date_time_start,
           od.date_time_end,
           od.status_id,
           o.id org_id,
           o.name organization,
           sd.id subdiv_id,
           sd.name subdivision,
           ss.id substat_id,
           ss.name substation,
           od.instruct_to,
           od.description
      FROM order_docs od
           LEFT OUTER JOIN
           substations ss ON ss.id = od.substat_id
           LEFT OUTER JOIN
           subdivisions sd ON sd.id = od.subdiv_id
           LEFT OUTER JOIN
           organizations o ON sd.org_id = o.id;


-- Представление: persons_short_view
CREATE VIEW IF NOT EXISTS persons_short_view AS
    SELECT id,
           surname || ' ' || substr(name, 1, 1) || '.' || substr(middle_name, 1, 1) || '. ' || access_group || ' гр.' name
      FROM persons_view;


-- Представление: persons_view
CREATE VIEW IF NOT EXISTS persons_view AS
    SELECT od.number_of_access_group access_group,
           od.next_test_date,
           od.order_date,
           p.*,
           s.name subdivisions,
           pos.name position
      FROM persons p
           LEFT OUTER JOIN
           subdivisions s ON p.subdiv_id = s.id
           LEFT OUTER JOIN
           positions pos ON p.position_id = pos.id
           LEFT OUTER JOIN
           (
               SELECT person_id,
                      number_of_access_group,
                      next_test_date,
                      MAX(order_date) order_date
                 FROM access_groups
                GROUP BY person_id
                ORDER BY person_id
           )
           od ON od.person_id = p.id;


-- Представление: sub_ee_ei_view
CREATE VIEW IF NOT EXISTS sub_ee_ei_view AS
    SELECT s.id sub_id,
           s.name substation,
           ei.id ei_id,
           ei.name electrical_installation,
           ee.id ee_id,
           ee.name electrical_equipment
      FROM electrical_equipments ee
           INNER JOIN
           electrical_installations ei ON ee.el_install_id = ei.id
           INNER JOIN
           substations s ON s.id = ei.substat_id;


-- Представление: subdivisions_view
CREATE VIEW IF NOT EXISTS subdivisions_view AS
    SELECT s.*,
           o.name organization
      FROM subdivisions s
           LEFT OUTER JOIN
           organizations o ON s.org_id = o.id;


-- Представление: substations_view
CREATE VIEW IF NOT EXISTS substations_view AS
    SELECT s.*,
           o.name organization
      FROM substations s
           LEFT OUTER JOIN
           organizations o ON s.org_id = o.id;


COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
