package ru.com.energyaccounting.controller;

import javafx.fxml.FXML;
import javafx.stage.Stage;
import ru.com.energyaccounting.dao.PersistException;

/**
 * интерфейс опмсывает поведение контролеров AddOrEditInterface
 * Created by UserSts on 15.03.2017.
 */
public interface AddOrEditInterface<E> {

    boolean isOkClicked();
    boolean isInputValid();

    /**
     *EAddOrEditLayout.fxml -> E.class -> SQL

     */
    @FXML void handleOK() throws PersistException;

    @FXML void handleCancel() throws PersistException;

    /**
     *  для корректировки E.class -> ObjectAddOrEditLayout.fxml
     * @param object
     */
    void setObject(E object);

    void setDialogStage(Stage dialogStage);

    }
