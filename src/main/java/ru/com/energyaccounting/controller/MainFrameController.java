package ru.com.energyaccounting.controller;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.paint.Color;
import ru.com.energyaccounting.dao.*;
import ru.com.energyaccounting.model.MainFrame;

import java.net.URL;
import java.sql.Connection;
import java.time.LocalDate;
import java.util.ResourceBundle;
import java.util.logging.Logger;

/**
 * Created by UserSts on 12.02.2017.
 */
public class MainFrameController  implements Initializable{
    private static final Logger log = Logger.getLogger(MainFrameController.class.getName());

    //основная общая
    @FXML private TableView<MainFrame> mainFrameTableView;
    @FXML private TableColumn<MainFrame, LocalDate> mainFrameColumnDate;
    @FXML private TableColumn<MainFrame, String> mainFrameColumnType;
    @FXML private TableColumn<MainFrame, String> mainFrameColumnDescription;

    private DaoFactoryInterface<Connection> factory;
    private Connection connection;
    private GenericDao<MainFrame, Integer > dao;

    public MainFrameController() {
        try {
            factory = new DaoFactory();
            connection = factory.getContext();
            dao = factory.getDao(connection, Class.forName(MainFrame.class.getName()));
        }catch (PersistException | ClassNotFoundException e){
            ServiceMessagesController.addErrorServiceMessage("Добавь класс в конструктор DaoFactory()");

            e.printStackTrace();
        }
    }


    private void populateObjects(ObservableList<MainFrame> data){
        mainFrameTableView.setItems(data);
    }
    private ObservableList<MainFrame> getObjData(){ //
        return null;
    }

    public void searchObjects(){
        try{
            populateObjects(dao.getAll());
            log.info("получаем общие данные с бд: dao.getAll()");
        }catch (PersistException e){
            e.printStackTrace();
        }
    }
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        mainFrameColumnDate.setCellValueFactory(cellData -> cellData.getValue().dateProperty());
        mainFrameColumnDescription.setCellValueFactory(cellData -> cellData.getValue().descriptionProperty());
        mainFrameColumnType.setCellValueFactory(cellData -> cellData.getValue().typeProperty());

        mainFrameColumnDate.setCellFactory(column -> {
            return new TableCell<MainFrame, LocalDate>(){
                @Override
                protected void updateItem(LocalDate item, boolean empty) {
                    super.updateItem(item, empty);
                    if (item == null || empty) {
                        setText(null);
                        setStyle("");
                    }else {
                        setText(AbstractJDBCDao.dtfDate.format(item));
                        if (item.isBefore(LocalDate.now())) {  //after is future
                            setTextFill(Color.RED);
                            setStyle("");
                        }else if(item.isAfter(LocalDate.now())){ //before is last
                            setTextFill(Color.DARKGOLDENROD);
                            setStyle("");
                        }else {
                            setTextFill(Color.BLACK);
                            setStyle("");
                        }
                    }
                }
            };
        });
        searchObjects();
    }


    @FXML void handleRefresh(){
        searchObjects();
    }
}
