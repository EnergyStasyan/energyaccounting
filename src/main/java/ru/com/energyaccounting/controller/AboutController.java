package ru.com.energyaccounting.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import ru.com.energyaccounting.Main;
import ru.com.energyaccounting.model.StartChoice;
import ru.com.energyaccounting.util.AddressDBSingleton;
import ru.com.energyaccounting.util.PropertyUtils;

import java.net.URL;
import java.util.Properties;
import java.util.ResourceBundle;

public class AboutController implements Initializable {
    @FXML private Label poVersionLabel;
    @FXML private Label descriptionLabel;
    @FXML private Label pathLabel;
    @FXML private Label nameLabel;
    @FXML private Hyperlink siteHyperLink;
    @FXML private Hyperlink vkHyperLink;
    @FXML private ImageView logoImageView;
    private Stage stage;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Properties properties = PropertyUtils.readFromResourceProperties();
        StartChoice startChoice = AddressDBSingleton.getInstance().getStartChoice();
        String baseName = startChoice.getBaseName()!= null? startChoice.getBaseName() :"";
        String basePath = startChoice.getFilePath()!= null? startChoice.getFilePath() :"";
        String baseDescription = (startChoice.getDescription() != null? startChoice.getDescription() :"") ;
        poVersionLabel.setText(properties.getProperty("major.version"));
        descriptionLabel.setText(baseDescription);
        pathLabel.setText(basePath);
        nameLabel.setText(baseName);
        siteHyperLink.setText(properties.getProperty("site"));
        siteHyperLink.setOnAction((e)-> Main.getHostService().showDocument(siteHyperLink.getText()));
        vkHyperLink.setText(properties.getProperty("vk"));
        vkHyperLink.setOnAction((e)-> Main.getHostService().showDocument(vkHyperLink.getText()));
        logoImageView.setImage(new Image(Main.logo));

    }

    @FXML
    private void handleOK() {
        getStage().close();
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public Stage getStage() {
        return stage;
    }

}
