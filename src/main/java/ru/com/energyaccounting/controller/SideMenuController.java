package ru.com.energyaccounting.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import jiconfont.icons.GoogleMaterialDesignIcons;
import jiconfont.javafx.IconFontFX;
import jiconfont.javafx.IconNode;
import ru.com.energyaccounting.Main;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import static jiconfont.icons.GoogleMaterialDesignIcons.FEEDBACK;
import static jiconfont.icons.GoogleMaterialDesignIcons.PERM_CONTACT_CALENDAR;


public class SideMenuController {

    private static final Logger LOG = Logger.getLogger(SideMenuController.class.getName());
    @FXML private ResourceBundle resources;
    @FXML private URL location;
    @FXML private Label mainLabel;
    @FXML private Label personLabel;
    @FXML private Label electricalInstallationsLabel;
    @FXML private Label equipmentLabel;
    @FXML private Label IPMLabel;
    @FXML private Label extinguishersLabel;
    @FXML private Label newDocLabel;
    private RootController rootController;

    protected void setRootController(RootController rootController){
        this.rootController = rootController;
        setIcons(personLabel, PERM_CONTACT_CALENDAR);
        setIcons(electricalInstallationsLabel, FEEDBACK);
    }
   private void setIcons(Label label, GoogleMaterialDesignIcons ico){
        IconNode iconNode = new IconNode(ico);
        IconFontFX.register(GoogleMaterialDesignIcons.getIconFont());
        iconNode.setIconSize(16);
        iconNode.setFill(Color.BLACK);
        label.setGraphic(iconNode);
    }

    @FXML private void handleMainLabelClickMouse (){
       LOG.info("try click" );
        rootController.initBlankLayout(Main.mMainLayout );
        LOG.info("clicked" );
    }


    @FXML private void handlePersonLabelClickMouse(){
        LOG.info("try click" );
        rootController.initBlankLayout(Main.mPersonMenuLayout);
        LOG.info("clicked" );
    }

    @FXML private void handleElectricalInstallationsLabelClickMouse(){
        rootController.initBlankLayout(Main.mElectricalInstallationsMenuLayout);
    }

    @FXML private void handleElectricalEquipmentsLabelClickMouse(){
        rootController.initBlankLayout(Main.mElectricalEquipmentsMenuLayout);
    }

    @FXML private void handleElectricalEquipmentTypesLabelClickMouse(){
        rootController.initBlankLayout(Main.mElectricalEquipmentTypesMenuLayout);
    }

    @FXML private void  handleServiceMessagesLabelClickMouse(){
        rootController.initBlankLayout(Main.mServiceMessagesMenuLayout);
    }
    @FXML private void  handleSubdivisionLabelClickMouse(){
        rootController.initBlankLayout(Main.mSubdivisionsMenuLayout);
    }

    @FXML private void handleSubstationLabelClickMouse(){
        rootController.initBlankLayout(Main.mSubstationsMenuLayout);
    }

    @FXML private void handleOrganizationLabelClickMouse(){
        rootController.initBlankLayout(Main.mOrganizationsMenuLayout);
    }

    @FXML private void handleExtinguishersTypesLabelClickMouse(){
        rootController.initBlankLayout(Main.mExtinguisherTypeMenuLayout);
    }

    @FXML private void handleExtinguishersLabelClickMouse(){
        rootController.initBlankLayout(Main.mExtinguisherMenuLayout);
    }

    @FXML private void handleIMPsTypesLabelClickMouse(){
        rootController.initBlankLayout(Main.mIpmTypesMenuLayout);
    }

    @FXML private void handleIMPsLabelClickMouse(){
        rootController.initBlankLayout(Main.mIpmsMenuLayout);
    }

    @FXML private void handleShiftsLabelClickMouse(){
        rootController.initBlankLayout(Main.mShiftMenuLayout);
    }

    @FXML private void handleVoltageTypesLabelClickMouse(){
        rootController.initBlankLayout(Main.mVoltageTypeMenuLayout);
    }

    @FXML private void handlePositionsLabelClickMouse(){
        rootController.initBlankLayout(Main.mPositionMenuLayout);
    }

    @FXML private void handleOrderDocClickMouse(){
        rootController.initBlankLayout(Main.mOrderDocMenuLayout);
    }

    @FXML private void handleAccessGroupClickMouse(){
        rootController.initBlankLayout(Main.mAccessGroupMenuLayout);
    }
    @FXML private void handleIpmDocClickMouse(){
        rootController.initBlankLayout(Main.mIpmDocMenuLayout);
    }
    @FXML private void handleElectricalEquipmentDocClickMouse(){
        rootController.initBlankLayout(Main.mElectricalEquipmentDocMenuLayout);
    }
    @FXML private void handleElectricalEquipmentDocAddOrEditClickMouse(){
        rootController.initBlankLayout(Main.mElectricalEquipmentDocAddOrEditLayout);
    }

    @FXML private void handleExtinguisherDocClickMouse(){
        rootController.initBlankLayout(Main.mExtinguisherDocMenuLayout);
    }
}

