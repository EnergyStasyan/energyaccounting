package ru.com.energyaccounting.controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.LoadException;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import ru.com.energyaccounting.Main;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Logger;

/**
 * Created by UserSts on 05.02.2017.
 */
public class RootController {
    private static final Logger LOG = Logger.getLogger(RootController.class.getName());
    private static RootController rootController;
    private BorderPane rootLayout;
    private ArrayList<String> mExtensionsList = new ArrayList<>();
    private AnchorPane serviceMessageLayout;

    @FXML
    public void initialize() {}

    @FXML
    public void handleExit() {
        StartChoiceBaseController.getSecondaryStage().close();
    }

    @FXML
    public void handleOpen() {}

    @FXML
    public void handleHelp() {
        String layout = "fxml/AboutLayout.fxml";
        showModalLayout(layout);
    }

    //запускаем список методов, данный метод ВРЕМЕННЫЙ - ахахахах
    public void Go() {
        initRootLayout();                   //загружаем слой root
        showSideMenu();                     // загружаем боковое меню
        initBlankLayout(Main.mBlankLayout); //загружаем основное окно
        showServiceMessage();               // инициализируем сервисные сообщения
    }

    public BorderPane getRootLayout() {
        return rootLayout;
    }

    public void initRootLayout() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getClassLoader().getResource(Main.mRootLayout));
            rootLayout = loader.load();

            Scene scene = new Scene(rootLayout);
            StartChoiceBaseController.getSecondaryStage().setScene(scene);

            StartChoiceBaseController.getSecondaryStage().show();

        } catch (IOException e) {
            LOG.warning("Ощибка при инициализации слоя root + " + e.getMessage());
            ServiceMessagesController.addErrorServiceMessage(this.getClass().getSimpleName() + " : " + e.getMessage());
            e.printStackTrace();
        }
    }

    public void showSideMenu() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getClassLoader().getResource(Main.mSideMenuLayout));
            AnchorPane sideMenuLayout = loader.load();
            //помещаем боковое меню слева
            rootLayout.setLeft(sideMenuLayout);

            //Даем контролеру доступ к главному приложению
            //иначе контролер не видит и выдает NullPointerException
            SideMenuController sideMenuController = loader.getController(); //так сделано, потому что он всегда на экране
            sideMenuController.setRootController(this);
        } catch (IOException e) {
            ServiceMessagesController.addErrorServiceMessage(this.getClass().getSimpleName() + " : " + e.getMessage());
            e.printStackTrace();
        }
    }

    public void initBlankLayout(String strLayout) {
        LOG.info("Загрузка: " + strLayout);
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getClassLoader().getResource(strLayout));
            LOG.info("loader.setLoader loading was successful");
            AnchorPane anchorPane;
            try {
                anchorPane = loader.load();  //проверь конструктор, он должен быть пустым
                LOG.info("loader.load loading was successful");

                rootLayout.setCenter(anchorPane);
            } catch (LoadException e) {
                e.printStackTrace();
                LOG.warning("Ошибка загрузки Loader");
            }

        } catch (IOException e) {
            LOG.warning(e.getMessage());
            ServiceMessagesController.addErrorServiceMessage(this.getClass().getSimpleName() + " : " + e.getMessage());
            e.printStackTrace();

        } catch (Exception e) {
            LOG.warning(e.getMessage());
        }
    }


    private void showServiceMessage() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getClassLoader().getResource(Main.mServiceMessageLayout));
            serviceMessageLayout = loader.load();
            rootLayout.setBottom(serviceMessageLayout);

            ServiceMessagesController serviceMessagesController = loader.getController();
            serviceMessagesController.setRootController(this); //устанавливаем в экземпляр класса serviceMessagesController, что управляет им именно этот экземпляр RootController

        } catch (IOException e) {
            ServiceMessagesController.addErrorServiceMessage(this.getClass().getSimpleName() + " : " + e.getMessage());
            e.printStackTrace();
        }
    }


    public RootController getRootLayoutController() {
        return rootController;
    }

    protected void showModalLayout(String layout) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getClassLoader().getResource(layout));
            AnchorPane page = loader.load();

            //Создаем  окно
            Stage stage = new Stage();
            stage.setTitle("О программе");
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(StartChoiceBaseController.getSecondaryStage()); // получаем статическую primaryStage и указываем ее как хозяина
            Scene scene = new Scene(page);
            stage.setScene(scene);

            LOG.info("Пытаемся загрузить контролер редактирования объекта");
            AboutController controller = loader.getController();
            controller.setStage(stage);

            stage.getIcons().add(new Image(Main.ico));
            //показываем диалог и ждем закрытия
            stage.showAndWait();

        } catch (IOException e) {
            LOG.warning(e.getMessage());
        }
    }
}
