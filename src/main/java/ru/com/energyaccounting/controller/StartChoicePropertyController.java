package ru.com.energyaccounting.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import ru.com.energyaccounting.model.StartChoice;
import ru.com.energyaccounting.util.AddressDBSingleton;
import ru.com.energyaccounting.util.DBUtilAuditStatic;
import ru.com.energyaccounting.util.DBUtilStatic;
import ru.com.energyaccounting.util.EnergyAlert;

import java.io.File;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import static ru.com.energyaccounting.util.DBUtilStatic.bdName;

public class StartChoicePropertyController implements Initializable {
    private final static Logger LOG = Logger.getLogger(StartChoicePropertyController.class.getName());
    private StartChoice startChoice;
    private Stage stage;
    @FXML
    private TextField addressTextField;
    @FXML
    private TextField nameTextField;
    @FXML
    private CheckBox createBaseCheckBox;
    @FXML
    private CheckBox addDataCheckBox;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        createBaseCheckBox.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue){
                addDataCheckBox.setVisible(true);
                addDataCheckBox.setDisable(false);
                stage.setTitle("Создание базы данных");
            }else {
                addDataCheckBox.setVisible(false);
                addDataCheckBox.setDisable(true);
                addDataCheckBox.setSelected(false);
                stage.setTitle("Добавление базы данных");
            }
        });
    }

    private StartChoice getStartChoice() {
        return startChoice;
    }

    void setStartChoice(StartChoice startChoice) {
        this.startChoice = startChoice;
        addressTextField.setText(startChoice.getFilePath());
        nameTextField.setText(startChoice.getBaseName());

        if (!isValidTextFields()) {
            createBaseCheckBox.setVisible(true);
            createBaseCheckBox.setDisable(false);
        }
    }

    public boolean isInputValid() {
        String errorMessage = "";
        if (nameTextField.getText() == null || nameTextField.getText().length() == 0) {
            errorMessage += "Отсутствует найменование базы даных!\n";
        }
        if (addressTextField.getText() == null || addressTextField.getText().length() == 0) {
            errorMessage += "Отсутствует адрес базы данных!\nпример: создайте на диске С:\\ папку EnergyAccounting (С:\\EnergyAccounting) и укажите ее в этой программе. Внимание! В одной папке может быть только одна база данных";
        }

        return warningMessage(errorMessage);
    }


    @FXML
    public void handleCancel() {
        this.stage.close();
    }

    @FXML
    public void handleOK() {
        if (isInputValid()) {
            String pathDataBase = addressTextField.getText();

            getStartChoice().setFilePath(pathDataBase);
            getStartChoice().setBaseName(nameTextField.getText());

            File eaBdFile = new File(pathDataBase);
            String eaBdFileName = eaBdFile.getName();
            String eaBdPath = eaBdFile.getParent();
            getStartChoice().setBasePath(eaBdPath + File.separator);
            if (eaBdFile.exists()) {
                LOG.info("file EA.db is exist");

                if (createBaseCheckBox.isSelected()) {
                    EnergyAlert energyAlert = new EnergyAlert(Alert.AlertType.WARNING);
                    energyAlert.setTitle("Ошибка!");
                    energyAlert.setHeaderText("Ошибка создания базы данных!");
                    energyAlert.setContentText("В папке по адресу: " + eaBdPath + "\nбаза данных " + eaBdFileName + " уже существует!\nВыберите другую папку!");
                    energyAlert.showAndWait();
                } else {
                    this.stage.close();
                }

            } else {
                LOG.info("file EA.db is NOT exist");
                createBaseAndDemoDatas();
                this.stage.close();
            }
        }
    }

    private void createBaseAndDemoDatas() {
        try {
            //если чекбокс "создание бд" выбран, то передаем адрес в синглтон, после чего создаем базу
            LOG.info("come in createBaseAndDemoDatas and try getInstance");
            AddressDBSingleton addressDB = AddressDBSingleton.getInstance();
            LOG.info("try to SetStartChoice");
            addressDB.setStartChoice(getStartChoice());
            LOG.info("try to CreateSql");
            DBUtilStatic.createSqlDB();
            LOG.info("SQL base is made");
            DBUtilStatic.createRecordAtMigrationHistory();
            LOG.info("Record At MigrationHistory table is done");

            if (addDataCheckBox.isSelected()) {
                LOG.warning("addDataCheckBox is checked");
                DBUtilStatic.demoDataBD();
            }

        } catch (SQLException e) {
            e.printStackTrace();
            LOG.warning(e.getMessage());
        }
    }

    @FXML
    private void handleOpen() {
        if (startChoice.getBaseName() == null) {
            openDirectoryChooser();
        } else {
            openFileChooser();
        }
    }

    private void openDirectoryChooser() {
        DirectoryChooser dirChooser = new DirectoryChooser();
        dirChooser.setTitle("Выбрать базу данных");

        File selectedDirectory = dirChooser.showDialog(getStage());
        if (selectedDirectory != null) {
            String dirPath = selectedDirectory.getAbsolutePath();
            String fullPath = dirPath + File.separator + bdName;
            fullPath = fullPath.replace("\\\\", "\\");
            addressTextField.setText(fullPath);
        }
    }

    private void openFileChooser() {
        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter filter = new FileChooser.ExtensionFilter("SQL base", "*.db");
        fileChooser.getExtensionFilters().add(filter);
        fileChooser.setTitle("Выбрать базу данных");
        fileChooser.setInitialFileName(bdName);
//        fileChooser.initialDirectoryProperty().set(startChoice.getBasePath());

        try {
            File bdFile = fileChooser.showOpenDialog(getStage());
            if (bdFile != null) {
                String filePath = bdFile.getAbsolutePath();
                addressTextField.setText(filePath);
            }
        } catch (NullPointerException e) {
            EnergyAlert energyAlert = new EnergyAlert(Alert.AlertType.ERROR);
            energyAlert.setContentText("Выберите файл базы данных EnergyAccounting EA.db");
            energyAlert.showAndWait();
            e.getStackTrace();
        }
    }

    private boolean isValidTextFields() {
        if (addressTextField.getText() != null & nameTextField.getText() != null) {
            return addressTextField.getText().length() > 0 & nameTextField.getText().length() > 0;
        }
        return false;
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public boolean warningMessage(String errorMessage) {
        if (errorMessage != null && errorMessage.length() == 0) {
            return true;
        } else {
            EnergyAlert energyAlert = new EnergyAlert(Alert.AlertType.ERROR);
            energyAlert.initOwner(stage);
            energyAlert.setTitle("Не заполнены поля");
            energyAlert.setContentText(errorMessage);
            energyAlert.showAndWait();
            return false;
        }
    }
}
