package ru.com.energyaccounting.controller;

import ru.com.energyaccounting.dao.PersistException;
import ru.com.energyaccounting.model.ServiceMessage;

/**
 * Created by UserSts on 04.05.2017.
 */
public class blankAddOrEditController extends AbstractAddOrEditController<ServiceMessage> {

    public blankAddOrEditController(String dClass) {
        super(dClass);
    }

    @Override
    public boolean isInputValid() {
        return false;
    }

    @Override
    public void handleOK() throws PersistException {

    }

    @Override
    public void setObject(ServiceMessage object) {

    }
}
