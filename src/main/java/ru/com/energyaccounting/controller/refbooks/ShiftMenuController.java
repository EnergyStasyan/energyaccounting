package ru.com.energyaccounting.controller.refbooks;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import ru.com.energyaccounting.Main;
import ru.com.energyaccounting.controller.AbstractMenuController;
import ru.com.energyaccounting.model.Shift;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by UserSts on 31.05.2017.
 */
public class ShiftMenuController extends AbstractMenuController<Shift, ShiftAddOrEditController> implements Initializable{
    @FXML private TableColumn<Shift, Integer>   shiftColumnId;
    @FXML private TableColumn<Shift, String >   shiftColumnName;
    @FXML private TableColumn<Shift, String>    shiftColumnDescription;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);

        shiftColumnId.setCellValueFactory(cellData -> cellData.getValue().idProperty().asObject());
        shiftColumnName.setCellValueFactory(cellData ->cellData.getValue().nameProperty());
        shiftColumnDescription.setCellValueFactory(cellData ->cellData.getValue().descriptionProperty());
        searchObjects();
    }

    @Override
    protected Shift createObj() {
        return new Shift();
    }

    @Override
    protected String editOrAddLayout() {
        return Main.mShiftAddOrEditLayout;
    }

    @Override
    protected String getPathForJasperPattern() {
        return null;
    }
}
