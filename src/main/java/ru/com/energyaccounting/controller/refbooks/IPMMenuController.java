package ru.com.energyaccounting.controller.refbooks;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import ru.com.energyaccounting.Main;
import ru.com.energyaccounting.controller.AbstractMenuController;
import ru.com.energyaccounting.model.IPM;

import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

/**
 * Created by UserSts on 22.05.2017.
 */
public class IPMMenuController extends AbstractMenuController<IPM, IPMAddOrEditController> implements Initializable{


    @FXML private TableColumn<IPM, String> ipmColumnInventoryNumber;
    @FXML private TableColumn<IPM, String>  ipmColumnElectricalInstallation;
    @FXML private TableColumn<IPM, String>  ipmColumnIPMType;
    @FXML private TableColumn<IPM, String>  ipmColumnVoltageType;
    @FXML private TableColumn<IPM, String>  ipmColumnDescription;
    @FXML private TableColumn<IPM, LocalDate> ipmColumnNextTest;
    @FXML private TableColumn<IPM, LocalDate> ipmColumnLastTest;



    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);

        ipmColumnInventoryNumber.setCellValueFactory(cellData -> cellData.getValue().inventoryNumberProperty());
        ipmColumnElectricalInstallation.setCellValueFactory(cellData -> cellData.getValue().electricalInstallationProperty());
        ipmColumnIPMType.setCellValueFactory(cellData -> cellData.getValue().impTypeProperty());
        ipmColumnVoltageType.setCellValueFactory(cellData -> cellData.getValue().voltageTypeProperty());
        ipmColumnNextTest.setCellValueFactory(cellData -> cellData.getValue().nextTestDateProperty());
        ipmColumnLastTest.setCellValueFactory(cellData -> cellData.getValue().lastTestDateProperty());
        ipmColumnDescription.setCellValueFactory(cellData -> cellData.getValue().descriptionProperty());
        searchObjects();
    }

    @Override
    protected IPM createObj() {
        return new IPM();
    }

    @Override
    protected String editOrAddLayout() {
        return Main.mIpmAddOrEditLayout;
    }

    @Override
    protected String getPathForJasperPattern() {
        return null;
    }
}
