package ru.com.energyaccounting.controller.refbooks;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import ru.com.energyaccounting.Main;
import ru.com.energyaccounting.controller.AbstractMenuController;
import ru.com.energyaccounting.model.Subdivision;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by UserSts on 14.05.2017.
 */
public class SubdivisionMenuController extends AbstractMenuController<Subdivision, SubdivisionAddOrEditController> implements Initializable{

    @FXML private TableColumn<Subdivision, Integer> subdivisionsColumnId;
    @FXML private TableColumn<Subdivision, String> subdivisionsColumnName;
    @FXML private TableColumn<Subdivision, String> subdivisionsColumnOrganization;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);

        subdivisionsColumnId.setCellValueFactory(cellData -> cellData.getValue().idProperty().asObject());
        subdivisionsColumnName.setCellValueFactory(cellData -> cellData.getValue().nameProperty());
        subdivisionsColumnOrganization.setCellValueFactory(cellData -> cellData.getValue().organizationProperty());
        searchObjects();
    }

    @Override
    protected Subdivision createObj() {
        return new Subdivision();
    }

    @Override
    protected String editOrAddLayout() {
        return Main.mSubdivisionAddOrEditLayout;
    }

    @Override
    protected String getPathForJasperPattern() {
        return null;
    }
}
