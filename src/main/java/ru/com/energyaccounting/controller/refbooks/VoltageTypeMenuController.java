package ru.com.energyaccounting.controller.refbooks;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import ru.com.energyaccounting.Main;
import ru.com.energyaccounting.controller.AbstractMenuController;
import ru.com.energyaccounting.model.VoltageType;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by UserSts on 31.05.2017.
 */
public class VoltageTypeMenuController extends AbstractMenuController<VoltageType, VoltageTypeAddOrEditController> implements Initializable {
    @FXML private TableColumn<VoltageType, Integer> voltageTypeColumnId;
    @FXML private TableColumn<VoltageType, String>  voltageTypeColumnName;
    @FXML private TableColumn<VoltageType, String>  voltageTypeColumnDescription;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);

        voltageTypeColumnId.setCellValueFactory(cellData -> cellData.getValue().idProperty().asObject());
        voltageTypeColumnName.setCellValueFactory(cellData -> cellData.getValue().nameProperty());
        voltageTypeColumnDescription.setCellValueFactory(cellData -> cellData.getValue().descriptionProperty());
        searchObjects();
    }

    @Override
    protected VoltageType createObj() {
        return new VoltageType();
    }

    @Override
    protected String editOrAddLayout() {
        return Main.mVoltageTypeAddOrEditLayout;
    }

    @Override
    protected String getPathForJasperPattern() {
        return null;
    }
}
