package ru.com.energyaccounting.controller.refbooks;

import ru.com.energyaccounting.controller.AbstractAddOrEditController;
import ru.com.energyaccounting.dao.PersistException;
import ru.com.energyaccounting.model.VoltageType;

/**
 * Created by UserSts on 31.05.2017.
 */
public class VoltageTypeAddOrEditController extends AbstractAddOrEditController<VoltageType> {
    VoltageType voltageType;
    public VoltageTypeAddOrEditController() {
        super(VoltageType.class.getName());
    }

    @Override
    public boolean isInputValid() {
        String errorMessage = "";
        if (nameTextField == null || nameTextField.getText().length() == 0){
            errorMessage += "Отсутствует найменование Класса напряжения!\n";
        }
        return warningMessage(errorMessage);
    }

    @Override
    public void handleOK() throws PersistException {
        String name = nameTextField.getText();
        String description = descriptionTextField.getText();
        if (isInputValid()){
            voltageType.setName(name);
            voltageType.setDescription(description);
           objectPersistUpdate(voltageType);

        }
    }

    @Override
    public void setObject(VoltageType voltageType) {
        this.voltageType = voltageType;
        this.dialogStage.setTitle("Редактирование: " + this.voltageType.getDialogStageTitle());
        idLabel.setText(Integer.toString(voltageType.getId()));
        nameTextField.setText(voltageType.getName());
        descriptionTextField.setText(voltageType.getDescription());
    }
}
