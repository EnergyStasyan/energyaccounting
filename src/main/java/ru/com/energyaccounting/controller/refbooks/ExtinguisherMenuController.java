package ru.com.energyaccounting.controller.refbooks;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import ru.com.energyaccounting.Main;
import ru.com.energyaccounting.controller.AbstractMenuController;
import ru.com.energyaccounting.model.Extinguisher;

import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

/**
 * Created by UserSts on 30.05.2017.
 */
public class ExtinguisherMenuController extends AbstractMenuController<Extinguisher,ExtinguisherAddOrEditController> implements Initializable {
    @FXML private TableColumn<Extinguisher, Integer>    extinguisherColumnId;
    @FXML private TableColumn<Extinguisher, String>     extinguisherColumnInventoryNumber;
    @FXML private TableColumn<Extinguisher, String>     extinguisherColumnExtType;
    @FXML private TableColumn<Extinguisher, LocalDate>  extinguisherColumnNextTestDate;
    @FXML private TableColumn<Extinguisher, LocalDate>  extinguisherColumnLastTestDate;
    @FXML private TableColumn<Extinguisher, String>     extinguisherColumnElectricalInstallations;
    @FXML private TableColumn<Extinguisher, String>     extinguisherColumnDescription;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);

        extinguisherColumnId.setCellValueFactory(cellData ->cellData.getValue().idProperty().asObject());
        extinguisherColumnInventoryNumber.setCellValueFactory(cellData ->cellData.getValue().inventoryNumberProperty());
        extinguisherColumnExtType.setCellValueFactory(cellData ->cellData.getValue().extinguisherTypeProperty());
        extinguisherColumnNextTestDate.setCellValueFactory(cellData ->cellData.getValue().nextTestDateProperty());
        extinguisherColumnLastTestDate.setCellValueFactory(cellData ->cellData.getValue().lastTestDateProperty());
        extinguisherColumnElectricalInstallations.setCellValueFactory(cellData ->cellData.getValue().electricalInstallationProperty());
        extinguisherColumnDescription.setCellValueFactory(cellData ->cellData.getValue().descriptionProperty());
        searchObjects();
    }

    @Override
    protected Extinguisher createObj() {
        return new Extinguisher();
    }

    @Override
    protected String editOrAddLayout() {
        return Main.mExtinguisherAddOrEditLayout;
    }

    @Override
    protected String getPathForJasperPattern() {
        return null;
    }
}
