package ru.com.energyaccounting.controller.refbooks;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import ru.com.energyaccounting.Main;
import ru.com.energyaccounting.controller.AbstractMenuController;
import ru.com.energyaccounting.model.ExtinguisherType;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by UserSts on 29.05.2017.
 */
public class ExtinguisherTypeMenuController extends AbstractMenuController<ExtinguisherType, ExtinguisherTypeAddOrEditController> implements Initializable{

    @FXML private TableColumn<ExtinguisherType, Integer> extinguisherTypeColumnId;
    @FXML private TableColumn<ExtinguisherType, String> extinguisherTypeColumnName;
    @FXML private TableColumn<ExtinguisherType, String> extinguisherTypeColumnDescription;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);

        extinguisherTypeColumnId.setCellValueFactory(cellData -> cellData.getValue().idProperty().asObject());
        extinguisherTypeColumnName.setCellValueFactory(cellData -> cellData.getValue().nameProperty());
        extinguisherTypeColumnDescription.setCellValueFactory(cellData -> cellData.getValue().descriptionProperty());
        searchObjects();
    }

    @Override
    protected ExtinguisherType createObj() {
        return new ExtinguisherType();
    }

    @Override
    protected String editOrAddLayout() {
        return Main.mExtinguisherTypeAddOrEditLayout;
    }

    @Override
    protected String getPathForJasperPattern() {
        return null;
    }
}
