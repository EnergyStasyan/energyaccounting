package ru.com.energyaccounting.controller.refbooks;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import ru.com.energyaccounting.controller.AbstractAddOrEditController;
import ru.com.energyaccounting.dao.PersistException;
import ru.com.energyaccounting.model.ElectricalEquipmentType;

import java.util.logging.Logger;

public class ElectricalEquipmentTypesAddOrEditController extends AbstractAddOrEditController<ElectricalEquipmentType> {
    private static final Logger LOG = Logger.getLogger(ElectricalEquipmentTypesAddOrEditController.class.getName());

    @FXML
    private Label idLabel;
    @FXML
    private TextField nameTextField;
    @FXML
    private TextField descriptionTextField;
    private ElectricalEquipmentType electricalEquipmentType;

    public ElectricalEquipmentTypesAddOrEditController() {
        super(ElectricalEquipmentType.class.getName());
    }


    @Override
    public boolean isInputValid() {
        String errorMessage = "";

        if (nameTextField == null || nameTextField.getText().length() == 0) {
            errorMessage +=  errorTextMessage + "Типа эл.оборудования!\n";
        }
        return warningMessage(errorMessage);
    }

    @Override
    public void handleOK() throws PersistException {
        LOG.info("нажали Ок");
        String name = nameTextField.getText();
        String description = descriptionTextField.getText();
        if (isInputValid()) {
            electricalEquipmentType.setName(name);
            electricalEquipmentType.setDescription(description);
            objectPersistUpdate(electricalEquipmentType);
        }
    }

    @Override
    public void setObject(ElectricalEquipmentType electricalEquipmentType) {
        this.electricalEquipmentType = electricalEquipmentType;
        this.dialogStage.setTitle(edit + this.electricalEquipmentType.getDialogStageTitle());
        idLabel.setText(Integer.toString(electricalEquipmentType.getId()));
        nameTextField.setText(electricalEquipmentType.getName());
        descriptionTextField.setText(electricalEquipmentType.getDescription());
    }
}
