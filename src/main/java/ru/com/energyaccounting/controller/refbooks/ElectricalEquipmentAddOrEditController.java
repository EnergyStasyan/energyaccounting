package ru.com.energyaccounting.controller.refbooks;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import ru.com.energyaccounting.Main;
import ru.com.energyaccounting.controller.AbstractAddOrEditController;
import ru.com.energyaccounting.dao.PersistException;
import ru.com.energyaccounting.model.ElectricalEquipment;
import ru.com.energyaccounting.model.ElectricalEquipmentType;
import ru.com.energyaccounting.model.ElectricalInstallation;
import ru.com.energyaccounting.model.VoltageType;

import java.util.logging.Logger;

/**
 * Created by UserSts on 12.05.2017.
 */
public class ElectricalEquipmentAddOrEditController extends AbstractAddOrEditController<ElectricalEquipment> {
    private static final Logger LOG = Logger.getLogger(ElectricalEquipmentAddOrEditController.class.getName());

    @FXML
    private Label indexNumberLabel;
    @FXML
    private TextField nameTextField;
    @FXML
    private TextField inventoryNumberTextField;
    @FXML
    private Label voltageTypeLabel;
    @FXML
    private Label electricalInstallationLabel;
    @FXML
    private Label electricalEquipmentTypeLabel;
    @FXML
    private TextField remarksTextField;
    @FXML
    private Label lastTestDateLabel;
    @FXML
    private Label nextTestDateLabel;

    private ElectricalEquipment electricalEquipment;

    public ElectricalEquipmentAddOrEditController() {
        super(ElectricalEquipment.class.getName());
    }

    @Override
    public boolean isInputValid() {
        String errorMessage = "";
        if (nameTextField.getText() == null || nameTextField.getText().length() == 0) {
            errorMessage += "Отсутствует Наименование!\n";
        }
        if (voltageTypeLabel.getText() == null) {
            errorMessage += "Отсутствует Класс Напряжения!\n";
        }
        if (electricalInstallationLabel.getText() == null) {
            errorMessage += "Отсутствует Электроустановка\n";
        }
        if (electricalEquipmentTypeLabel.getText() == null) {
            errorMessage += "Отсутствует тип эл.оборудования\n";
        }
        return warningMessage(errorMessage);
    }

    @Override
    public void handleOK() throws PersistException {
        LOG.info("нажали Ок");

        String name = nameTextField.getText();
        String inventoryNumber = inventoryNumberTextField.getText();
        String voltageType = voltageTypeLabel.getText();
        String electricalEquipmentType = electricalEquipmentTypeLabel.getText();
        String electricalInstallationString = electricalInstallationLabel.getText();
        String remarks = remarksTextField.getText();

        if (isInputValid()) {
            electricalEquipment.setName(name);
            electricalEquipment.setInventoryNumber(inventoryNumber);
            electricalEquipment.setVoltageType(voltageType);
            electricalEquipment.setElectricalInstallation(electricalInstallationString);
            electricalEquipment.setDescription(remarks);
            electricalEquipment.setElectricalEquipmentType(electricalEquipmentType);
            objectPersistUpdate(electricalEquipment);
        }
    }

    @Override
    public void setObject(ElectricalEquipment electricalEquipment) {
        this.electricalEquipment = electricalEquipment;
        this.dialogStage.setTitle("Редактирование: " + this.electricalEquipment.getDialogStageTitle());
        indexNumberLabel.setText(Integer.toString(electricalEquipment.getId()));
        inventoryNumberTextField.setText(electricalEquipment.getInventoryNumber());
        nameTextField.setText(electricalEquipment.getName());
        voltageTypeLabel.setText(electricalEquipment.getVoltageType());
        electricalInstallationLabel.setText(electricalEquipment.getElectricalInstallation());
        electricalEquipmentTypeLabel.setText(electricalEquipment.getElectricalEquipmentType());
        remarksTextField.setText(electricalEquipment.getDescription());
        statusLabel.setText(electricalEquipment.getStatus());
        lastTestDateLabel.setText(electricalEquipment.getLastTestDate() != null ? electricalEquipment.getLastTestDate().toString() : "Неизвестно");
        nextTestDateLabel.setText(electricalEquipment.getNextTestDate() != null ? electricalEquipment.getNextTestDate().toString() : "Неизвестно");

    }


    @Override
    public void threeDotsVoltageType() {
        VoltageType voltageType = (VoltageType) showLayoutSelectMenu(Main.mVoltageTypeMenuLayout);
        if (voltageType != null) {
            voltageTypeLabel.setText(voltageType.getName());
            electricalEquipment.setVt_id(voltageType.getId());
        }
    }

    @Override
    public void threeDotsElectricalEquipmentType() {
        ElectricalEquipmentType electricalEquipmentType = (ElectricalEquipmentType) showLayoutSelectMenu(Main.mElectricalEquipmentTypesMenuLayout);
        if (electricalEquipmentType != null) {
            electricalEquipmentTypeLabel.setText(electricalEquipmentType.getName());
            electricalEquipment.setElectrical_equipment_type_id(electricalEquipmentType.getId());
        }
    }

    @Override
    public void threeDotsElectricalInstallation() {
        ElectricalInstallation electricalInstallation = (ElectricalInstallation) showLayoutSelectMenu(Main.mElectricalInstallationsMenuLayout);
        if (electricalInstallation != null) {
            electricalInstallationLabel.setText(electricalInstallation.getName());
            electricalEquipment.setEl_install_id(electricalInstallation.getId());
        }
    }
}
