package ru.com.energyaccounting.controller.refbooks;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import ru.com.energyaccounting.Main;
import ru.com.energyaccounting.controller.AbstractMenuController;
import ru.com.energyaccounting.model.Organization;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by UserSts on 20.05.2017.
 */
public class OrganizationMenuController extends AbstractMenuController<Organization, OrganizationAddOrEditController> implements Initializable {

    @FXML private TableColumn<Organization, Integer> organizationsColumnId;
    @FXML private TableColumn<Organization, String> organizationsColumnName;
    @FXML private TableColumn<Organization, String> organizationsColumnDescription;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);

        organizationsColumnId.          setCellValueFactory(cellData -> cellData.getValue().idProperty().asObject());
        organizationsColumnName.        setCellValueFactory(cellData -> cellData.getValue().nameProperty());
        organizationsColumnDescription. setCellValueFactory(cellData -> cellData.getValue().descriptionProperty());
        searchObjects();
    }

    @Override
    protected Organization createObj() {
        return new Organization();
    }

    @Override
    protected String editOrAddLayout() {
        return Main.mOrganizationAddOrEditLayout;
    }

    @Override
    protected String getPathForJasperPattern() {
        return null;
    }
}
