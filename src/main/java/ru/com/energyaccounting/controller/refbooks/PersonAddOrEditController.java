package ru.com.energyaccounting.controller.refbooks;

import javafx.fxml.FXML;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import ru.com.energyaccounting.Main;
import ru.com.energyaccounting.controller.AbstractAddOrEditController;
import ru.com.energyaccounting.dao.PersistException;
import ru.com.energyaccounting.model.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 *
 */
public class PersonAddOrEditController extends AbstractAddOrEditController<Person> {

    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd MMMM yyyy");
    @FXML private Label idLabel;
    @FXML private TextField personIdTextField;
    @FXML private TextField surnameTextField;
    @FXML private TextField nameTextField;
    @FXML private TextField middleNameTextField;
    @FXML private TextField phoneTextField;
    @FXML private DatePicker hireDateDP;
    @FXML private DatePicker dismissalDateDP;
    @FXML private DatePicker birthdayDateDP;
    @FXML private Label subdivisionLabel;
    @FXML private Label groupOfAccessLabel;
    @FXML private Label nextTestDateLabel;
    @FXML private Label positionLabel;
    @FXML private TextField salaryTextField;
    private Person person;

    public PersonAddOrEditController() {
        super(Person.class.getName());
    }

    @Override
    public void setObject(Person person) {
        this.person = person;
        this.dialogStage.setTitle("Редактирование: " + this.person.getDialogStageTitle());
        idLabel.setText(Integer.toString(person.getId()));
        personIdTextField.setText(Integer.toString(person.getPersonId()));
        surnameTextField.setText(person.getSurname());
        nameTextField.setText(person.getName());
        middleNameTextField.setText(person.getMiddleName());
        phoneTextField.setText(person.getPhone());
        birthdayDateDP.setValue(person.getBirthdayDate());
        hireDateDP.setValue(person.getHireDate());
        dismissalDateDP.setValue(person.getDismissalDate());
        try {
            nextTestDateLabel.setText(person.getNextTestDate().format(dtf));
        }catch (NullPointerException e){
            nextTestDateLabel.setText("Экзамена не было");
        }
        subdivisionLabel.setText(person.getSubdivision());
        groupOfAccessLabel.setText(person.getAccessGroup());
        positionLabel.setText(person.getPosition());
        salaryTextField.setText(Float.toString(person.getSalary()));
    }

    @Override
    public boolean isInputValid() {
        String errorMessage = "";

        if (surnameTextField.getText() == null || surnameTextField.getText().length() == 0){
            errorMessage += "Отсутствует фамилия!\n";
        }

        if (nameTextField.getText() == null || nameTextField.getText().length() == 0){
            errorMessage += "Отсутствует имя!\n";
        }

        return warningMessage(errorMessage);
    }

    @Override
    @FXML public void handleOK() throws PersistException{

        String surName = surnameTextField.getText();
        String name = nameTextField.getText();
        String middleName = middleNameTextField.getText();
        String phoneNumber = phoneTextField.getText();
        String subdivision =  subdivisionLabel.getText();


        float salaryFloat = Float.parseFloat(salaryTextField.getText());
        int personId = Integer.parseInt(personIdTextField.getText());

        //записываем в локальную дату данные с формв
        LocalDate birthdayDate = birthdayDateDP.getValue() ;
        LocalDate hireDate = hireDateDP.getValue();
        LocalDate dismissalDate = dismissalDateDP.getValue();

        if (isInputValid()) {
            person.setPersonId(personId);
            person.setSurname(surName);
            person.setName(name);
            person.setMiddleName(middleName);
            person.setPhone(phoneNumber);
            person.setBirthdayDate(birthdayDate);
            person.setHireDate(hireDate);
            person.setDismissalDate(dismissalDate);
            person.setSubdivision(subdivision);
            person.setSalary(salaryFloat);

            objectPersistUpdate(person);
        }
    }

    @Override
    @FXML public void threeDotsSubdivision(){
        Subdivision subdivision = (Subdivision) showLayoutSelectMenu(Main.mSubdivisionsMenuLayout);
        if (subdivision != null) {
            subdivisionLabel.setText(subdivision.getName());
            person.setSubdiv_id(subdivision.getId());
        }
    }
    @Override
    @FXML public void threeDotsPosition(){
        Position position = (Position) showLayoutSelectMenu(Main.mPositionMenuLayout);
        if (position != null) {
            positionLabel.setText(position.getName());
            person.setPosition_id(position.getId());
        }
    }

}
