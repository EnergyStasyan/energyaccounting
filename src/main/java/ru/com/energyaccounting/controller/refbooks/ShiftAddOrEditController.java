package ru.com.energyaccounting.controller.refbooks;

import ru.com.energyaccounting.controller.AbstractAddOrEditController;
import ru.com.energyaccounting.dao.PersistException;
import ru.com.energyaccounting.model.Shift;

/**
 * Created by UserSts on 31.05.2017.
 */
public class ShiftAddOrEditController extends AbstractAddOrEditController<Shift> {
    private Shift shift;

    public ShiftAddOrEditController() {
        super(Shift.class.getName());
    }

    @Override
    public boolean isInputValid() {
        String errorMessage = "";

        if (nameTextField == null || nameTextField.getText().length() == 0){
            errorMessage += "Отсутствует найменование Смены!\n";
        }

        return warningMessage(errorMessage);
    }

    @Override
    public void handleOK() throws PersistException {
        String name = nameTextField.getText();
        String description = descriptionTextField.getText();
        if (isInputValid()){
            shift.setName(name);
            shift.setDescription(description);
            objectPersistUpdate(shift);

        }
    }

    @Override
    public void setObject(Shift shift) {
        this.shift = shift;
        this.dialogStage.setTitle("Редактирование: " + this.shift.getDialogStageTitle());
        idLabel.setText(Integer.toString(shift.getId()));
        nameTextField.setText(shift.getName());
        descriptionTextField.setText(shift.getDescription());
    }
}
