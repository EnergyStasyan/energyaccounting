package ru.com.energyaccounting.controller.refbooks;

import ru.com.energyaccounting.controller.AbstractAddOrEditController;
import ru.com.energyaccounting.controller.docs.IpmDocAddOrEditController;
import ru.com.energyaccounting.dao.PersistException;
import ru.com.energyaccounting.model.ExtinguisherType;

import java.util.logging.Logger;

/**
 * Created by UserSts on 29.05.2017.
 */
public class ExtinguisherTypeAddOrEditController extends AbstractAddOrEditController<ExtinguisherType> {
    private static final Logger LOG = Logger.getLogger(ExtinguisherTypeAddOrEditController.class.getName());

    private ExtinguisherType extinguisherType;

    public ExtinguisherTypeAddOrEditController() {
        super(ExtinguisherType.class.getName());
    }

    @Override
    public boolean isInputValid() {
        String errorMessage = "";

        if (nameTextField == null || nameTextField.getText().length() == 0){
            errorMessage += "Отсутствует найменование Организации!\n";
        }

        return warningMessage(errorMessage);
    }

    @Override
    public void handleOK() throws PersistException {
        LOG.info("нажали Ок");

        String name = nameTextField.getText();
        String description = descriptionTextField.getText();
        if (isInputValid()){
            extinguisherType.setName(name);
            extinguisherType.setDescription(description);
            objectPersistUpdate(extinguisherType);

        }
    }

    @Override
    public void setObject(ExtinguisherType extinguisherType) {
        this.extinguisherType = extinguisherType;
        this.dialogStage.setTitle("Редактирование: " + this.extinguisherType.getDialogStageTitle());
        idLabel.setText(Integer.toString(extinguisherType.getId()));
        nameTextField.setText(extinguisherType.getName());
        descriptionTextField.setText(extinguisherType.getDescription());
    }
}
