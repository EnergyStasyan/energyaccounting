package ru.com.energyaccounting.controller.refbooks;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import ru.com.energyaccounting.Main;
import ru.com.energyaccounting.controller.AbstractAddOrEditController;
import ru.com.energyaccounting.dao.PersistException;
import ru.com.energyaccounting.model.Organization;
import ru.com.energyaccounting.model.Subdivision;

/**
 * Created by UserSts on 14.05.2017.
 */
public class SubdivisionAddOrEditController extends AbstractAddOrEditController<Subdivision> {

    @FXML private Label idLabel;
    @FXML private TextField nameTextField;
    @FXML private Label organizationLabel;
    private Subdivision subdivision;

    public SubdivisionAddOrEditController() {
        super(Subdivision.class.getName());
    }

    @Override
    public boolean isInputValid() {
        String errorMessage = "";

        if (nameTextField == null || nameTextField.getText().length() == 0){
            errorMessage += "Отсутствует найменование Организации!\n";
        }

        return warningMessage(errorMessage);
    }

    @Override
    public void handleOK() throws PersistException {

        String name = nameTextField.getText();
        String organization = organizationLabel.getText();

        if (isInputValid()){
            subdivision.setName(name);
            subdivision.setOrganization(organization);
            objectPersistUpdate(subdivision);

        }
    }

    @Override
    public void setObject(Subdivision subdivision) {
        this.subdivision = subdivision;
        this.dialogStage.setTitle("Редактирование: " + this.subdivision.getDialogStageTitle());
        idLabel.setText(Integer.toString(subdivision.getId()));
        nameTextField.setText(subdivision.getName());
        organizationLabel.setText(subdivision.getOrganization());

    }

    @Override
    public void threeDotsOrganization() {
        Organization organization = (Organization) showLayoutSelectMenu(Main.mOrganizationsMenuLayout);
        if (organization != null) {
            organizationLabel.setText(organization.getName());
            subdivision.setOrg_id(organization.getId());
        }
    }
}
