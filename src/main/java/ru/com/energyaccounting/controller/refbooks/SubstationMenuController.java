package ru.com.energyaccounting.controller.refbooks;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import ru.com.energyaccounting.Main;
import ru.com.energyaccounting.controller.AbstractMenuController;
import ru.com.energyaccounting.model.Substation;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by UserSts on 19.05.2017.
 */
public class SubstationMenuController extends AbstractMenuController<Substation, SubstationAddOrEditController > implements Initializable {

    @FXML private TableColumn<Substation, Integer> substationsColumnId;
    @FXML private TableColumn<Substation, String> substationsColumnName;
    @FXML private TableColumn<Substation, String > substationsColumnOrganization;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);

        substationsColumnId.setCellValueFactory(cellData -> cellData.getValue().idProperty().asObject());
        substationsColumnName.setCellValueFactory(cellData -> cellData.getValue().nameProperty());
        substationsColumnOrganization.setCellValueFactory(cellData -> cellData.getValue().organizationProperty());
        searchObjects();
    }

    @Override
    protected Substation createObj() {
        return new Substation();
    }

    @Override
    protected String editOrAddLayout() {
        return Main.mSubstationAddOrEditLayout;
    }

    @Override
    protected String getPathForJasperPattern() {
        return null;
    }
}
