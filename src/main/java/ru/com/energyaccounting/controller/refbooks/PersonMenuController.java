package ru.com.energyaccounting.controller.refbooks;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import ru.com.energyaccounting.Main;
import ru.com.energyaccounting.controller.AbstractMenuController;
import ru.com.energyaccounting.model.Person;

import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

/**
 * Created by UserSts on 02.05.2017.
 */
public class PersonMenuController extends AbstractMenuController<Person, PersonAddOrEditController> implements Initializable{

    @FXML private TableColumn<Person, Integer> personColumnId;
    @FXML private TableColumn<Person, Integer> personColumnPersonId;
    @FXML private TableColumn<Person, String> personColumnSurname;
    @FXML private TableColumn<Person, String> personColumnName;
    @FXML private TableColumn<Person, String> personColumnMiddleName;
    @FXML private TableColumn<Person, LocalDate> personColumnBirthday;
    @FXML private TableColumn<Person, LocalDate> personColumnHireDate;
    @FXML private TableColumn<Person, LocalDate> personColumnDismissalDate;
    @FXML private TableColumn<Person, String> personColumnPhone;
    @FXML private TableColumn<Person, String> personColumnSubdivision;



    @Override
    protected Person createObj() {
        return new Person();
    }

    @Override
    protected String editOrAddLayout() {
        return Main.mPersonEditOrAddLayout;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);
        personColumnId.setCellValueFactory(cellData ->cellData.getValue().idProperty().asObject());
        personColumnPersonId.setCellValueFactory(cellData ->cellData.getValue().personIdProperty().asObject());
        personColumnName.setCellValueFactory(cellData -> cellData.getValue().nameProperty());
        personColumnMiddleName.setCellValueFactory(cellData -> cellData.getValue().middleNameProperty());
        personColumnSurname.setCellValueFactory(cellData -> cellData.getValue().surnameProperty());
        personColumnBirthday.setCellValueFactory(cellData -> cellData.getValue().birthdayDateProperty());
        personColumnHireDate.setCellValueFactory(cellData -> cellData.getValue().hireDateProperty());
        personColumnDismissalDate.setCellValueFactory(cellData -> cellData.getValue().dismissalDateProperty());
        personColumnPhone.setCellValueFactory(cellData -> cellData.getValue().phoneProperty());
        personColumnSubdivision.setCellValueFactory(cellData -> cellData.getValue().subdivisionProperty());

        searchObjects();
    }

    @Override
    protected String getPathForJasperPattern() {
        return "testPattern.jrxml";
    }
}
