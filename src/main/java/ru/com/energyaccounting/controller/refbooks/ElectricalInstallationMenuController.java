package ru.com.energyaccounting.controller.refbooks;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import ru.com.energyaccounting.Main;
import ru.com.energyaccounting.controller.AbstractMenuController;
import ru.com.energyaccounting.model.ElectricalInstallation;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by UserSts on 11.05.2017.
 */
public class ElectricalInstallationMenuController extends AbstractMenuController<ElectricalInstallation, ElectricalInstallationAddOrEditController> implements Initializable {

    @FXML private TableColumn<ElectricalInstallation, Integer> electricalInstallationsColumnId;
    @FXML private TableColumn<ElectricalInstallation, String> electricalInstallationsName;
    @FXML private TableColumn<ElectricalInstallation, String> electricalInstallationsVoltage;
    @FXML private TableColumn<ElectricalInstallation, String> electricalInstallationsSubstation;
    @FXML private TableColumn<ElectricalInstallation, String> electricalInstallationsNote;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);
        electricalInstallationsColumnId.setCellValueFactory(cellData -> cellData.getValue().idProperty().asObject());
        electricalInstallationsName.setCellValueFactory(cellData -> cellData.getValue().nameProperty());
        electricalInstallationsVoltage.setCellValueFactory(cellData -> cellData.getValue().voltageTypeProperty());
        electricalInstallationsSubstation.setCellValueFactory(cellData -> cellData.getValue().substationProperty());
        electricalInstallationsNote.setCellValueFactory(cellData -> cellData.getValue().descriptionProperty());

        searchObjects();
    }

    @Override
    protected ElectricalInstallation createObj() {
        return new ElectricalInstallation();
    }

    @Override
    protected String editOrAddLayout() {
        return Main.mElectricalInstallationsAddOrEditLayout;
    }

    @Override
    protected String getPathForJasperPattern() {
        return null;
    }
}
