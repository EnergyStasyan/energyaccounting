package ru.com.energyaccounting.controller.refbooks;

import ru.com.energyaccounting.controller.AbstractAddOrEditController;
import ru.com.energyaccounting.dao.PersistException;
import ru.com.energyaccounting.model.Organization;

/**
 * Created by UserSts on 20.05.2017.
 */
public class OrganizationAddOrEditController extends AbstractAddOrEditController<Organization> {

    private Organization organization;

    public OrganizationAddOrEditController() {
        super(Organization.class.getName());
    }

    @Override
    public boolean isInputValid() {
        String errorMessage = "";

        if (nameTextField == null || nameTextField.getText().length() == 0){
            errorMessage += "Отсутствует найменование Организации!\n";
        }

        return warningMessage(errorMessage);
    }

    @Override
    public void handleOK() throws PersistException {
        String name = nameTextField.getText();
        String description = descriptionTextField.getText();
        if (isInputValid()){
            organization.setName(name);
            organization.setDescription(description);
            objectPersistUpdate(organization);

        }
    }

    @Override
    public void setObject(Organization organization) {
        this.organization = organization;
        this.dialogStage.setTitle("Редактирование: " + this.organization.getDialogStageTitle());
        idLabel.setText(Integer.toString(organization.getId()));
        nameTextField.setText(organization.getName());
        descriptionTextField.setText(organization.getDescription());
    }
}
