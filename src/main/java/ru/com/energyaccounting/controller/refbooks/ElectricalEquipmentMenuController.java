package ru.com.energyaccounting.controller.refbooks;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import ru.com.energyaccounting.Main;
import ru.com.energyaccounting.controller.AbstractMenuController;
import ru.com.energyaccounting.model.ElectricalEquipment;

import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

/**
 * Created by UserSts on 12.05.2017.
 */
public class ElectricalEquipmentMenuController extends AbstractMenuController<ElectricalEquipment, ElectricalEquipmentAddOrEditController> implements Initializable{

    @FXML private TableColumn<ElectricalEquipment, Integer> electricalEquipmentColumnId;
    @FXML private TableColumn<ElectricalEquipment, String> electricalEquipmentColumnElectricalInstallation;
    @FXML private TableColumn<ElectricalEquipment, String> electricalEquipmentColumnName;
    @FXML private TableColumn<ElectricalEquipment, String> electricalEquipmentColumnInventoryNumber;
    @FXML private TableColumn<ElectricalEquipment, String> electricalEquipmentColumnDescription;
    @FXML private TableColumn<ElectricalEquipment, LocalDate> electricalEquipmentColumnNextTest;
    @FXML private TableColumn<ElectricalEquipment, LocalDate> electricalEquipmentColumnLastTest;
    @FXML private TableColumn<ElectricalEquipment, String> electricalEquipmentColumnElectricalEquipmentType;



    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);

        electricalEquipmentColumnId.setCellValueFactory(cellData -> cellData.getValue().idProperty().asObject());
        electricalEquipmentColumnElectricalInstallation.setCellValueFactory(cellData -> cellData.getValue().electricalInstallationProperty());
        electricalEquipmentColumnName.setCellValueFactory(cellDate -> cellDate.getValue().nameProperty());
        electricalEquipmentColumnInventoryNumber.setCellValueFactory(cellDate -> cellDate.getValue().inventoryNumberProperty());
        electricalEquipmentColumnDescription.setCellValueFactory(cellData -> cellData.getValue().descriptionProperty());
        electricalEquipmentColumnNextTest.setCellValueFactory(cellData -> cellData.getValue().nextTestDateProperty());
        electricalEquipmentColumnLastTest.setCellValueFactory(cellData -> cellData.getValue().lastTestDateProperty());
        electricalEquipmentColumnElectricalEquipmentType.setCellValueFactory(cellData -> cellData.getValue().electricalEquipmentTypeProperty());

        searchObjects();
    }

    @Override
    protected ElectricalEquipment createObj() {
        return new ElectricalEquipment();
    }

    @Override
    protected String editOrAddLayout() {
        return Main.mElectricalEquipmentsAddOrEditLayout;
    }

    @Override
    protected String getPathForJasperPattern() {
        return null;
    }
}
