package ru.com.energyaccounting.controller.refbooks;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import ru.com.energyaccounting.Main;
import ru.com.energyaccounting.controller.AbstractAddOrEditController;
import ru.com.energyaccounting.controller.docs.IpmDocAddOrEditController;
import ru.com.energyaccounting.dao.PersistException;
import ru.com.energyaccounting.model.*;

import java.util.logging.Logger;

/**
 * Created by UserSts on 11.05.2017.
 */
public class ElectricalInstallationAddOrEditController extends AbstractAddOrEditController<ElectricalInstallation> {
    private static final Logger LOG = Logger.getLogger(ElectricalInstallationAddOrEditController.class.getName());

    @FXML private Label idLabel;
    @FXML private TextField nameTextField;
    @FXML private Label voltageTypeLabel;
    @FXML private Label substationLabel;
    @FXML private TextField noteTextField;

    private ElectricalInstallation electricalInstallation;

    public ElectricalInstallationAddOrEditController() {
        super(ElectricalInstallation.class.getName());
    }

    @Override
    public boolean isInputValid() {
        String errorMessage = "";
        if (nameTextField.getText() == null || nameTextField.getLength() == 0) {
            errorMessage += "Отсутствует Наименование!\n";
        }
        // FIXME: 13.04.2017  починить!!
        if (voltageTypeLabel.getText() == null) {
            errorMessage += "Отсутствует Класс Напряжения!\n";
        }
        if (substationLabel.getText() == null) {
            errorMessage += "Отсутствует Подстанция!\n";
        }

        return warningMessage(errorMessage);
    }

    @Override
    public void handleOK() throws PersistException {
        LOG.info("нажали Ок");

        //boolean isInsert;
        String name = nameTextField.getText();
        String voltageType = voltageTypeLabel.getText();
        String substation = substationLabel.getText();
        String note = noteTextField.getText();

        if (isInputValid()) {
            electricalInstallation.setName(name);
            electricalInstallation.setVoltageType(voltageType);
            electricalInstallation.setSubstation(substation);
            electricalInstallation.setDescription(note);


           objectPersistUpdate(electricalInstallation);

        }
    }

    @Override
    public void setObject(ElectricalInstallation electricalInstallation) {
        this.electricalInstallation = electricalInstallation;
        this.dialogStage.setTitle("Редактирование: " + this.electricalInstallation.getDialogStageTitle());
        idLabel.setText(Integer.toString(electricalInstallation.getId()));
        nameTextField.setText(electricalInstallation.getName());
        voltageTypeLabel.setText(electricalInstallation.getVoltageType());
        substationLabel.setText(electricalInstallation.getSubstation());
        noteTextField.setText(electricalInstallation.getDescription());
    }

    @Override
    public void threeDotsVoltageType() {
        VoltageType voltageType = (VoltageType) showLayoutSelectMenu(Main.mVoltageTypeMenuLayout);
        if (voltageType != null) {
            voltageTypeLabel.setText(voltageType.getName());
            electricalInstallation.setVt_id(voltageType.getId());
        }
    }

    @Override
    public void threeDotsSubstation() {
        Substation substation = (Substation) showLayoutSelectMenu(Main.mSubstationsMenuLayout);
        if (substation != null) {
            substationLabel.setText(substation.getName());
            electricalInstallation.setSubstat_id(substation.getId());
        }
    }
}
