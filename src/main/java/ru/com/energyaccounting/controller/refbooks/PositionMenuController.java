package ru.com.energyaccounting.controller.refbooks;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import ru.com.energyaccounting.Main;
import ru.com.energyaccounting.controller.AbstractMenuController;
import ru.com.energyaccounting.model.Position;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by UserSts on 14.05.2017.
 */
public class PositionMenuController extends AbstractMenuController<Position, PositionAddOrEditController> implements Initializable{

    @FXML private TableColumn<Position, Integer> positionsColumnId;
    @FXML private TableColumn<Position, String> positionsColumnName;
    @FXML private TableColumn<Position, String> positionsColumnDescription;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);

        positionsColumnId.setCellValueFactory(cellData -> cellData.getValue().idProperty().asObject());
        positionsColumnName.setCellValueFactory(cellData -> cellData.getValue().nameProperty());
        positionsColumnDescription.setCellValueFactory(cellData -> cellData.getValue().descriptionProperty());
        searchObjects();
    }

    @Override
    protected Position createObj() {
        return new Position();
    }

    @Override
    protected String editOrAddLayout() {
        return Main.mPositionAddOrEditLayout;
    }

    @Override
    protected String getPathForJasperPattern() {
        return null;
    }
}
