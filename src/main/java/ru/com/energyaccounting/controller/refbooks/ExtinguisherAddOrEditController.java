package ru.com.energyaccounting.controller.refbooks;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import ru.com.energyaccounting.Main;
import ru.com.energyaccounting.controller.AbstractAddOrEditController;
import ru.com.energyaccounting.dao.PersistException;
import ru.com.energyaccounting.model.*;

import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

/**
 * Created by UserSts on 30.05.2017.
 */
public class ExtinguisherAddOrEditController extends AbstractAddOrEditController<Extinguisher> implements Initializable{

    @FXML private TextField     inventoryNumberTextField;
    @FXML private Label         extTypeLabel;
    @FXML private Label         lastTestDateLabel;
    @FXML private Label         nextTestDateLabel;
    @FXML private Label         electricalInstallationLabel;
    private Extinguisher extinguisher;

    public ExtinguisherAddOrEditController() {
        super(Extinguisher.class.getName());
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
    @Override
    public boolean isInputValid() {
        String errorMessage = "";

        if (extTypeLabel.getText() == null || extTypeLabel.getText().length() == 0){
            errorMessage += "Отсутствует тип огнетушителя!\n";
        }
        if (electricalInstallationLabel.getText() == null || electricalInstallationLabel.getText().length() == 0 ){
            errorMessage +="Отсутствует Электроустановка!\n";
        }
        return warningMessage(errorMessage);
    }

    @Override
    public void handleOK() throws PersistException {
        String inventoryNumber = inventoryNumberTextField.getText();
        String extType = extTypeLabel.getText();
        String electricalInstallation = electricalInstallationLabel.getText();
        String description = descriptionTextField.getText();
        if (isInputValid()){
            extinguisher.setInventoryNumber(inventoryNumber);
            extinguisher.setExtinguisherType(extType);
            extinguisher.setElectricalInstallation(electricalInstallation);
            extinguisher.setDescription(description);
            objectPersistUpdate(extinguisher);

        }
    }

    @Override
    public void setObject(Extinguisher extinguisher) {
        this.extinguisher = extinguisher;
        this.dialogStage.setTitle("Редактирование: " + this.extinguisher.getDialogStageTitle());
        idLabel.setText(Integer.toString(extinguisher.getId()));
        inventoryNumberTextField.setText(extinguisher.getInventoryNumber());
        extTypeLabel.setText(extinguisher.getExtinguisherType());
        nextTestDateLabel.setText(extinguisher.getNextTestDate()!=null ? extinguisher.getNextTestDate().toString(): "Неизвестно");
        lastTestDateLabel.setText(extinguisher.getLastTestDate()!=null ? extinguisher.getLastTestDate().toString(): "Неизвестно");;
        electricalInstallationLabel.setText(extinguisher.getElectricalInstallation());
        descriptionTextField.setText(extinguisher.getDescription());
        statusLabel.setText(extinguisher.getStatus());
    }

    @Override
    public void threeDotsExtinguisherType() {
        ExtinguisherType extinguisherType = (ExtinguisherType) showLayoutSelectMenu(Main.mExtinguisherTypeMenuLayout);
        if (extinguisherType != null) {
            extTypeLabel.setText(extinguisherType.getName());
            extinguisher.setExt_type_id(extinguisherType.getId());
        }
    }

    @Override
    public void threeDotsElectricalInstallation() {
        ElectricalInstallation electricalInstallation = (ElectricalInstallation) showLayoutSelectMenu(Main.mElectricalInstallationsMenuLayout);
        if (electricalInstallation != null) {
            electricalInstallationLabel.setText(electricalInstallation.getName());
            extinguisher.setEl_install_id(electricalInstallation.getId());
        }
    }
}
