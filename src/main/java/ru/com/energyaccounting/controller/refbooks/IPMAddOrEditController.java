package ru.com.energyaccounting.controller.refbooks;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import ru.com.energyaccounting.Main;
import ru.com.energyaccounting.controller.AbstractAddOrEditController;
import ru.com.energyaccounting.dao.PersistException;
import ru.com.energyaccounting.model.*;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by UserSts on 22.05.2017.
 */
public class IPMAddOrEditController extends AbstractAddOrEditController<IPM> implements Initializable {


    @FXML private TextField inventoryNumberTextField;
    @FXML private Label     ipmTypeLabel;
    @FXML private Label     voltageTypeLabel;
    @FXML private Label     lastTestDateLabel;
    @FXML private Label     nextTestDateLabel;
    @FXML private Label     electricalInstallationLabel;


    private IPM ipm;

    public IPMAddOrEditController() {
        super(IPM.class.getName());
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    @Override
    public boolean isInputValid() {
        String errorMessage = "";
        if (ipmTypeLabel.getText() == null || ipmTypeLabel.getText().length() == 0) {
            errorMessage += "Отсутствует Тип СИЗа!\n";
        }
        if (voltageTypeLabel.getText() == null || voltageTypeLabel.getText().length() == 0) {
            errorMessage += "Отсутствует Класс напряжения!\n";
        }
        if (electricalInstallationLabel.getText() == null || electricalInstallationLabel.getText().length() == 0) {
            errorMessage += "Отсутствует Электроустановка!\n";
        }
        return warningMessage(errorMessage);
    }

    @Override
    public void handleOK() throws PersistException {
        String inventoryNumber = inventoryNumberTextField.getText();
        String ipmType = ipmTypeLabel.getText();
        String voltageType = voltageTypeLabel.getText();
        String electrInstall = electricalInstallationLabel.getText();
        if (isInputValid()) {
            ipm.setInventoryNumber(inventoryNumber);
            ipm.setImpType(ipmType);
            ipm.setVoltageType(voltageType);
            ipm.setElectricalInstallation(electrInstall);

            objectPersistUpdate(ipm);

        }
    }

    @Override
    public void setObject(IPM ipm) {
        this.ipm = ipm;
        this.dialogStage.setTitle("Редактирование: " + this.ipm.getDialogStageTitle());
        idLabel.setText(Integer.toString(ipm.getId()));
        inventoryNumberTextField.setText(ipm.getInventoryNumber());
        ipmTypeLabel.setText(ipm.getImpType());
        voltageTypeLabel.setText(ipm.getVoltageType());
        lastTestDateLabel.setText(ipm.getLastTestDate() != null ? ipm.getLastTestDate().toString() : "Неизвестно");
        nextTestDateLabel.setText(ipm.getNextTestDate() != null ? ipm.getNextTestDate().toString() : "Неизвестно");
        electricalInstallationLabel.setText(ipm.getElectricalInstallation());
        descriptionTextField.setText(ipm.getDescription());

        statusLabel.setText(ipm.getStatus());


    }

    @Override
    public void threeDotsVoltageType() {
        VoltageType voltageType = (VoltageType) showLayoutSelectMenu(Main.mVoltageTypeMenuLayout);
        if (voltageType != null) {
            voltageTypeLabel.setText(voltageType.getName());
            ipm.setVt_id(voltageType.getId());
        }
    }

    @Override
    public void threeDotsIPMType() {
        IPMType ipmType = (IPMType) showLayoutSelectMenu(Main.mIpmTypesMenuLayout);
        if (ipmType != null) {
            ipmTypeLabel.setText(ipmType.getName());
            ipm.setImp_type_id(ipmType.getId());
        }
    }

    @Override
    public void threeDotsElectricalInstallation() {
        ElectricalInstallation electricalInstallation = (ElectricalInstallation) showLayoutSelectMenu(Main.mElectricalInstallationsMenuLayout);
        if (electricalInstallation != null) {
            electricalInstallationLabel.setText(electricalInstallation.getName());
            ipm.setEl_install_id(electricalInstallation.getId());
        }
    }
}
