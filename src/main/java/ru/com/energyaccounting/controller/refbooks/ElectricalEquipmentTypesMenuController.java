package ru.com.energyaccounting.controller.refbooks;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import ru.com.energyaccounting.Main;
import ru.com.energyaccounting.controller.AbstractMenuController;
import ru.com.energyaccounting.model.ElectricalEquipmentType;

import java.net.URL;
import java.util.ResourceBundle;


public class ElectricalEquipmentTypesMenuController
        extends AbstractMenuController<ElectricalEquipmentType, ElectricalEquipmentTypesAddOrEditController>
        implements Initializable {

    @FXML private TableColumn<ElectricalEquipmentType, Integer> electricalEquipmentTypeColumnId;
    @FXML private TableColumn<ElectricalEquipmentType, String> electricalEquipmentTypeColumnName;
    @FXML private TableColumn<ElectricalEquipmentType, String> electricalEquipmentTypeColumnDescription;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);
        electricalEquipmentTypeColumnId.setCellValueFactory(cellData -> cellData.getValue().idProperty().asObject());
        electricalEquipmentTypeColumnName.setCellValueFactory(cellData -> cellData.getValue().nameProperty());
        electricalEquipmentTypeColumnDescription.setCellValueFactory(cellData -> cellData.getValue().descriptionProperty());
        searchObjects();
    }

    @Override
    protected ElectricalEquipmentType createObj() {
        return new ElectricalEquipmentType();
    }

    @Override
    protected String editOrAddLayout() {
        return Main.mElectricalEquipmentTypesAddOrEditLayout;
    }

    @Override
    protected String getPathForJasperPattern() {
        return null;
    }
}
