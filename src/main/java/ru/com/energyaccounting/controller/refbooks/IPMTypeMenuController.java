package ru.com.energyaccounting.controller.refbooks;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import ru.com.energyaccounting.Main;
import ru.com.energyaccounting.controller.AbstractMenuController;
import ru.com.energyaccounting.model.IPMType;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by UserSts on 22.05.2017.
 */
public class IPMTypeMenuController extends AbstractMenuController<IPMType,IPMTypesAddOrEditController> implements Initializable{

    @FXML private TableColumn<IPMType, Integer> ipmTypesColumnId;
    @FXML private TableColumn<IPMType, String> ipmTypesColumnName;



    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);

        ipmTypesColumnId.setCellValueFactory(cellData -> cellData.getValue().idProperty().asObject());
        ipmTypesColumnName.setCellValueFactory(cellData -> cellData.getValue().nameProperty());
        searchObjects();
    }

    @Override
    protected IPMType createObj() {
        return new IPMType();
    }

    @Override
    protected String editOrAddLayout() {
        return Main.mIpmTypeAddOrEditLayout;
    }

    @Override
    protected String getPathForJasperPattern() {
        return null;
    }
}
