package ru.com.energyaccounting.controller.refbooks;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import ru.com.energyaccounting.controller.AbstractAddOrEditController;
import ru.com.energyaccounting.dao.PersistException;
import ru.com.energyaccounting.model.Position;

/**
 * Created by UserSts on 14.05.2017.
 */
public class PositionAddOrEditController extends AbstractAddOrEditController<Position> {

    @FXML private Label idLabel;
    @FXML private TextField nameTextField;
    @FXML private TextField descriptionTextField;

    private Position position;

    public PositionAddOrEditController() {
        super(Position.class.getName());
    }

    @Override
    public boolean isInputValid() {
        String errorMessage = "";

        if (nameTextField == null || nameTextField.getText().length() == 0){
            errorMessage += "Отсутствует найменование Организации!\n";
        }

        return warningMessage(errorMessage);
    }

    @Override
    public void handleOK() throws PersistException {

        String name = nameTextField.getText();
        String description = descriptionTextField.getText();
        if (isInputValid()){
            position.setName(name);
            position.setDescription(description);
            objectPersistUpdate(position);

        }
    }

    @Override
    public void setObject(Position position) {
        this.position = position;
        this.dialogStage.setTitle("Редактирование: " + this.position.getDialogStageTitle());
        idLabel.setText(Integer.toString(position.getId()));
        nameTextField.setText(position.getName());
        descriptionTextField.setText(position.getDescription());
    }


}
