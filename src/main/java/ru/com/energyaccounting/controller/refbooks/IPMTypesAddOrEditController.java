package ru.com.energyaccounting.controller.refbooks;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import ru.com.energyaccounting.controller.AbstractAddOrEditController;
import ru.com.energyaccounting.dao.PersistException;
import ru.com.energyaccounting.model.IPMType;

/**
 * Created by UserSts on 22.05.2017.
 */
public class IPMTypesAddOrEditController extends AbstractAddOrEditController<IPMType> {
    @FXML private Label idLabel;
    @FXML private TextField nameTextField;
    private IPMType ipmType;

    public IPMTypesAddOrEditController() {
        super(IPMType.class.getName());
    }

    @Override
    public boolean isInputValid() {
        String errorMessage = "";

        if (nameTextField == null || nameTextField.getText().length() == 0){
            errorMessage += errorTextMessage + "Типа СИЗа!\n";
        }
        return warningMessage(errorMessage);
    }

    @Override
    public void handleOK() throws PersistException {
        String name = nameTextField.getText();
        if (isInputValid()){
            ipmType.setName(name);
            objectPersistUpdate(ipmType);
        }
    }

    @Override
    public void setObject(IPMType ipmType) {
        this.ipmType = ipmType;
        this.dialogStage.setTitle(edit + this.ipmType.getDialogStageTitle());
        idLabel.setText(Integer.toString(ipmType.getId()));
        nameTextField.setText(ipmType.getName());
    }
}
