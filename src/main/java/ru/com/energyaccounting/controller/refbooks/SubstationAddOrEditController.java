package ru.com.energyaccounting.controller.refbooks;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import ru.com.energyaccounting.Main;
import ru.com.energyaccounting.controller.AbstractAddOrEditController;
import ru.com.energyaccounting.dao.PersistException;
import ru.com.energyaccounting.model.Organization;
import ru.com.energyaccounting.model.Substation;

/**
 * Created by UserSts on 19.05.2017.
 */
public class SubstationAddOrEditController extends AbstractAddOrEditController<Substation> {
    @FXML private Label idLabel;
    @FXML private Label organizationLabel;
    @FXML private TextField nameTextField;
    private Substation substation;


    public SubstationAddOrEditController() {
        super(Substation.class.getName());
    }

    @Override
    public boolean isInputValid() {
        String errorMessage = "";
        if (nameTextField.getText() == null || nameTextField.getText().length() == 0){
            errorMessage += "Отсутствует найменование подстанции!\n";
        }

        return warningMessage(errorMessage);
    }

    @Override
    public void handleOK() throws PersistException {

        String name = nameTextField.getText();
        String organization = organizationLabel.getText();
        if (isInputValid()){
            substation.setName(name);
            substation.setOrganization(organization);

           objectPersistUpdate(substation);
        }
    }

    @Override
    public void setObject(Substation substation) {
        this.substation = substation;
        this.dialogStage.setTitle("Редактирование: " + this.substation.getDialogStageTitle());
        idLabel.setText(Integer.toString(substation.getId()));
        nameTextField.setText(substation.getName());
        organizationLabel.setText(substation.getOrganization());
    }

    @Override
    public void threeDotsOrganization() {
        Organization organization = (Organization) showLayoutSelectMenu(Main.mOrganizationsMenuLayout);
        if (organization != null) {
            organizationLabel.setText(organization.getName());
            substation.setOrg_id(organization.getId());
        }
    }
}
