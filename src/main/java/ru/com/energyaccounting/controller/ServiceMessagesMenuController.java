package ru.com.energyaccounting.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import ru.com.energyaccounting.model.ServiceMessage;
import ru.com.energyaccounting.util.DBUtilAuditStatic;

import java.net.URL;
import java.time.LocalDateTime;
import java.util.ResourceBundle;

import static ru.com.energyaccounting.util.DBUtilAuditStatic.dbConnect;

/**
 * Created by UserSts on 04.05.2017.
 */
public class ServiceMessagesMenuController extends AbstractMenuController<ServiceMessage, blankAddOrEditController> implements Initializable {

    @FXML
    private TableColumn<ServiceMessage, Integer> smColumnId;
    @FXML
    private TableColumn<ServiceMessage, LocalDateTime> smColumnEventDateTime;
    @FXML
    private TableColumn<ServiceMessage, String> smColumnMessageType;
    @FXML
    private TableColumn<ServiceMessage, String> smColumnDescription;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);

        smColumnId.setCellValueFactory(cellData -> cellData.getValue().idProperty().asObject());
        smColumnEventDateTime.setCellValueFactory(cellData -> cellData.getValue().eventDateTimeProperty());
        smColumnMessageType.setCellValueFactory(cellData -> cellData.getValue().messageTypeProperty());
        smColumnDescription.setCellValueFactory(cellData -> cellData.getValue().descriptionProperty());

        searchObjects(dbConnect(true));
    }

    @Override
    protected ServiceMessage createObj() {
        return new ServiceMessage();
    }

    @Override
    protected String editOrAddLayout() {
        return null;
    }

    @Override
    protected String getPathForJasperPattern() {
        return null;
    }
}
