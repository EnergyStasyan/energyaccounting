package ru.com.energyaccounting.controller.docs;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.ComboBoxTableCell;
import javafx.scene.control.cell.TextFieldTableCell;
import ru.com.energyaccounting.Main;
import ru.com.energyaccounting.controller.AbstractAddOrEditController;
import ru.com.energyaccounting.dao.CRUDDao;
import ru.com.energyaccounting.dao.GenericDao;
import ru.com.energyaccounting.dao.PersistException;
import ru.com.energyaccounting.model.Extinguisher;
import ru.com.energyaccounting.model.ExtinguisherDoc;
import ru.com.energyaccounting.model.ExtinguisherDocExting;
import tornadofx.control.DatePickerTableCell;
import tornadofx.control.DateTimePicker;

import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import static ru.com.energyaccounting.controller.docs.IpmDocAddOrEditController.statusTempList;

public class ExtinguisherDocAddOrEditController extends AbstractAddOrEditController<ExtinguisherDoc> implements Initializable {
    private static final Logger LOG = Logger.getLogger(ExtinguisherDocAddOrEditController.class.getName());

    @FXML private DateTimePicker docDataTimePicker;
    @FXML private Label idLabel;
    @FXML private TableView<ExtinguisherDocExting> docTableView;
    @FXML private TableColumn<ExtinguisherDocExting, String> inventNumbColumn;
    @FXML private TableColumn<ExtinguisherDocExting, String> infoColumn;
    @FXML private TableColumn<ExtinguisherDocExting, LocalDate> lastTestColumn;
    @FXML private TableColumn<ExtinguisherDocExting, LocalDate> nextTestColumn;
    @FXML private TableColumn<ExtinguisherDocExting, String> statusColumn;
    @FXML private TableColumn<ExtinguisherDocExting, String> commentColumn;
    @FXML private TextField descriptionTextField;

    private ExtinguisherDoc extinguisherDoc;

    private ObservableList<ExtinguisherDocExting> extinguisherDocExtinguisherList = FXCollections.observableArrayList();
    private ObservableList<ExtinguisherDocExting> extinguisherDocExtinguisherRemovedList = FXCollections.observableArrayList();


    public ExtinguisherDocAddOrEditController() {
        super(ExtinguisherDoc.class.getName());
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        inventNumbColumn.setCellValueFactory(param -> param.getValue().inventoryNumberProperty());
        infoColumn.setCellValueFactory(param -> param.getValue().nameProperty());

        lastTestColumn.setCellValueFactory(param -> param.getValue().lastTestDateProperty());
        lastTestColumn.setCellFactory(DatePickerTableCell.forTableColumn());
        lastTestColumn.setOnEditCommit((TableColumn.CellEditEvent<ExtinguisherDocExting, LocalDate> t)->{
            t.getTableView().getItems().get(t.getTablePosition().getRow()).setLastTestDate(t.getNewValue());
        });

        nextTestColumn.setCellValueFactory(param -> param.getValue().nextTestDateProperty());
        nextTestColumn.setCellFactory(DatePickerTableCell.forTableColumn());
        nextTestColumn.setOnEditCommit((TableColumn.CellEditEvent<ExtinguisherDocExting, LocalDate> t)->{
            t.getTableView().getItems().get(t.getTablePosition().getRow()).setNextTestDate(t.getNewValue());
        });

        statusColumn.setCellValueFactory(param -> param.getValue().statusProperty());
        statusColumn.setCellFactory(ComboBoxTableCell.forTableColumn(statusTempList));
        statusColumn.setOnEditCommit((TableColumn.CellEditEvent<ExtinguisherDocExting,String> t) ->{
            t.getTableView().getItems().get(t.getTablePosition().getRow()).setStatus(t.getNewValue());
        });

        commentColumn.setCellValueFactory(param -> param.getValue().descriptionProperty());
        commentColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        commentColumn.setOnEditCommit((TableColumn.CellEditEvent<ExtinguisherDocExting, String> t) ->
                t.getTableView().getItems().get(t.getTablePosition().getRow()).setDescription(t.getNewValue())
        );
    }


    @Override
    public void setObject(ExtinguisherDoc extinguisherDoc) {
        this.extinguisherDoc = extinguisherDoc;
        this.dialogStage.setTitle("Редактирование: " + this.extinguisherDoc.getDialogStageTitle());
        String docNumber = Integer.toString(extinguisherDoc.getId());
        idLabel.setText(docNumber.equals("0")? "": docNumber);
        docDataTimePicker.setDateTimeValue(extinguisherDoc.getDocDateTime()!=null ? extinguisherDoc.getDocDateTime() : LocalDateTime.now());
        descriptionTextField.setText(extinguisherDoc.getDescription());
        extinguisherDocExtinguisherList = getIpmDocIpmListFromDao();

        populateTables();
    }

    @Override
    public boolean isInputValid() {
        return true;
    }

    @Override
    public void handleOK() {
        LOG.info("нажали Ок");

        String description = descriptionTextField.getText();
        LocalDateTime docDateTime = (docDataTimePicker.getDateTimeValue()!=null? docDataTimePicker.getDateTimeValue(): LocalDateTime.now());
        extinguisherDoc.setDescription(description);
        extinguisherDoc.setDocDateTime(docDateTime);
        try {
            objectPersistUpdate(extinguisherDoc);
            ExtinguisherDoc extinguisherDocFromDao = getObjectFromDao();
            extinguisherDocExtinguisherList.forEach((ede)->ede.setDoc_id(extinguisherDocFromDao.getId())); // FIXME: 18.06.2018  рефакторинг
            extinguisherDocExtinguisherList.forEach(ede ->  {
                try{
                    new CRUDDao<>(ede).objectPersistUpdate();
                }catch (PersistException e){
                    e.printStackTrace();
                }
            });

            extinguisherDocExtinguisherRemovedList.forEach(ede -> {
                try {
                    new CRUDDao<>(ede).remove();
                }catch (PersistException e){
                    e.printStackTrace();
                }
            });

        }catch (PersistException e){
            LOG.severe("Ошибка записи");
            e.printStackTrace();
        }

    }


    //Identifier
    @SuppressWarnings("unchecked")
    private ObservableList<ExtinguisherDocExting> getIpmDocIpmListFromDao(){
        ObservableList<ExtinguisherDocExting> result = FXCollections.observableArrayList();
        try {
            GenericDao<ExtinguisherDocExting, Integer> daoExtinguisher = factory.getDao(connection, ExtinguisherDocExting.class);
            result = daoExtinguisher.getOLByPk(extinguisherDoc.getId());
        }catch (PersistException e){
            e.printStackTrace();
            LOG.warning(e.getMessage());
        }
        return result;
    }

    private void populateTables(){
        docTableView.setItems(extinguisherDocExtinguisherList);
    }

    @FXML private void addIpmToIpmList(){
        Extinguisher extinguisher =(Extinguisher) showLayoutSelectMenu(Main.mExtinguisherMenuLayout);
        if (extinguisher != null) {
            ExtinguisherDocExting extinguisherDocExting = ipmToIpmDocIpmAdapter(extinguisher);
            int findDuplicate = extinguisherDocExting.getExt_id();
            if (extinguisherDocExtinguisherList.stream().noneMatch(ip ->(ip.getExt_id() == findDuplicate))) {
                extinguisherDocExtinguisherList.add(extinguisherDocExting);
                extinguisherDocExtinguisherRemovedList.remove(extinguisherDocExting);
            }
        }
    }

    private ExtinguisherDocExting ipmToIpmDocIpmAdapter(Extinguisher ext){
        ExtinguisherDocExting  ede = new ExtinguisherDocExting();
        ede.setExt_id(ext.getId());
        ede.setName(ext.getElectricalInstallation() + ", " + ext.getExtinguisherType() + ", " + ext.getExtinguisherType());
        ede.setDescription(ext.getDescription());
        ede.setInventoryNumber(ext.getInventoryNumber());

        ede.setLastTestDate(ext.getNextTestDate());
        ede.setExt_type_id(ext.getExt_type_id());
        ede.setExtinguisherType(ext.getExtinguisherType());
        ede.setEl_install_id(ext.getEl_install_id());
        ede.setElectricalInstallation(ext.getElectricalInstallation());
        ede.setStatus(ext.getStatus());

        return ede;
    }

    @FXML private void deleteElementFromIpmList(){
        Object objectWasDeleted = deleteElementFromList(docTableView, extinguisherDocExtinguisherList);
        if (objectWasDeleted instanceof ExtinguisherDocExting) {
            ExtinguisherDocExting result = (ExtinguisherDocExting) objectWasDeleted;
            if (result.getId()!=0) {
                extinguisherDocExtinguisherRemovedList.add(result);
            }
        }
    }

}
