package ru.com.energyaccounting.controller.docs;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import ru.com.energyaccounting.Main;
import ru.com.energyaccounting.controller.AbstractMenuController;
import ru.com.energyaccounting.dao.AbstractJDBCDao;
import ru.com.energyaccounting.model.ElectricalEquipmentDoc;

import java.net.URL;
import java.time.LocalDateTime;
import java.util.ResourceBundle;

public class ElectricalEquipmentDocMenu extends AbstractMenuController<ElectricalEquipmentDoc, ElectricalEquipmentDocAddOrEditController> implements Initializable {

    @FXML private TableColumn<ElectricalEquipmentDoc, Integer> electricalEquipmentDocColumnId;
    @FXML private TableColumn<ElectricalEquipmentDoc, LocalDateTime> electricalEquipmentDocColumnDocDate;
    @FXML private TableColumn<ElectricalEquipmentDoc, String> electricalEquipmentDocDescription;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);
        electricalEquipmentDocColumnId.setCellValueFactory(cellData -> cellData.getValue().idProperty().asObject());
        electricalEquipmentDocDescription.setCellValueFactory(cellData -> cellData.getValue().descriptionProperty());
        electricalEquipmentDocColumnDocDate.setCellValueFactory(cellData -> cellData.getValue().docDateTimeProperty());
        electricalEquipmentDocColumnDocDate.setCellFactory(col -> new TableCell<ElectricalEquipmentDoc, LocalDateTime>(){ //TODO сделать сссылочный метод
            @Override
            protected void updateItem(LocalDateTime item, boolean empty) {
                super.updateItem(item, empty);
                if (empty){
                    setText(null);
                }else {
                    setText(item.format(AbstractJDBCDao.dtfDateTime));
                }
            }
        });

        searchObjects();
    }

    @Override
    protected ElectricalEquipmentDoc createObj() {
        return new ElectricalEquipmentDoc();
    }

    @Override
    protected String editOrAddLayout() {
        return Main.mElectricalEquipmentDocAddOrEditLayout;
    }

    @Override
    protected String getPathForJasperPattern() {
        throw new NullPointerException();
    }
}
