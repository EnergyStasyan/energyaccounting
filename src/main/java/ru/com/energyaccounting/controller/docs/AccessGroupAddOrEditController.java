package ru.com.energyaccounting.controller.docs;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import ru.com.energyaccounting.Main;
import ru.com.energyaccounting.controller.AbstractAddOrEditController;
import ru.com.energyaccounting.dao.PersistException;
import ru.com.energyaccounting.model.AccessGroup;
import ru.com.energyaccounting.model.Person;

import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
import java.util.logging.Logger;

/**
 * Created by UserSts on 06.07.2017.
 */
public class AccessGroupAddOrEditController extends AbstractAddOrEditController<AccessGroup> implements Initializable {
    private static final Logger LOG = Logger.getLogger(AccessGroupAddOrEditController.class.getName());

    @FXML private Label         idLabel;
    @FXML private TextField     numberOrderTextField;
    @FXML private DatePicker    orderDateDP;
    @FXML private DatePicker    lastTestDateDP;
    @FXML private DatePicker    nextTestDateDP;
    @FXML private ChoiceBox<String>     accessGroupChoiceBox;
    @FXML private Label         personLabel;
    @FXML private Label         directorLabel;
    @FXML private TextField     descriptionTextField;

    private AccessGroup accessGroup;
    private final ObservableList<String> accessGroupList = FXCollections.observableArrayList("I", "II", "II", "IV", "V", "VI");

    public AccessGroupAddOrEditController() {
        super(AccessGroup.class.getName());
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        accessGroupChoiceBox.setItems(accessGroupList);
    }

    @Override
    public boolean isInputValid() {
        String errorMessage = "";
        if (numberOrderTextField.getText() == null || numberOrderTextField.getLength() == 0) {
            errorMessage += "Отсутствует Номер приказа!\n";
        }
        if (orderDateDP.getValue() == null) {
            errorMessage += "Отсутствует Дата приказа!\n";
        }
        if (nextTestDateDP.getValue() == null) {
            errorMessage += "Отсутствует Дата следующего экзамена!\n";
        }
        if (personLabel.getText() == null) {
            errorMessage += "Отсутствует Сотрудник!\n";
        }

        return warningMessage(errorMessage);
    }

    @Override
    public void handleOK() throws PersistException {
        LOG.info("нажали Ок");

        String numberOfOrder = numberOrderTextField.getText();
        LocalDate lastTestDate = lastTestDateDP.getValue();
        LocalDate nextTestDate = nextTestDateDP.getValue();
        LocalDate orderDate = orderDateDP.getValue();
        String accessGroupInt = accessGroupChoiceBox.getValue();
        String person = personLabel.getText();
        String director = directorLabel.getText();
        String description = descriptionTextField.getText();

        if (isInputValid()) {
            accessGroup.setNumberOfOrder(numberOfOrder);
            accessGroup.setLastTestDate(lastTestDate);
            accessGroup.setNextTestDate(nextTestDate);
            accessGroup.setOrderDate(orderDate);
            accessGroup.setNumberOfAccessGroup(accessGroupInt);
            accessGroup.setPerson(person);
            accessGroup.setDirector(director);
            accessGroup.setDescription(description);

            objectPersistUpdate(accessGroup);
        }
    }

    @Override
    public void setObject(AccessGroup accessGroup) {
        this.accessGroup = accessGroup;
        this.dialogStage.setTitle("Редактирование: " + this.accessGroup.getDialogStageTitle());

        idLabel.setText(Integer.toString(accessGroup.getId()));
        numberOrderTextField.setText(accessGroup.getNumberOfOrder());
        orderDateDP.setValue(accessGroup.getOrderDate());
        lastTestDateDP.setValue(accessGroup.getLastTestDate());
        nextTestDateDP.setValue(accessGroup.getNextTestDate());
        accessGroupChoiceBox.setValue(accessGroup.getNumberOfAccessGroup());
        personLabel.setText(accessGroup.getPerson());
        directorLabel.setText(accessGroup.getDirector());
        descriptionTextField.setText(accessGroup.getDescription());

    }

    @Override
    public void threeDotsPerson() {
        Person person = (Person) showLayoutSelectMenu(Main.mPersonMenuLayout);
        if (person != null) {
            personLabel.setText(person.getSurname() + " " + person.getName() + " " + person.getMiddleName());
            accessGroup.setPersonId(person.getId());
        }
    }

    public void threeDotsDirector() {
        Person person = (Person) showLayoutSelectMenu(Main.mPersonMenuLayout);

        if (person != null) {
            directorLabel.setText(person.getSurname() + " " + person.getName() + " " + person.getMiddleName());
            accessGroup.setDirectorId(person.getId());
        }
    }

}
