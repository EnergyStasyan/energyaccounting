package ru.com.energyaccounting.controller.docs;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import ru.com.energyaccounting.Main;
import ru.com.energyaccounting.controller.AbstractMenuController;
import ru.com.energyaccounting.dao.AbstractJDBCDao;
import ru.com.energyaccounting.dao.PersistException;
import ru.com.energyaccounting.model.ExtinguisherDoc;

import java.net.URL;
import java.time.LocalDateTime;
import java.util.ResourceBundle;

// FIXME: 09.06.2018  собрать все документы под один абстрактный класс
public class ExtinguisherDocMenuController extends AbstractMenuController<ExtinguisherDoc, ExtinguisherDocAddOrEditController> implements Initializable {

    @FXML private TableColumn<ExtinguisherDoc, Integer> extinguisherDocColumnId;
    @FXML private TableColumn<ExtinguisherDoc, LocalDateTime> extinguisherDocColumnDocDate;
    @FXML private TableColumn<ExtinguisherDoc, String> extinguisherDocDescription;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);
        extinguisherDocColumnId.setCellValueFactory(cellData -> cellData.getValue().idProperty().asObject());
        extinguisherDocDescription.setCellValueFactory(cellData -> cellData.getValue().descriptionProperty());
        extinguisherDocColumnDocDate.setCellValueFactory(cellData -> cellData.getValue().docDateTimeProperty());
        extinguisherDocColumnDocDate.setCellFactory(col -> new TableCell<ExtinguisherDoc, LocalDateTime>(){
            @Override
            protected void updateItem(LocalDateTime item, boolean empty) {
                super.updateItem(item, empty);
                if (empty){
                    setText(null);
                }else {
                    setText(item.format(AbstractJDBCDao.dtfDateTime));
                }
            }
        });

        searchObjects();
    }
    @Override
    protected ExtinguisherDoc createObj() {
        return new ExtinguisherDoc();
    }

    @Override
    protected String editOrAddLayout() {
        return Main.mExtinguisherDocAddOrEditLayout;
    }

    @Override
    protected String getPathForJasperPattern() {
        throw new NullPointerException();
    }
}
