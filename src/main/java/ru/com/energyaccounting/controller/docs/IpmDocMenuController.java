package ru.com.energyaccounting.controller.docs;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import ru.com.energyaccounting.Main;
import ru.com.energyaccounting.controller.AbstractMenuController;
import ru.com.energyaccounting.dao.AbstractJDBCDao;
import ru.com.energyaccounting.model.IpmDoc;

import java.net.URL;
import java.time.LocalDateTime;
import java.util.ResourceBundle;

public class IpmDocMenuController extends AbstractMenuController<IpmDoc, IpmDocAddOrEditController> implements Initializable {

    @FXML private TableColumn<IpmDoc, Integer> ipmDocColumnId;
    @FXML private TableColumn<IpmDoc, LocalDateTime> ipmDocColumnDocDate;
    @FXML private TableColumn<IpmDoc, String> ipmDocDescription;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);
        ipmDocColumnId.setCellValueFactory(cellData -> cellData.getValue().idProperty().asObject());
        ipmDocDescription.setCellValueFactory(cellData -> cellData.getValue().descriptionProperty());
        ipmDocColumnDocDate.setCellValueFactory(cellData -> cellData.getValue().docDateTimeProperty());
        ipmDocColumnDocDate.setCellFactory(col -> new TableCell<IpmDoc, LocalDateTime>(){
            @Override
            protected void updateItem(LocalDateTime item, boolean empty) {
                super.updateItem(item, empty);
                if (empty){
                    setText(null);
                }else {
                    setText(item.format(AbstractJDBCDao.dtfDateTime));
                }
            }
        });

        searchObjects();
    }

    @Override
    protected IpmDoc createObj() {
        return new IpmDoc();
    }

    @Override
    protected String editOrAddLayout() {
        return Main.mIpmDocAddOrEditLayout;
    }

    @Override
    protected String getPathForJasperPattern() {
        throw new NullPointerException();
    }
}
