package ru.com.energyaccounting.controller.docs;


import ru.com.energyaccounting.dao.CRUDDao;
import ru.com.energyaccounting.util.TimeSpinner;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.image.Image;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import ru.com.energyaccounting.Main;
import ru.com.energyaccounting.controller.AbstractAddOrEditController;
import ru.com.energyaccounting.controller.ServiceMessagesController;
import ru.com.energyaccounting.controller.StartChoiceBaseController;
import ru.com.energyaccounting.controller.SubstationElInstallationElEquipmentController;
import ru.com.energyaccounting.dao.GenericDao;
import ru.com.energyaccounting.dao.PersistException;
import ru.com.energyaccounting.model.*;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ResourceBundle;
import java.util.logging.Logger;


/**
 * Created by UserSts on 20.05.2017.
 */
public class OrderDocAddOrEditController extends AbstractAddOrEditController<OrderDoc> implements Initializable{
    private static final Logger LOG = Logger.getLogger(OrderDocAddOrEditController.class.getName());

    TreeItem<String> rootNode;
    @FXML private TextField numberTextField;
    @FXML private Label substationLabel;
    @FXML private Label subdivisionLabel;
    @FXML private Label organizationLabel;
    @FXML private Label performingAuthorityLabel; //ответственный руководитель работ
    @FXML private Label issueWorkPermitManLabel; //Выдающий наряд
    @FXML private Label allowingManLabel;           //допускающий
    @FXML private Label foremanLabel;               //производиель работ
    @FXML private Label viewerLabel;                //наблюдающий
    @FXML private TextField descriptionTextField;
    @FXML private TextField instructToTextField;      //поручается перечисление оборудование
    @FXML private TextArea instructToTextArea;      //поручается описание работы

    @FXML private TableView<OrderDocPerson> orderDocPersonTableView;
    @FXML private TableColumn<OrderDocPerson, String> orderDocPersonColumnPerson;
    @FXML private TableColumn<OrderDocPerson, String> orderDocPersonColumnAdditional;

    @FXML private TreeView<String> elInstallationElEquipmentTreeView;

    @FXML private TableView<OrderDocElectricalEquipment> orderDocElectricalEquipmentTableView;
    @FXML private TableColumn<OrderDocElectricalEquipment, String> orderDocElectricalEquipmentColumnElectricalInstallation;
    @FXML private TableColumn<OrderDocElectricalEquipment, String> orderDocElectricalEquipmentColumnAdditional;


    @FXML private DatePicker dateStartDP;
    @FXML private DatePicker dateEndDP;

    @FXML private TimeSpinner timeStartTS;
    @FXML private TimeSpinner timeEndTS;

    @FXML private DatePicker dateOfIssueDP;
    @FXML private TimeSpinner timeOfIssueTS;

    private OrderDoc orderDoc;

    private ObservableList<OrderDocPerson> crewObservableList = FXCollections.observableArrayList();
    private ObservableList<OrderDocPerson> allPersonsObservableList = FXCollections.observableArrayList();
    private ObservableList<OrderDocElectricalEquipment> orderDocElectricalEquipmentObservableList = FXCollections.observableArrayList();

    public OrderDocAddOrEditController() {
        super(OrderDoc.class.getName());
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //меняем значание поля dateEndDP на красный, если дата dateEndDP раньше, чем dateStartDP
        dateEndDP.setDayCellFactory(new Callback<DatePicker, DateCell>() {
            @Override
            public DateCell call(DatePicker param) {
                return new DateCell(){
                    @Override
                    public void updateItem(LocalDate item, boolean empty){
                        super.updateItem(item,empty);
                        setColorTextFieldDataPicker(dateEndDP,dateStartDP);
                    }
                };
            }
        });

        elInstallationElEquipmentTreeView.setOnMouseClicked(mouseEvent -> {
            if (mouseEvent.getButton().equals(MouseButton.PRIMARY)){
                if (mouseEvent.getClickCount() == 2){
                    addElementTreeViewEEtoTableViewEE();
                }
            }
        });

        populateTables();

        orderDocPersonColumnAdditional.setCellFactory(TextFieldTableCell.forTableColumn()); //TableView editable
        orderDocPersonColumnAdditional.setOnEditCommit(   //сохранение значения вбитых в таблицу в объект
                (TableColumn.CellEditEvent<OrderDocPerson, String> t) ->{
                    t.getTableView()
                            .getItems().get(
                            t.getTablePosition().getRow())
                            .setAdditional(t.getNewValue());
                });
        orderDocElectricalEquipmentColumnElectricalInstallation.setCellFactory(TextFieldTableCell.forTableColumn());
        orderDocElectricalEquipmentColumnElectricalInstallation.setOnEditCommit(
                (TableColumn.CellEditEvent<OrderDocElectricalEquipment,String> t) ->{
                    t.getTableView()
                            .getItems()
                            .get(t.getTablePosition().getRow())
                            .setElectricalEquipment(t.getNewValue());
                }
        );
        orderDocElectricalEquipmentColumnAdditional.setCellFactory(TextFieldTableCell.forTableColumn());
        orderDocElectricalEquipmentColumnAdditional.setOnEditCommit(
                (TableColumn.CellEditEvent<OrderDocElectricalEquipment, String> t) -> {
                    t.getTableView()
                            .getItems()
                            .get(t.getTablePosition().getRow())
                            .setAdditional(t.getNewValue());
                }
        );
    }

    @Override
    public boolean isInputValid() {
        String errorMessage = "";

//        if (nameTextField == null || nameTextField.getText().length() == 0){
//            errorMessage += "Отсутствует найменование Организации!\n";
//        }

        return warningMessage(errorMessage);
    }

    @Override
    public void handleOK() throws PersistException {
        LOG.info("нажали Ок");

        //String name = nameTextField.getText();
        String description = descriptionTextField.getText();
        String instructTo = instructToTextArea.getText();
        String number = numberTextField.getText();
        LocalDate dateOfIssue = dateOfIssueDP.getValue();
        LocalTime timeOfIssue = timeOfIssueTS.getValue();

        LocalDate dateStart = dateStartDP.getValue();
        LocalTime timeStart = timeStartTS.getValue();
        LocalDate dateEnd = dateEndDP.getValue();
        LocalTime timeEnd = timeEndTS.getValue();
        if (isInputValid()){
            orderDoc.setNumber(number);
            orderDoc.setInstructTo(instructTo);
            orderDoc.setDescription(description);
            orderDoc.setDateOfIssue(dateOfIssue);
            orderDoc.setTimeOfIssue(timeOfIssue);
            orderDoc.setDateStart(dateStart);
            orderDoc.setTimeStart(timeStart);
            orderDoc.setDateEnd(dateEnd);
            orderDoc.setTimeEnd(timeEnd);
            objectPersistUpdate(orderDoc);
            OrderDoc orderDocFromDao = getObjectFromDao();

            allPersonsObservableList.addAll(crewObservableList);

            for(OrderDocPerson person : allPersonsObservableList){
                if (person.getOrderDocId()==0) {
                    person.setOrderDocId(orderDocFromDao.getId());
                }
              //  прежде чем создать нужно удалить
                //записать orderDoc -> прочитать orderDoc -> записать остальные
                CRUDDao<OrderDocPerson> personCRUDDao = new CRUDDao<>(person);
                personCRUDDao.objectPersistUpdate();
            }

        }
    }

    @Override
    public void setObject(OrderDoc orderDoc) {
        LocalTime nowTime = LocalTime.now();
        LocalDate today = LocalDate.now();

        this.orderDoc = orderDoc;
        crewObservableList = getCrew();
        orderDocElectricalEquipmentObservableList = getOrderDocElectricalEquipment();
        this.dialogStage.setTitle("Редактирование: " + this.orderDoc.getDialogStageTitle());

        numberTextField.setText(orderDoc.getNumber() != null ? orderDoc.getNumber(): "");
        organizationLabel.setText(orderDoc.getOrganization());
        subdivisionLabel.setText(orderDoc.getSubdivision());
        substationLabel.setText(orderDoc.getSubstation());
        instructToTextArea.setText(orderDoc.getInstructTo());
        descriptionTextField.setText(orderDoc.getDescription());

        dateOfIssueDP.setValue(orderDoc.getDateOfIssue() != null ? orderDoc.getDateOfIssue() :today);
        timeOfIssueTS.getValueFactory().setValue(orderDoc.getTimeOfIssue() != null ? orderDoc.getTimeOfIssue(): nowTime);

        dateStartDP.setValue(orderDoc.getDateStart() != null ? orderDoc.getDateStart() : today);
        timeStartTS.getValueFactory().setValue(orderDoc.getTimeStart() != null ? orderDoc.getTimeStart(): nowTime);

        dateEndDP.setValue(orderDoc.getDateEnd() != null ? orderDoc.getDateEnd() : today);
        timeEndTS.getValueFactory().setValue(orderDoc.getTimeEnd() != null ? orderDoc.getTimeEnd(): nowTime);

        //order_doc_person_view
        performingAuthorityLabel.setText(orderDoc.getPerformingAuthorityNameAccessGroup());
        viewerLabel.setText(orderDoc.getViewerNameAccessGroup());
        performingAuthorityLabel.setText(orderDoc.getPerformingAuthorityNameAccessGroup());
        allowingManLabel.setText(orderDoc.getAllowingManNameAccessGroup());
        foremanLabel.setText(orderDoc.getForemanNameAccessGroup());
        issueWorkPermitManLabel.setText(orderDoc.getIssueWorkPermitManNameAccessGroup());
        setColorTextFieldDataPicker(dateEndDP,dateStartDP);

        populateTables();
        rootNode = new TreeItem<>("Электроустановки");
        populateSubstationElInstallationElEquipmentTreeView();
        elInstallationElEquipmentTreeView.setRoot(rootNode);


    }
    private void setColorTextFieldDataPicker(DatePicker firstDP, DatePicker secondDP){
        if ((firstDP.getValue() != null ) & (secondDP.getValue() != null)) {
            if (firstDP.getValue().isBefore(secondDP.getValue())) {
                firstDP.getEditor().setBackground(new Background(new BackgroundFill(Color.GOLD, CornerRadii.EMPTY, Insets.EMPTY)));
            } else {
                firstDP.getEditor().setBackground(secondDP.getEditor().getBackground());
            }
        }
    }

    @SuppressWarnings("unchecked")
    private ObservableList<OrderDocElectricalEquipment> getOrderDocElectricalEquipment(){
        ObservableList<OrderDocElectricalEquipment> result = FXCollections.observableArrayList();
        try {
            GenericDao<OrderDocElectricalEquipment, ?> daoODEE = factory.getDao(connection, Class.forName(OrderDocElectricalEquipment.class.getName()));
            result = daoODEE.getOLByPk(orderDoc.getId());
        }catch (ClassNotFoundException | PersistException e){
            e.getStackTrace();
            LOG.warning(e.getMessage());
        }
        return result;
    }

    @SuppressWarnings("unchecked")
    private void populateSubstationElInstallationElEquipmentTreeView(){
        ObservableList<SubstationElInstallationElEquipment> result = FXCollections.observableArrayList();

        try {
            GenericDao<SubstationElInstallationElEquipment, ?> daoSEE = factory.getDao(connection, Class.forName(SubstationElInstallationElEquipment.class.getName()));
            result = daoSEE.getAll();
        }catch (ClassNotFoundException | PersistException e){
            e.getStackTrace();
            LOG.warning(e.getMessage());
        }

        if (result != null){
            for (SubstationElInstallationElEquipment ooee : result){
                TreeItem<String> eeLeaf = new TreeItem<>(ooee.getElectricalEquipment());
                boolean found = false;
                for (TreeItem<String> eiNode: rootNode.getChildren()){
                    if (eiNode.getValue().contentEquals(ooee.getElectricalInstallation())){
                        eiNode.getChildren().add(eeLeaf);
                        found = true;
                        break;
                    }
                }
                if(!found){
                    TreeItem<String> eiNode = new TreeItem<>(ooee.getElectricalInstallation());
                    rootNode.getChildren().add(eiNode);
                    eiNode.getChildren().add(eeLeaf);
                }
            }
        }
    }
    @SuppressWarnings("unchecked")
    private ObservableList<OrderDocPerson> getCrewFromDao(){ // FIXME: 17.05.2018 сделать через интерфейсы
        ObservableList<OrderDocPerson> result = FXCollections.observableArrayList();
        try {
            GenericDao<OrderDocPerson, Integer>  daoODPTemp = factory.getDao(connection, Class.forName(OrderDocPerson.class.getName()));
            result = daoODPTemp.getOLByPk(orderDoc.getId());
        }catch (ClassNotFoundException |PersistException e){
            e.printStackTrace();
            LOG.warning(e.getMessage());
        }
        return result;
    }

    private ObservableList<OrderDocPerson> getCrew(){
        ObservableList<OrderDocPerson> crew = getCrewFromDao();

        ObservableList<OrderDocPerson> result = FXCollections.observableArrayList();
        if (crew.size()>0) {
            for(OrderDocPerson odp :crew){
                switch (odp.getStatusId()){
                    case 1://выдающий наряд
                        orderDoc.setIssueWorkPermitManNameAccessGroup(odp.getPerson());
                        orderDoc.setIssueWorkPermitManId(odp.getStatusId());
                        allPersonsObservableList.add(odp);
                        break;
                    case 2: //ответсвенный руководитель работ
                        orderDoc.setPerformingAuthorityNameAccessGroup(odp.getPerson());
                        orderDoc.setPerformingAuthorityId(odp.getPersonId());
                        allPersonsObservableList.add(odp);
                        break;
                    case 3://допускающий
                        orderDoc.setAllowingManNameAccessGroup(odp.getPerson());
                        orderDoc.setAllowingManId(odp.getPersonId());
                        allPersonsObservableList.add(odp);
                        break;
                    case 4://наблюдающий
                        orderDoc.setViewerNameAccessGroup(odp.getPerson());
                        orderDoc.setViewerId(odp.getPersonId());
                        allPersonsObservableList.add(odp);
                        break;
                    case 5://производитель работ
                        orderDoc.setForemanNameAccessGroup(odp.getPerson());
                        orderDoc.setForemanId(odp.getPersonId());
                        allPersonsObservableList.add(odp);
                        break;
                    case 6://член бригады
                        result.add(odp);
                        break;
                }
            }
            orderDoc.setCrewMember(result); // FIXME: 24.02.2018 переделать, может есть иной способ: разделить Lists
        }
        return result;

    }

    private void populateTables(){
        orderDocPersonColumnPerson.setCellValueFactory(param -> param.getValue().personProperty());
        orderDocPersonColumnAdditional.setCellValueFactory(param -> param.getValue().additionalProperty());
        LOG.info("Initialization Crew");

        orderDocPersonTableView.setItems(crewObservableList);
        LOG.info("Filling table Crew");

        orderDocElectricalEquipmentColumnElectricalInstallation.setCellValueFactory(param -> param.getValue().electricalEquipmentProperty());
        orderDocElectricalEquipmentColumnAdditional.setCellValueFactory(param -> param.getValue().additionalProperty());
        LOG.info("Initialization orderDocElectricalEquipmentColumnElectricalInstallation");


        orderDocElectricalEquipmentTableView.setItems(orderDocElectricalEquipmentObservableList);
        LOG.info("Filling table orderDocElectricalEquipmentColumnElectricalInstallation");

    }

    @FXML private void addElementTreeViewEEtoTableViewEE(){
        TreeItem item = elInstallationElEquipmentTreeView.getSelectionModel().getSelectedItem();
        if ((item != null)&& (item.getParent() != null) ) {
            String selectLeaf = (String) item.getValue();
            String selectParent = "";
            if ((item.getParent().getParent() != null)){
                selectParent = item.getParent().getValue() + ": ";
            }
            String parentLeaf = selectParent + selectLeaf;
            OrderDocElectricalEquipment temp = new OrderDocElectricalEquipment();
            temp.setElectricalEquipment(parentLeaf);
            orderDocElectricalEquipmentObservableList.add(temp);
        }
    }



    @FXML private void deleteElementFromEiEeList(){
       deleteElementFromList(orderDocElectricalEquipmentTableView,orderDocElectricalEquipmentObservableList);

    }
    @FXML private void deleteElementFromCrewList(){
        deleteElementFromList(orderDocPersonTableView,crewObservableList);
    }

    @FXML private void addElementToCrewList() {
        Person person = (Person) showLayoutSelectMenu( Main.mPersonMenuLayout);
        OrderDocPerson tmp = new OrderDocPerson();
        if (person != null){
            tmp.setPersonId(person.getId());
            tmp.setPerson(person.getName());
            tmp.setOrderDocId(orderDoc.getId());
            tmp.setStatusId(6);  // FIXME: 22.02.2018  исправить значение 6, на нормальный запрос. 6 обозначает, что это член бригады. Заменить на ENUM
            crewObservableList.add(tmp);
        }
    }

    @FXML private void threeDotsPerformingAuthority(){ //ответсвенный руководитель работ - 2
        Person person = (Person) showLayoutSelectMenu(Main.mPersonMenuLayout);
        if (person != null){
            performingAuthorityLabel.setText(person.getName());
            orderDoc.setPerformingAuthorityId(person.getId());

            OrderDocPerson orderDocPerson = new OrderDocPerson();
            orderDocPerson.setStatusId(2);
            orderDocPerson.setPersonId(person.getId());
            orderDocPerson.setOrderDocId(orderDoc.getId());

            findCrewMemberAtListRemoveThenAdd(orderDocPerson);
        }
    }
    @FXML private void threeDotsIssueWorkPermitMan(){ //выдающий наряд - 1
        Person person = (Person) showLayoutSelectMenu(Main.mPersonMenuLayout);
        if (person != null){
            issueWorkPermitManLabel.setText(person.getName());
            orderDoc.setIssueWorkPermitManId(person.getId());

            OrderDocPerson orderDocPerson = new OrderDocPerson();
            orderDocPerson.setStatusId(1);
            orderDocPerson.setPersonId(person.getId());
            orderDocPerson.setOrderDocId(orderDoc.getId());

            findCrewMemberAtListRemoveThenAdd(orderDocPerson);
        }
    }
    @FXML private void threeDotsAllowingMan(){ //допускающий - 3
        Person person = (Person) showLayoutSelectMenu( Main.mPersonMenuLayout);
        if (person != null){
            allowingManLabel.setText(person.getName());
            orderDoc.setAllowingManId(person.getId());

            OrderDocPerson orderDocPerson = new OrderDocPerson();
            orderDocPerson.setStatusId(3);
            orderDocPerson.setPersonId(person.getId());
            orderDocPerson.setOrderDocId(orderDoc.getId());

            findCrewMemberAtListRemoveThenAdd(orderDocPerson);
        }
    }
    @FXML private void threeDotsForeman(){ //производитель работ - 5
        Person person = (Person) showLayoutSelectMenu(Main.mPersonMenuLayout);
        if (person != null){
            foremanLabel.setText(person.getName());
            orderDoc.setForemanId(person.getId());

            OrderDocPerson orderDocPerson = new OrderDocPerson();
            orderDocPerson.setStatusId(5);
            orderDocPerson.setPersonId(person.getId());
            orderDocPerson.setOrderDocId(orderDoc.getId());

            findCrewMemberAtListRemoveThenAdd(orderDocPerson);
        }
    }
    @FXML private void threeDotsViewer(){ //Наблюдающий - 4
        Person person = (Person) showLayoutSelectMenu( Main.mPersonMenuLayout);
        if (person != null){
            viewerLabel.setText(person.getName());
            orderDoc.setViewerId(person.getId());

            OrderDocPerson orderDocPerson = new OrderDocPerson();
            orderDocPerson.setStatusId(4);
            orderDocPerson.setPersonId(person.getId());
            orderDocPerson.setOrderDocId(orderDoc.getId());

            findCrewMemberAtListRemoveThenAdd(orderDocPerson);
        }
    }

    @Override
    @FXML
    public void threeDotsOrganization() {
        Organization sm = (Organization) showLayoutSelectMenu(Main.mOrganizationsMenuLayout);

        if (sm != null){
            organizationLabel.setText(sm.getName());
            orderDoc.setOrgId(sm.getId());
        }
    }

    @Override
    public void threeDotsSubdivision() {
        Subdivision subdivision = (Subdivision) showLayoutSelectMenu( Main.mSubdivisionsMenuLayout);
        if (subdivision != null){
            subdivisionLabel.setText(subdivision.getName());
            orderDoc.setSubdivId(subdivision.getId());
        }
    }

    @Override
    public void threeDotsSubstation() {
        Substation substation = (Substation) showLayoutSelectMenu(Main.mSubstationsMenuLayout);
        if (substation != null){
            substationLabel.setText(substation.getName());
            orderDoc.setSubstatId(substation.getId());
        }
    }


    @FXML void getStringToInstructTo(){

        showLayoutElectricalInstallationTree();
    }
    private void showLayoutElectricalInstallationTree() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(SubstationElInstallationElEquipmentController.class.getClassLoader().getResource(Main.mSubstationElInstallationElEquipmentTreeLayout));
            AnchorPane page = loader.load();

            Stage dialogSEIEEStage = new Stage();
            dialogSEIEEStage.setTitle("Выбор Электроустановки");
            dialogSEIEEStage.initModality(Modality.APPLICATION_MODAL); // WINDOW_MODAL -> APPLICATION_MODAL иначе можно открывать неограниченное количество окон
            dialogSEIEEStage.initOwner(StartChoiceBaseController.getSecondaryStage()); // баз данной строки окно исчезает
            dialogSEIEEStage.getIcons().add(new Image(Main.ico));
            Scene scene = new Scene(page);
            dialogSEIEEStage.setScene(scene);
            SubstationElInstallationElEquipmentController controller = loader.getController();

            controller.populateTree();
            controller.setDialogSEIEEStage(dialogSEIEEStage);
            dialogSEIEEStage.showAndWait();

            String tmp = controller.getResultToInstruct();
            populateInstructToTextField(tmp);


        } catch (IOException e) {
            ServiceMessagesController.addErrorServiceMessage("Открытие окна невозможно! " + e.getMessage());
            e.printStackTrace();
        }
    }

    private void populateInstructToTextField(String instructTo){

        if (instructTo == null){
            instructTo = "";
        }
        if ((instructToTextField.getText().length() == 0) && instructToTextField.getText().isEmpty()   ){
            instructToTextField.setText(instructTo);
        }else if(!instructTo.isEmpty()){
            instructToTextField.setText( instructToTextField.getText()+", " + instructTo);
        }
    }

    private  void findCrewMemberAtListRemoveThenAdd(OrderDocPerson odp) {
        ObservableList<OrderDocPerson> localCrew = FXCollections.observableArrayList();
        localCrew.addAll(allPersonsObservableList);

        for (OrderDocPerson person : localCrew) {
            if (person.getStatusId() != 6) {
                if (person.getStatusId() == odp.getStatusId()) {
                    allPersonsObservableList.remove(person);
                }
            }
        }
        allPersonsObservableList.add(odp);
        System.out.println(allPersonsObservableList.size());
    }
}
