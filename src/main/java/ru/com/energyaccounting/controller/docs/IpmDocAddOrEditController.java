package ru.com.energyaccounting.controller.docs;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.ComboBoxTableCell;
import javafx.scene.control.cell.TextFieldTableCell;
import ru.com.energyaccounting.Main;
import ru.com.energyaccounting.controller.AbstractAddOrEditController;
import ru.com.energyaccounting.dao.CRUDDao;
import ru.com.energyaccounting.dao.GenericDao;
import ru.com.energyaccounting.dao.PersistException;
import ru.com.energyaccounting.model.*;
import tornadofx.control.DatePickerTableCell;
import tornadofx.control.DateTimePicker;

import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ResourceBundle;
import java.util.logging.Logger;

public class IpmDocAddOrEditController extends AbstractAddOrEditController<IpmDoc>  implements Initializable {
    private static final Logger LOG = Logger.getLogger(IpmDocAddOrEditController.class.getName());

    @FXML private DateTimePicker docDataTimePicker;
    @FXML private Label idLabel;
    @FXML private TableView<IpmDocIpm> docTableView;
    @FXML private TableColumn<IpmDocIpm, String> inventNumbColumn;
    @FXML private TableColumn<IpmDocIpm, String> infoColumn;
    @FXML private TableColumn<IpmDocIpm, LocalDate> lastTestColumn;
    @FXML private TableColumn<IpmDocIpm, LocalDate> nextTestColumn;
    @FXML private TableColumn<IpmDocIpm, String> statusColumn;
    @FXML private TableColumn<IpmDocIpm, String> commentColumn;
    @FXML private TextField descriptionTextField;

    private IpmDoc ipmDoc;

    public static final ObservableList<String> statusTempList = FXCollections.observableArrayList("Годен", "Изьят","Брак");
    private ObservableList<IpmDocIpm> ipmDocIpmList = FXCollections.observableArrayList();
    private ObservableList<IpmDocIpm> ipmDocIpmRemovedList = FXCollections.observableArrayList();

    public IpmDocAddOrEditController() {
        super(IpmDoc.class.getName());
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        inventNumbColumn.setCellValueFactory(param -> param.getValue().inventoryNumberProperty());
        infoColumn.setCellValueFactory(param -> param.getValue().nameProperty());

        lastTestColumn.setCellValueFactory(param -> param.getValue().lastTestDateProperty());
        lastTestColumn.setCellFactory(DatePickerTableCell.forTableColumn());
        lastTestColumn.setOnEditCommit((TableColumn.CellEditEvent<IpmDocIpm, LocalDate> t)->{
            t.getTableView().getItems().get(t.getTablePosition().getRow()).setLastTestDate(t.getNewValue());
        });

        nextTestColumn.setCellValueFactory(param -> param.getValue().nextTestDateProperty());
        nextTestColumn.setCellFactory(DatePickerTableCell.forTableColumn());
        nextTestColumn.setOnEditCommit((TableColumn.CellEditEvent<IpmDocIpm, LocalDate> t)->{
            t.getTableView().getItems().get(t.getTablePosition().getRow()).setNextTestDate(t.getNewValue());
        });

        statusColumn.setCellValueFactory(param -> param.getValue().statusProperty());
        statusColumn.setCellFactory(ComboBoxTableCell.forTableColumn(statusTempList));
        statusColumn.setOnEditCommit((TableColumn.CellEditEvent<IpmDocIpm,String> t) ->{
            t.getTableView().getItems().get(t.getTablePosition().getRow()).setStatus(t.getNewValue());
        });

        commentColumn.setCellValueFactory(param -> param.getValue().descriptionProperty());
        commentColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        commentColumn.setOnEditCommit((TableColumn.CellEditEvent<IpmDocIpm, String> t) ->
                t.getTableView().getItems().get(t.getTablePosition().getRow()).setDescription(t.getNewValue())
        );
    }

    @Override
    public void setObject(IpmDoc ipmDoc) {
        this.ipmDoc = ipmDoc;
        this.dialogStage.setTitle("Редактирование: " + this.ipmDoc.getDialogStageTitle());
        String docNumber = Integer.toString(ipmDoc.getId());
        idLabel.setText(docNumber.equals("0")? "": docNumber);
        docDataTimePicker.setDateTimeValue(ipmDoc.getDocDateTime()!=null ? ipmDoc.getDocDateTime(): LocalDateTime.now());
        descriptionTextField.setText(ipmDoc.getDescription());
        ipmDocIpmList = getIpmDocIpmListFromDao();

        populateTables();
    }

    @Override
    public boolean isInputValid() {
        return false;
    }

    @Override

    public void handleOK() {
        LOG.info("нажали Ок");
        String description = descriptionTextField.getText();
        LocalDateTime docDateTime = docDataTimePicker.getDateTimeValue()!=null? docDataTimePicker.getDateTimeValue(): LocalDateTime.now();
        ipmDoc.setDescription(description);
        ipmDoc.setDocDateTime(docDateTime);
        try {
            objectPersistUpdate(ipmDoc);
            IpmDoc ipmDocFromDao = getObjectFromDao();
            ipmDocIpmList.forEach((ipmDocIpm)->ipmDocIpm.setDoc_id(ipmDocFromDao.getId()));
            ipmDocIpmList.forEach(ipmDocIpm -> {
                CRUDDao<IpmDocIpm> crudDao = new CRUDDao<>(ipmDocIpm);
                try {
                    crudDao.objectPersistUpdate();
                } catch (PersistException e) {
                    e.printStackTrace();
                }
            });

            ipmDocIpmRemovedList.forEach(ipmDocIpm -> {
                CRUDDao<IpmDocIpm> crudDao = new CRUDDao<>(ipmDocIpm);
                try {
                    crudDao.remove();
                } catch (PersistException e) {
                    e.printStackTrace();
                }
            });

        } catch (PersistException e) {
            e.printStackTrace();
        }
    }

    //Identifier
    @SuppressWarnings("unchecked")
    private ObservableList<IpmDocIpm> getIpmDocIpmListFromDao(){
        ObservableList<IpmDocIpm> result = FXCollections.observableArrayList();
        try {
            GenericDao<IpmDocIpm, Integer> daoIpm = factory.getDao(connection, IpmDocIpm.class);
            result = daoIpm.getOLByPk(ipmDoc.getId());
        }catch (PersistException e){
            e.printStackTrace();
            LOG.warning(e.getMessage());
        }
        return result;
    }

    private void populateTables(){
        docTableView.setItems(ipmDocIpmList);
    }

    @FXML private void addIpmToIpmList(){
        IPM ipm =(IPM) showLayoutSelectMenu(Main.mIpmsMenuLayout);
        if (ipm != null) {
            IpmDocIpm ipmDocIpm = ipmToIpmDocIpmAdapter(ipm);
            int findDuplicate = ipmDocIpm.getIpm_id();
            if (ipmDocIpmList.stream().noneMatch(ip ->(ip.getIpm_id() == findDuplicate))) {
                ipmDocIpmList.add(ipmDocIpm);
                ipmDocIpmRemovedList.remove(ipmDocIpm);
            }
        }
    }

    private IpmDocIpm ipmToIpmDocIpmAdapter(IPM ipm){
        IpmDocIpm  ipmDocIpm = new IpmDocIpm();
        ipmDocIpm.setIpm_id(ipm.getId());
        ipmDocIpm.setName(ipm.getElectricalInstallation() + ", " + ipm.getImpType() + ", " + ipm.getVoltageType());
        ipmDocIpm.setDescription(ipm.getDescription());
        ipmDocIpm.setInventoryNumber(ipm.getInventoryNumber());

        ipmDocIpm.setLastTestDate(ipm.getNextTestDate());
        ipmDocIpm.setImp_type_id(ipm.getImp_type_id());
        ipmDocIpm.setImpType(ipm.getImpType());
        ipmDocIpm.setEl_install_id(ipm.getEl_install_id());
        ipmDocIpm.setElectricalInstallation(ipm.getElectricalInstallation());
        ipmDocIpm.setVt_id(ipm.getVt_id());
        ipmDocIpm.setVoltageType(ipm.getVoltageType());
        ipmDocIpm.setStatus(ipm.getStatus());

        return ipmDocIpm;
    }

    @FXML private void deleteElementFromIpmList(){
        Object objectWasDeleted = deleteElementFromList(docTableView, ipmDocIpmList);
        if (objectWasDeleted instanceof IpmDocIpm) {
            IpmDocIpm result = (IpmDocIpm) objectWasDeleted;
            if (result.getId()!=0) {
                ipmDocIpmRemovedList.add(result);
            }
        }
    }


}
