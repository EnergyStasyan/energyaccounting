package ru.com.energyaccounting.controller.docs;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.ComboBoxTableCell;
import javafx.scene.control.cell.TextFieldTableCell;
import ru.com.energyaccounting.Main;
import ru.com.energyaccounting.controller.AbstractAddOrEditController;
import ru.com.energyaccounting.dao.CRUDDao;
import ru.com.energyaccounting.dao.GenericDao;
import ru.com.energyaccounting.dao.PersistException;
import ru.com.energyaccounting.model.*;
import tornadofx.control.DatePickerTableCell;
import tornadofx.control.DateTimePicker;

import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ResourceBundle;
import java.util.logging.Logger;

public class ElectricalEquipmentDocAddOrEditController extends AbstractAddOrEditController<ElectricalEquipmentDoc>  implements Initializable {
    private static final Logger LOG = Logger.getLogger(ElectricalEquipmentDocAddOrEditController.class.getName());

    @FXML private DateTimePicker docDataTimePicker;
    @FXML private Label idLabel;
    @FXML private TableView<ElectricalEquipmentDocElectricalEquipment> docTableView;
    @FXML private TableColumn<ElectricalEquipmentDocElectricalEquipment, String> inventNumbColumn;
    @FXML private TableColumn<ElectricalEquipmentDocElectricalEquipment, String> infoColumn;
    @FXML private TableColumn<ElectricalEquipmentDocElectricalEquipment, LocalDate> lastTestColumn;
    @FXML private TableColumn<ElectricalEquipmentDocElectricalEquipment, LocalDate> nextTestColumn;
    @FXML private TableColumn<ElectricalEquipmentDocElectricalEquipment, String> statusColumn;
    @FXML private TableColumn<ElectricalEquipmentDocElectricalEquipment, String> commentColumn;
    @FXML private TextField descriptionTextField;

    private ElectricalEquipmentDoc electricalEquipmentDoc;

    public static final ObservableList<String> statusTempList = FXCollections.observableArrayList("Годен", "Изъят","Брак");
    private ObservableList<ElectricalEquipmentDocElectricalEquipment> ElectricalEquipmentDocElectricalEquipmentList = FXCollections.observableArrayList();
    private ObservableList<ElectricalEquipmentDocElectricalEquipment> ElectricalEquipmentDocElectricalEquipmentRemovedList = FXCollections.observableArrayList();

    public ElectricalEquipmentDocAddOrEditController() {
        super(ElectricalEquipmentDoc.class.getName());
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        inventNumbColumn.setCellValueFactory(param -> param.getValue().inventoryNumberProperty());
        infoColumn.setCellValueFactory(param -> param.getValue().nameProperty());

        lastTestColumn.setCellValueFactory(param -> param.getValue().lastTestDateProperty());
        lastTestColumn.setCellFactory(DatePickerTableCell.forTableColumn());
        lastTestColumn.setOnEditCommit((TableColumn.CellEditEvent<ElectricalEquipmentDocElectricalEquipment, LocalDate> t)->{
            t.getTableView().getItems().get(t.getTablePosition().getRow()).setLastTestDate(t.getNewValue());
        });

        nextTestColumn.setCellValueFactory(param -> param.getValue().nextTestDateProperty());
        nextTestColumn.setCellFactory(DatePickerTableCell.forTableColumn());
        nextTestColumn.setOnEditCommit((TableColumn.CellEditEvent<ElectricalEquipmentDocElectricalEquipment, LocalDate> t)->{
            t.getTableView().getItems().get(t.getTablePosition().getRow()).setNextTestDate(t.getNewValue());
        });

        statusColumn.setCellValueFactory(param -> param.getValue().statusProperty());
        statusColumn.setCellFactory(ComboBoxTableCell.forTableColumn(statusTempList));
        statusColumn.setOnEditCommit((TableColumn.CellEditEvent<ElectricalEquipmentDocElectricalEquipment,String> t) ->{
            t.getTableView().getItems().get(t.getTablePosition().getRow()).setStatus(t.getNewValue());
        });

        commentColumn.setCellValueFactory(param -> param.getValue().descriptionProperty());
        commentColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        commentColumn.setOnEditCommit((TableColumn.CellEditEvent<ElectricalEquipmentDocElectricalEquipment, String> t) ->
                t.getTableView().getItems().get(t.getTablePosition().getRow()).setDescription(t.getNewValue())
        );
    }

    @Override
    public void setObject(ElectricalEquipmentDoc electricalEquipmentDoc) {
        this.electricalEquipmentDoc = electricalEquipmentDoc;
        this.dialogStage.setTitle("Редактирование: " + this.electricalEquipmentDoc.getDialogStageTitle());
        String docNumber = Integer.toString(electricalEquipmentDoc.getId());
        idLabel.setText(docNumber.equals("0")? "": docNumber);
        docDataTimePicker.setDateTimeValue(electricalEquipmentDoc.getDocDateTime()!=null? electricalEquipmentDoc.getDocDateTime(): LocalDateTime.now());
        descriptionTextField.setText(electricalEquipmentDoc.getDescription());
        ElectricalEquipmentDocElectricalEquipmentList = getElectricalEquipmentDocElectricalEquipmentListFromDao();

        populateTables();
    }

    @Override
    public boolean isInputValid() {
        return false;
    }

    @Override
    public void handleOK() {
        LOG.info("нажали Ок");
        String description = descriptionTextField.getText();
        LocalDateTime docDateTime = docDataTimePicker.getDateTimeValue()!=null? docDataTimePicker.getDateTimeValue(): LocalDateTime.now();
        electricalEquipmentDoc.setDescription(description);
        electricalEquipmentDoc.setDocDateTime(docDateTime);
        try {
            objectPersistUpdate(electricalEquipmentDoc);
            ElectricalEquipmentDoc electricalEquipmentDocFromDao = getObjectFromDao();
            ElectricalEquipmentDocElectricalEquipmentList.forEach(electricalEquipmentDoc->electricalEquipmentDoc.setDoc_id(electricalEquipmentDocFromDao.getId()));
            ElectricalEquipmentDocElectricalEquipmentList.forEach(electricalEquipmentDoc -> {
                CRUDDao<ElectricalEquipmentDocElectricalEquipment> crudDao = new CRUDDao<>(electricalEquipmentDoc);
                try {
                    crudDao.objectPersistUpdate();
                } catch (PersistException e) {
                    e.printStackTrace();
                }
            });

            ElectricalEquipmentDocElectricalEquipmentRemovedList.forEach(electricalEquipmentDoc -> {
                CRUDDao<ElectricalEquipmentDocElectricalEquipment> crudDao = new CRUDDao<>(electricalEquipmentDoc);
                try {
                    crudDao.remove();
                } catch (PersistException e) {
                    e.printStackTrace();
                }
            });

        } catch (PersistException e) {
            e.printStackTrace();
        }
    }

    //Identifier
    @SuppressWarnings("unchecked")
    private ObservableList<ElectricalEquipmentDocElectricalEquipment> getElectricalEquipmentDocElectricalEquipmentListFromDao(){
        ObservableList<ElectricalEquipmentDocElectricalEquipment> result = FXCollections.observableArrayList();
        try {
            GenericDao<ElectricalEquipmentDocElectricalEquipment, Integer> daoElectricalEquipment = factory.getDao(connection, ElectricalEquipmentDocElectricalEquipment.class);
            result = daoElectricalEquipment.getOLByPk(electricalEquipmentDoc.getId());
        }catch (PersistException e){
            e.printStackTrace();
            LOG.warning(e.getMessage());
        }
        return result;
    }

    private void populateTables(){
        docTableView.setItems(ElectricalEquipmentDocElectricalEquipmentList);
    }

    @FXML private void addElectricalEquipmentToElectricalEquipmentList(){
        ElectricalEquipment electricalEquipment =(ElectricalEquipment) showLayoutSelectMenu(Main.mElectricalEquipmentsMenuLayout);
        if (electricalEquipment != null) {
            ElectricalEquipmentDocElectricalEquipment electricalEquipmentDocElectricalEquipment = electricalEquipmentToElectricalEquipmentAdapter(electricalEquipment);
            int findDuplicate = electricalEquipmentDocElectricalEquipment.getElectricalEquipment_id();
            if (ElectricalEquipmentDocElectricalEquipmentList.stream().noneMatch(ip ->(ip.getElectricalEquipment_id() == findDuplicate))) {
                ElectricalEquipmentDocElectricalEquipmentList.add(electricalEquipmentDocElectricalEquipment);
                ElectricalEquipmentDocElectricalEquipmentRemovedList.remove(electricalEquipmentDocElectricalEquipment);
            }
        }
    }

    private ElectricalEquipmentDocElectricalEquipment electricalEquipmentToElectricalEquipmentAdapter(ElectricalEquipment ee){
        ElectricalEquipmentDocElectricalEquipment  eedee = new ElectricalEquipmentDocElectricalEquipment();
        eedee.setElectricalEquipment_id(ee.getId());
        eedee.setName(ee.getElectricalInstallation() + ", " + ee.getElectricalEquipmentType() + ", " + ee.getVoltageType());
        eedee.setDescription(ee.getDescription());
        eedee.setInventoryNumber(ee.getInventoryNumber());

        eedee.setLastTestDate(ee.getNextTestDate());
        eedee.setElectrical_equipment_type_id(ee.getElectrical_equipment_type_id());
        eedee.setElectricalEquipmentType(ee.getElectricalEquipmentType());
        eedee.setEl_install_id(ee.getEl_install_id());
        eedee.setElectricalInstallation(ee.getElectricalInstallation());
        eedee.setVt_id(ee.getVt_id());
        eedee.setVoltageType(ee.getVoltageType());
        eedee.setStatus(ee.getStatus());

        return eedee;
    }

    @FXML private void deleteElementFromElectricalEquipmentList(){
        Object objectWasDeleted = deleteElementFromList(docTableView, ElectricalEquipmentDocElectricalEquipmentList);
        if (objectWasDeleted instanceof ElectricalEquipmentDocElectricalEquipment) {
            ElectricalEquipmentDocElectricalEquipment result = (ElectricalEquipmentDocElectricalEquipment) objectWasDeleted;
            if (result.getId()!=0) {
                ElectricalEquipmentDocElectricalEquipmentRemovedList.add(result);
            }
        }
    }


}
