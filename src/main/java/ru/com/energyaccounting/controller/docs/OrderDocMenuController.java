package ru.com.energyaccounting.controller.docs;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import ru.com.energyaccounting.Main;
import ru.com.energyaccounting.controller.AbstractMenuController;
import ru.com.energyaccounting.model.OrderDoc;

import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

/**
 * Created by UserSts on 20.05.2017.
 */
public class OrderDocMenuController extends AbstractMenuController<OrderDoc, OrderDocAddOrEditController> implements Initializable {

    @FXML private TableColumn<OrderDoc, Integer> orderDocsColumnId;
    @FXML private TableColumn<OrderDoc, String> orderDocsColumnNumber;
    @FXML private TableColumn<OrderDoc, LocalDate> orderDocsColumnDateOfIssue;
    @FXML private TableColumn<OrderDoc, String> orderDocsColumnPoruchaetsya;
    @FXML private TableColumn<OrderDoc, String> orderDocsColumnDescription;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);
        orderDocsColumnId.          setCellValueFactory(cellData -> cellData.getValue().idProperty().asObject());
        orderDocsColumnNumber.        setCellValueFactory(cellData -> cellData.getValue().numberProperty());
        orderDocsColumnDateOfIssue.setCellValueFactory(cellData -> cellData.getValue().dateOfIssueProperty());
        orderDocsColumnPoruchaetsya. setCellValueFactory(cellData -> cellData.getValue().instructToProperty());
        orderDocsColumnDescription. setCellValueFactory(cellData -> cellData.getValue().descriptionProperty());

        searchObjects();
    }

    @Override
    protected OrderDoc createObj() {
        return new OrderDoc();
    }

    @Override
    protected String editOrAddLayout() {
        return Main.mOrderDocAddOrEditLayout;
    }

    @Override
    protected String getPathForJasperPattern() {
        return null;
    }
}
