package ru.com.energyaccounting.controller.docs;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import ru.com.energyaccounting.Main;
import ru.com.energyaccounting.controller.AbstractMenuController;
import ru.com.energyaccounting.model.AccessGroup;

import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;


/**
 * Created by UserSts on 06.07.2017.
 */
public class AccessGroupMenuController extends AbstractMenuController<AccessGroup, AccessGroupAddOrEditController> implements Initializable {

    @FXML private TableColumn<AccessGroup, Integer> accessGroupColumnId;
    @FXML private TableColumn<AccessGroup, String> accessGroupColumnNumberOfOrder;
    @FXML private TableColumn<AccessGroup, String> accessGroupColumnNumberOfGroup;
    @FXML private TableColumn<AccessGroup, LocalDate> accessGroupColumnDateOfOrder;
    @FXML private TableColumn<AccessGroup, LocalDate> accessGroupColumnLastTestDate;
    @FXML private TableColumn<AccessGroup, LocalDate> accessGroupColumnNextTestDate;
    @FXML private TableColumn<AccessGroup, String> accessGroupColumnPerson;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);
        accessGroupColumnId.setCellValueFactory(cellData -> cellData.getValue().idProperty().asObject());
        accessGroupColumnNumberOfOrder.setCellValueFactory(cellData -> cellData.getValue().numberOfOrderProperty());
        accessGroupColumnNumberOfGroup.setCellValueFactory(cellData -> cellData.getValue().numberOfAccessGroupProperty());
        accessGroupColumnDateOfOrder.setCellValueFactory(cellData -> cellData.getValue().orderDateProperty());
        accessGroupColumnLastTestDate.setCellValueFactory(cellData -> cellData.getValue().lastTestDateProperty());
        accessGroupColumnNextTestDate.setCellValueFactory(cellData -> cellData.getValue().nextTestDateProperty());
        accessGroupColumnPerson.setCellValueFactory(cellData -> cellData.getValue().personProperty());
        searchObjects();
    }

    @Override
    protected AccessGroup createObj() {
        return new AccessGroup();
    }

    @Override
    protected String editOrAddLayout() {
        return Main.mAccessGroupAddOrEditLayout;
    }

    @Override
    protected String getPathForJasperPattern() {
        return null;
    }
}
