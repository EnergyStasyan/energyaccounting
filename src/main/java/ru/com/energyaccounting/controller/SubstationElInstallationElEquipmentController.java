package ru.com.energyaccounting.controller;


import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.stage.Stage;
import ru.com.energyaccounting.dao.DaoFactory;
import ru.com.energyaccounting.dao.DaoFactoryInterface;
import ru.com.energyaccounting.dao.GenericDao;
import ru.com.energyaccounting.dao.PersistException;
import ru.com.energyaccounting.model.SubstationElInstallationElEquipment;

import java.sql.Connection;
import java.util.logging.Logger;

public class SubstationElInstallationElEquipmentController {
    private static final Logger LOG = Logger.getLogger(SubstationElInstallationElEquipment.class.getName());
    TreeItem<String> rootNode;
    private Stage dialogSEIEEStage;
    @FXML
    private TreeView<String> subEEEITree;
    private String resultToInstruct;

    public SubstationElInstallationElEquipmentController() {
        rootNode = new TreeItem<>("Электроустановки");

    }

    public String getResultToInstruct() {
        return resultToInstruct;
    }

    public void setResultToInstruct(String resultToInstruct) {
        this.resultToInstruct = resultToInstruct;
    }

    @FXML
    private void handleCancel() {
        LOG.info("Нажали Cancel");
        dialogSEIEEStage.close();
    }

    @FXML
    private void handleOK() {
        LOG.info("Нажали ОК");
        TreeItem<String> item = subEEEITree.getSelectionModel().getSelectedItem();
        if (item != null && item.getParent() != null) {
            String selectedLeaf = item.getValue();
            String selectedParent = "";
            if (item.getParent().getParent() != null) {
                selectedParent = item.getParent().getValue() + ": ";
            }
            String parentLeaf = selectedParent + selectedLeaf;
            setResultToInstruct(parentLeaf);
        }
        dialogSEIEEStage.close();
    }

    public Stage getDialogSEIEEStage() {
        return dialogSEIEEStage;
    }

    public void setDialogSEIEEStage(Stage dialogSEIEEStage) {
        this.dialogSEIEEStage = dialogSEIEEStage;
    }


    private ObservableList<SubstationElInstallationElEquipment> getSubstationElInstallationElEquipmentFromDao() {
        ObservableList<SubstationElInstallationElEquipment> result = FXCollections.observableArrayList();
        try {
            DaoFactoryInterface<Connection> factory = new DaoFactory();
            Connection connection = factory.getContext();
            GenericDao<SubstationElInstallationElEquipment, Integer> dao = factory.getDao(connection, Class.forName(SubstationElInstallationElEquipment.class.getName()));
            result = dao.getAll();

        } catch (ClassNotFoundException | PersistException e) {
            e.printStackTrace();
        }
        return result;
    }

    public void populateTree() {
        ObservableList<SubstationElInstallationElEquipment> result = getSubstationElInstallationElEquipmentFromDao();
        if (result != null) {
            for (SubstationElInstallationElEquipment seiee : result) {
                TreeItem<String> eeLeaf = new TreeItem<>(seiee.getElectricalEquipment());
                boolean found = false;
                for (TreeItem<String> eiNode : rootNode.getChildren()) {
                    if (eiNode.getValue().contentEquals(seiee.getElectricalInstallation())) {
                        eiNode.getChildren().add(eeLeaf);
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    TreeItem<String> eiNode = new TreeItem<>(seiee.getElectricalInstallation());
                    rootNode.getChildren().add(eiNode);
                    eiNode.getChildren().add(eeLeaf);
                }
            }
        }
        subEEEITree.setRoot(rootNode);
    }
}
