package ru.com.energyaccounting.controller;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import ru.com.energyaccounting.Main;
import ru.com.energyaccounting.dao.*;
import ru.com.energyaccounting.model.Root;

import java.io.IOException;
import java.sql.Connection;
import java.util.logging.Logger;

/**
 * Created by UserSts on 04.05.2017.
 */
public abstract class AbstractAddOrEditController<E extends Root>  extends  AbstractThreeDots implements AddOrEditInterface<E> {
    protected static final Logger LOG = Logger.getLogger(AbstractAddOrEditController.class.getName());
    protected Stage dialogStage;
    protected boolean okClicked = false;
    @FXML protected Label idLabel;
    @FXML protected TextField nameTextField;
    @FXML protected TextField descriptionTextField;
    @FXML protected Label statusLabel;
    public final String[] status = {"Неизвестно", "Изьято", "Испытывается", "В работе"};
    public static final String errorTextMessage = "Отсутствует наименование ";
    public static final String edit = "Редактирование: ";

    protected GenericDao dao;
    protected DaoFactoryInterface<Connection> factory ; // TODO: 04.05.2017  заменить эти вызовы одним классом. Зачем?
    protected Connection connection;
    E object; // FIXME: 15.05.2017  сделать универсально
    SelectMenuController selectMenuController;
    private E objectFromDao;

    public AbstractAddOrEditController(String dClass) {
        try {
            factory = new DaoFactory();
            connection = factory.getContext();
            dao = factory.getDao(connection, Class.forName(dClass));  // FIXME: 15.05.2018 вставить синглтон
        }catch (PersistException | ClassNotFoundException e){
            e.printStackTrace();
        }
    }

    @Override
    public boolean isOkClicked() {
        return okClicked;
    }

    @Override
    @FXML public void handleCancel() {
        okClicked = false;
        dialogStage.close();
    }

    @Override
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
        this.dialogStage.getIcons().add(new Image(Main.ico));
    }

    public Stage getDialogStage() {
        return dialogStage;
    }

    /**
     *
     * @param errorMessage
     * @return true если сообщение пустое
     */
    public boolean warningMessage( String errorMessage){
        if(errorMessage!=null && errorMessage.length() == 0){
            return true;
        }else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.setTitle("Не заполнены поля");
            alert.setContentText(errorMessage);
            alert.showAndWait();
            return false;
        }
    }

    protected Identifier showLayoutSelectMenu(String table) {
        LOG.info("открываем: " + table + " layout: " + Main.mSelectMenuLayout);
        try {
            FXMLLoader loaderTable = new FXMLLoader();
            loaderTable.setLocation(getClass().getClassLoader().getResource(table));
            AnchorPane pageTable = loaderTable.load();

            FXMLLoader loaderSM = new FXMLLoader();
            loaderSM.setLocation(getClass().getClassLoader().getResource(Main.mSelectMenuLayout));
            BorderPane pageSm = loaderSM.load();

            Stage dialogMenuSelectStage = new Stage();
            dialogMenuSelectStage.setTitle("Выбор элемента");
            dialogMenuSelectStage.initModality(Modality.APPLICATION_MODAL); // WINDOW_MODAL -> APPLICATION_MODAL иначе можно открывать неограниченное количество окон
            dialogMenuSelectStage.initOwner(StartChoiceBaseController.getSecondaryStage()); // баз данной строки окно исчезает
            dialogMenuSelectStage.getIcons().add(new Image(Main.ico));
            Scene scene = new Scene(pageSm);
            dialogMenuSelectStage.setScene(scene);
            pageSm.setCenter(pageTable);

            SelectMenuController selectMenuController = loaderSM.getController();
            selectMenuController.setDialogMenuSelectStage(dialogMenuSelectStage);

            AbstractMenuController menuController = loaderTable.getController();
            selectMenuController.setSelectMenuTable(menuController.getObjTable());
            selectMenuController.initializeDoubleClick();
            dialogMenuSelectStage.showAndWait();

            return selectMenuController.getIdentifier();

        } catch (IOException e) {
            ServiceMessagesController.addErrorServiceMessage("Открытие окна невозможно! " + e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Добавление или апдейт объекта в базе данных
     * @param obj
     * @throws PersistException
     */
    protected void objectPersistUpdate(E obj) throws PersistException{ // FIXME: 05.06.2018  заменить на CRUD
        boolean isInsert;
        if (obj.getId() == 0){
           objectFromDao = (E)dao.persist(obj);

            isInsert =  true;
        }else {
            objectFromDao = obj;
            dao.update(obj);
            isInsert =  false;
        }

        ServiceMessagesController.addInfoServiceMessage( obj + (isInsert ?" добавлен" : " изменен" ));

        okClicked = true;
        dialogStage.close();
    }


    @Override
    public void setObject(E object) {
        this.object = object;
    }

    public E getObjectFromDao() {
        return objectFromDao;
    }

    protected Object deleteElementFromList(TableView tableView, ObservableList list){
        Object result = null;
        int selectedIndex = tableView.getSelectionModel().getSelectedIndex();
        if (selectedIndex>=0){
            result= list.get(selectedIndex);
            list.remove(selectedIndex);
        }
        return result;
    }
}
