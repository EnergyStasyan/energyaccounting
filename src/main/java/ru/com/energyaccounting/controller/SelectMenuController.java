package ru.com.energyaccounting.controller;

import javafx.fxml.FXML;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseButton;
import javafx.stage.Stage;
import ru.com.energyaccounting.dao.Identifier;
import ru.com.energyaccounting.model.Root;

import java.util.logging.Logger;

public class SelectMenuController {

    private static final Logger log = Logger.getLogger(SelectMenuController.class.getName());

    @FXML
    private TableView<? extends Root> selectMenuTable;

    private Identifier identifier;
    private Stage dialogMenuSelectStage;

    public SelectMenuController() {}

    public Identifier getIdentifier() {
        return identifier;
    }

    public TableView<? extends Root> getSelectMenuTable() {
        return selectMenuTable;
    }

    public void setSelectMenuTable(TableView<? extends Root> selectMenuTable) {
        this.selectMenuTable = selectMenuTable;
    }

    void setDialogMenuSelectStage(Stage dialogMenuSelectStage) {
        this.dialogMenuSelectStage = dialogMenuSelectStage;
    }

    @FXML
    void handleCancel() {
        dialogMenuSelectStage.close();
    }

    @FXML
    private void handleOK() {
        log.info("Нажали на ОК");
        identifier = selectMenuTable.getSelectionModel().getSelectedItem();
        if (identifier != null) {
            log.info(selectMenuTable.getSelectionModel().getSelectedItem().toString());
            dialogMenuSelectStage.close();
        }
    }

    public void initializeDoubleClick() {
        selectMenuTable.setOnMouseClicked(mouseEvent -> {
            if (mouseEvent.getButton().equals(MouseButton.PRIMARY)
                    && mouseEvent.getClickCount() == 2
            ) {
                handleOK();
            }
        });
    }
}
