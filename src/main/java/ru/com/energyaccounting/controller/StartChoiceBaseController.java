package ru.com.energyaccounting.controller;

import javafx.application.HostServices;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.apache.commons.lang.StringUtils;
import ru.com.energyaccounting.Main;
import ru.com.energyaccounting.model.StartChoice;
import ru.com.energyaccounting.util.AddressDBSingleton;
import ru.com.energyaccounting.util.DBUtilStatic;
import ru.com.energyaccounting.util.PropertyUtils;

import java.io.*;
import java.net.URL;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.logging.Logger;

public class StartChoiceBaseController implements Initializable {
    private final static String propertyFile = "config.properties";
    private final static Logger LOG = Logger.getLogger(StartChoiceBaseController.class.getName());

    //private static String bases[];
    private static Stage secondaryStage;
    private Stage primaryStage;
    private ObservableList<StartChoice> baseList;
    private Properties property;
    @FXML
    private TableView<StartChoice> baseTableView;
    @FXML
    private TableColumn<StartChoice, String> baseTableColumn;
    @FXML
    private Label addressLabel;
    private AddressDBSingleton singleton;

    public StartChoiceBaseController() {
        baseList = FXCollections.observableArrayList();
        readFromProperties();
        singleton = AddressDBSingleton.getInstance();
    }

    public static Stage getSecondaryStage() {
        return secondaryStage;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        baseTableView.setItems(baseList);
        baseTableColumn.setCellValueFactory(cellData -> cellData.getValue().baseNameProperty());

        baseTableView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                addressLabel.setText(newValue.getBaseName() + " : " + newValue.getFilePath());
            } else {
                addressLabel.setText("");
            }
        });

        baseTableView.setOnMouseClicked(mouseEvent -> {
            if (mouseEvent.getButton().equals(MouseButton.PRIMARY)) {
                if (mouseEvent.getClickCount() == 2) {
                    handleOpen();
                }
            }
        });
    }

    private void readFromProperties() {
        property = PropertyUtils.readFromProperties(propertyFile);
        String properties = this.property.getProperty("sql.base");
        if (StringUtils.isNotEmpty(properties)) {
            String[] bases = properties.split(",");
            for (String base : bases) {
                StartChoice startChoice = new StartChoice();
                String basePath = this.property.getProperty(base + ".host");
                startChoice.setBaseName(base);
                startChoice.setFilePath(basePath);
                startChoice.setBasePath(basePath.substring(0, basePath.length() - DBUtilStatic.bdName.length()));
                baseList.add(startChoice);
            }
        }
    }

    private void clearPropertyFile(File file) {
        try (PrintWriter writer = new PrintWriter(file)) {
            writer.print("");

        } catch (FileNotFoundException e) {
            LOG.warning(e.getMessage());
            e.printStackTrace();
        }
    }

    public void saveProperty() {
        clearPropertyFile(new File(propertyFile));
        StringBuilder sb = new StringBuilder();
        try (FileOutputStream prop = new FileOutputStream(propertyFile)) {
            prop.write(("").getBytes());
            for (StartChoice startChoice : baseList) {
                if (startChoice.getBaseName() != null) {
                    sb.append(startChoice.getBaseName()).append(",");
                    property.setProperty(startChoice.getBaseName() + ".host", startChoice.getFilePath());
                }
            }
            property.setProperty("sql.base", sb.toString());
            property.store(prop, "save by button");

        } catch (IOException e) {
            e.printStackTrace();
            LOG.warning("can't write property file " + e.getMessage());
        }
    }

    @FXML
    void handleOpen() {
        try {
            StartChoice startChoice = baseTableView.getSelectionModel().getSelectedItem();
            if (startChoice != null) {
                singleton.setStartChoice(startChoice);
                getPrimaryStage().close();
                go();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void handleEdit() {
        StartChoice startChoice = baseTableView.getSelectionModel().getSelectedItem();
        if (startChoice != null) {
            openPropertyStartChoiceWindow(startChoice);
        }
    }

    @FXML
    private void handleCreate() {

        StartChoice createChoice = openPropertyStartChoiceWindow(new StartChoice());
        if (isValidCreateChoice(createChoice)) {
            baseList.add(createChoice);
        }
    }

    private boolean isValidCreateChoice(StartChoice createChoice) {
        if (createChoice.getBaseName() != null & createChoice.getFilePath() != null) {
            if (createChoice.getBaseName().length() > 0 & createChoice.getFilePath().length() > 0) {
                return true;
            }
        }
        return false;
    }

    @FXML
    private void handleDelete() {
        int selectedIndex = baseTableView.getSelectionModel().getSelectedIndex();
        StartChoice startChoice = baseTableView.getSelectionModel().getSelectedItem();
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image(Main.ico));

        alert.setTitle("Удаление");
        alert.setHeaderText(null);
        alert.setContentText("Вы действительно хотите удалить?\n" + startChoice);

        if (selectedIndex >= 0 && (alert.showAndWait().get() == ButtonType.OK) && startChoice != null) {
            baseTableView.getItems().remove(selectedIndex);
        }
    }

    private void go() {
        try {
            secondaryStage = new Stage();
            secondaryStage.setTitle("EnergyAccounting");
            secondaryStage.getIcons().add(new Image(Main.ico));
            RootController rootController = new RootController();
            rootController.Go(); //запускаем список методов

        } catch (Exception e) {
            LOG.warning("Error " + e.getMessage());
            e.printStackTrace();
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setHeaderText("УЖАСНАЯ ОШИИИБКАААА");
            alert.setTitle("AAAAAAAAAAAAAAAAAAAAAAAA");
            alert.setContentText("УЖАССС!! ВСЕ СЛОМАЛОСЬ!!!!!");
            alert.showAndWait();
        }
    }

    private StartChoice openPropertyStartChoiceWindow(StartChoice startChoice) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(StartChoicePropertyController.class.getClassLoader().getResource(Main.mStartChoicePropertyLayout));
            AnchorPane pane = loader.load();

            Stage stage = new Stage();
            if (startChoice.getBaseName() == null) {
                stage.setTitle("Добавление базы данных");
            } else {
                stage.setTitle("Редактирование базы данных");
            }

            stage.initModality(Modality.APPLICATION_MODAL);
            stage.initOwner(getPrimaryStage());
            stage.getIcons().add(new Image(Main.ico));
            Scene scene = new Scene(pane);
            stage.setScene(scene);
            StartChoicePropertyController controller = loader.getController();
            controller.setStartChoice(startChoice);
            controller.setStage(stage);
            stage.showAndWait();
        } catch (IOException e) {
            LOG.warning("Открытие окна невозможно! " + e);
            e.printStackTrace();
        }
        return startChoice;
    }

    @FXML
    private void handleExit() {
        getPrimaryStage().close();
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }
}
