package ru.com.energyaccounting.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.swing.JRViewer;
import ru.com.energyaccounting.Main;
import ru.com.energyaccounting.dao.DaoFactory;
import ru.com.energyaccounting.dao.DaoFactoryInterface;
import ru.com.energyaccounting.dao.GenericDao;
import ru.com.energyaccounting.dao.PersistException;
import ru.com.energyaccounting.model.Root;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.util.*;
import java.util.logging.Logger;

/**
 * Created by UserSts on 02.05.2017.
 * пример: PersonMenuController extends AbstractMenuController<Person, tPersonAddOrEditController>
 */
public abstract class AbstractMenuController<E extends Root, T extends AddOrEditInterface> implements Initializable {
    private static final Logger log = Logger.getLogger("AbstractMenuController");
    /**
     * универальное имя таблицы
     */
    @FXML
    protected TableView<E> objTable;
    @FXML
    protected ContextMenu objTableContextMenu;
    private final MenuItem addMenuItem = new MenuItem("Добавить");
    private final MenuItem editMenuItem = new MenuItem("Редактировать");
    private final MenuItem deleteMenuItem = new MenuItem("Удалить");
    private final MenuItem refreshMenuItem = new MenuItem("Обновить");
    private final MenuItem printMenuItem = new MenuItem("На печать");
    private final SeparatorMenuItem separator = new SeparatorMenuItem();
    private final List<MenuItem> menuItems = FXCollections.observableArrayList(addMenuItem, editMenuItem, deleteMenuItem, refreshMenuItem, separator, printMenuItem);
    /**
     * Данная конструкция для вывода на печать информацию
     */
    private JasperPrint jasperPrint;
    private final JFrame jFrame = new JFrame();
    private ObservableList<E> objData = FXCollections.observableArrayList();
    private GenericDao<E, Integer> dao;


    /**
     * определяет с каким классом будет работать. Пример:Person.class, ElectricalInstallation.class
     * пример заполнения: Person.class.getName()
     */
    @SuppressWarnings("unchecked")
    protected AbstractMenuController() {

        String thisObject = createObj().getClass().getName();
        try {
            DaoFactoryInterface<Connection> factory = new DaoFactory();
            Connection connection = factory.getContext();
            dao = factory.getDao(connection, Class.forName(thisObject));
        } catch (PersistException | ClassNotFoundException e) {
            ServiceMessagesController.addErrorServiceMessage("Добавь класс в конструктор DaoFactory()");
            log.config("Добавь класс в конструктор DaoFactory()");
            e.printStackTrace();
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        objTable.setOnMouseClicked(mouseEvent -> {
            if (mouseEvent.getButton().equals(MouseButton.PRIMARY)
                    && mouseEvent.getClickCount() == 2
            ) {
                try {
                    handleEdit();
                } catch (PersistException e) {
                    log.info(e.getClass() + " Ошибка двойного клика при вызове handleEdit: " + e);
                    e.printStackTrace();
                }
            }
        });

        menuItemsAction();
        objTableContextMenu.getItems().addAll(menuItems);
    }

    private void populateObjects(ObservableList<E> objData) {
        setObjData(objData);
        objTable.setItems(objData);
    }

    private ObservableList<E> getObjData() { //
        return objData;
    }

    public void setObjData(ObservableList<E> objData) {
        this.objData = objData;
    }

    public void searchObjects() {
        try {
            populateObjects(dao.getAll());
            log.info("получаем общие данные с бд: dao.getAll()");
        } catch (PersistException e) {
            e.printStackTrace();
        }
    }

    /*
        костыль для записи аудита в отдельную бд
     */
    public void searchObjects(Connection connection) {
        try {
            populateObjects(dao.getAll(connection));
            log.info("получаем общие данные с бд: dao.getAll()");
        } catch (PersistException e) {
            e.printStackTrace();
        }
    }

    /**
     * Данные костыли нужны, так как не нашел, каким образом делать новый обхект
     * примерно: return new Person();
     *
     * @return объект
     */
    protected abstract E createObj();

    /**
     * указываем с каким именно слоем для редактирования объекта нужно работать
     * возвращает адрес слоя
     *
     * @return String
     */
    protected abstract String editOrAddLayout();

    /**
     * кнопка добавления
     * E tempE =  createObj(); <- return new Person();
     * boolean isClicked = showLayoutAddOrEdit(tempE);
     * if (isClicked){
     * getObjectObservableListData().add(tempE);
     * }
     * searchObjects();
     */
    @FXML
    public void handleAdd() throws PersistException {
        log.info("Пытаемся добавить объект");
        E tempObj = createObj();
        boolean isClicked = showLayoutAddOrEdit(tempObj);
        if (isClicked) {
            getObjData().add(tempObj);
        }
        searchObjects();
    }

    /**
     * кнопка редактирования
     * * E selectedE = <>E</>Table.getSelectionModel().getSelectedItem();
     * if (selectedE != null){
     * showLayoutAddOrEdit(selectedE);
     * }
     */
    @FXML
    public void handleEdit() throws PersistException {
        log.info("Редактируем объект");
        E selectedObj = objTable.getSelectionModel().getSelectedItem();
        if (selectedObj != null) {
            showLayoutAddOrEdit(selectedObj);
        }
    }

    /**
     * кнопка удаления
     */
    @FXML
    public void handleDelete() throws PersistException {
        log.info("Удаляем объект");
        int selectedIndex = objTable.getSelectionModel().getSelectedIndex();
        E EObject = objTable.getSelectionModel().getSelectedItem();
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);

        Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image(Main.ico));

        alert.setTitle("Удаление");
        alert.setHeaderText(null);
        alert.setContentText("Вы действительно хотите удалить?\n" + EObject);


        if (selectedIndex >= 0
                && alert.showAndWait().get() == ButtonType.OK
                && EObject != null) {

            dao.delete(EObject);
            objTable.getItems().remove(selectedIndex);

            ServiceMessagesController.addInfoServiceMessage("Удален: " + EObject.toString());
        }
    }

    /**
     * кнопка обновления
     */
    @FXML
    public void handleRefresh() {
        searchObjects();
    }

    /**
     * метод вызова диалогового окна для редактирования и создания нового объекта
     *
     * @param object
     * @return
     */
    protected boolean showLayoutAddOrEdit(E object) throws PersistException {
        try {
            FXMLLoader loader = new FXMLLoader();
            String layout = editOrAddLayout();
            log.info("Пытаемся открыть окно редактирования объекта : " + layout);
            loader.setLocation(getClass().getClassLoader().getResource(layout));
            AnchorPane page = loader.load();

            //Создаем Диалогове окно
            log.info("Пытаемся создать окно редактирования объекта");
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Редактирование...");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(StartChoiceBaseController.getSecondaryStage()); // получаем статическую primaryStage и указываем ее как хозяина
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            //загоняем сотрудника в контролер
            log.info("Пытаемся загрузить контролер редактирования объекта");
            T controller = loader.getController();
            controller.setDialogStage(dialogStage);

            if (object != null) {
                controller.setObject(object);
            }

            //показываем диалог и ждем закрытия
            dialogStage.showAndWait();

            return controller.isOkClicked();

        } catch (IOException e) {
            log.warning("\nКонструктор ObjectAddOrEditController проверь входящий параметр, необходимо его удалить!!!!\n");
            e.getStackTrace();
            throw new PersistException("Открытие окна невозможно! " + e.getMessage(), e);

        }
    }

    /**
     * возвращает имя паттерна JasperReport
     *
     * @return String
     */
    abstract protected String getPathForJasperPattern();

    // FIXME: 01.07.2017 сменинть void -> JasperPrint и конструкция будет  JRViewer viewer = new JRViewer(getJasperPrint());
    protected void prepareForJasperPrint() throws JRException, PersistException {
        JRBeanCollectionDataSource beanColDataSource = new JRBeanCollectionDataSource(getObjData());
        File reportPattern;
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("DATE", new Date());

        log.info("Начало вывода джаспер");
        ClassLoader classLoader = getClass().getClassLoader();
        try {
            reportPattern = new File(classLoader.getResource("jrxml/menu/" + getPathForJasperPattern()).getFile());
            log.info(classLoader.getResource("jrxml/menu/testPattern.jrxml").toString());
        } catch (NullPointerException e) {
            throw new PersistException("Ошибка загрузки jrxml: " + e);
        }


        log.info("джаспер вывод создание объекта файл");
        JasperDesign jasperDesign = JRXmlLoader.load(reportPattern);
        JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
        jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, beanColDataSource);
    }

    @FXML
    protected void previewForJasperPrint() throws JRException, PersistException {
        log.fine("Ошибка вывода: previewForJasperPrint");
        prepareForJasperPrint();
        JRViewer viewer = new JRViewer(jasperPrint);
        viewer.setOpaque(true);
        viewer.setVisible(true);
        jFrame.add(viewer);
        jFrame.setSize(700, 500);
        jFrame.setVisible(true);

        jFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE); //для принудительного закрытия программы, иначе оно висело в оперативе

        log.fine("удачный вывод: previewForJasperPrint");

    }

    @FXML
    protected void copyToClipboard() {  // FIXME: 07.07.2017 копирование в буфер. доделать
        System.out.println("Copied!");
        final Clipboard clipboard = Clipboard.getSystemClipboard();
        final ClipboardContent content = new ClipboardContent();

        int row = objTable.getSelectionModel().getSelectedIndex();
        String rowString = objTable.getContextMenu().getId();
        System.out.println(rowString);
        clipboard.setContent(content);
    }

    // установка управления контекстного меню
    private void menuItemsAction() {
        addMenuItem.setOnAction(event -> {
            try {
                handleAdd();
            } catch (PersistException e) {
                log.warning("menuItemsAction");
                e.getStackTrace();
            }
        });
        editMenuItem.setOnAction(event -> {
            try {
                handleEdit();
            } catch (PersistException e) {
                log.warning("menuItemsAction");
                e.getStackTrace();
            }
        });
        deleteMenuItem.setOnAction(event -> {
            try {
                handleDelete();
            } catch (PersistException e) {
                log.warning("menuItemsAction");
                e.getStackTrace();
            }
        });
        refreshMenuItem.setOnAction(event -> handleRefresh());

        printMenuItem.setOnAction(event -> handleRefresh());

    }

    public TableView<E> getObjTable() {
        return objTable;
    }
}
