package ru.com.energyaccounting.controller;

import javafx.fxml.FXML;

/**
 * Created by UserSts on 15.05.2017.
 */
public abstract class AbstractThreeDots {
    @FXML public void threeDotsSubdivision(){}
    @FXML public void threeDotsExtinguisher(){}
    @FXML public void threeDotsExtinguisherType(){}

    @FXML public void threeDotsOrganization(){}

    @FXML public void threeDotsVoltageType(){}

    @FXML public void threeDotsElectricalEquipmentType(){}

    @FXML public void threeDotsSubstation(){}

    @FXML public void threeDotsIPMType(){}

    @FXML public void threeDotsElectricalInstallation(){}

    @FXML public void threeDotsPosition(){}

    @FXML public void threeDotsPerson(){}
}
