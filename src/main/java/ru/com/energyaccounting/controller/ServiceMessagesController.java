package ru.com.energyaccounting.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import ru.com.energyaccounting.model.MessagesType;
import ru.com.energyaccounting.model.ServiceMessageDAOStatic;

import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;
/*
возможный стандарт сообщиний ошибок
ServiceMessagesController.addErrorServiceMessage(this.getClass().getSimpleName() + " : " + e.getMessage());
 */

/**
 * Created by UserSts on 01.03.2017.
 */

public class ServiceMessagesController implements Initializable {

   // private static LocalDateTime LDT;
    static DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    static String LDTString = LocalDateTime.now().format(dateTimeFormatter) ; //создаем текущее время
    private static ObservableList<String> observableList = FXCollections.observableArrayList();
    @FXML  public ListView<String> listViewServiceMessage;
    private RootController rootController;

    public static void addInfoServiceMessage( String description) {

        String fullMessage = LDTString + " : "+ description; //замечание: в сервисное окно выводится время через LocalDateTime.now(), SQL ставит время сама
        observableList.add(fullMessage);                     //добавляем в лист выводящий информацию в footer программы
        ServiceMessageDAOStatic.insertServiceMessage(MessagesType.INFO.toString(), description);  //производим запись сообщения/ошибки в БД
        /*
        -id- | -eventTime-sql | -messageType- | description
         */
    }

    public static void addErrorServiceMessage(String description){
        String fullMessage = LDTString + " : "+ description;        //замечание: в сервисное окно выводится время через LocalDateTime.now(), SQL ставит время сама
        observableList.add(fullMessage);                           //добавляем в лист выводящий информацию в footer программы
        ServiceMessageDAOStatic.insertServiceMessage(MessagesType.ERROR.toString(), description);     //производим запись сообщения/ошибки в БД
        /*
        -id- | -eventTime-sql | -messageType- | description
         */
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        listViewServiceMessage.setItems(observableList);
    }

    public void setRootController(RootController rootController) {
        this.rootController = rootController;
    }
}

