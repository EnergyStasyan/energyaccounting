package ru.com.energyaccounting.model;

/**
 * Created by UserSts on 23.05.2017.
 */
public class ExtinguisherType extends Root  {

    @Override
    public String toString() {
        return "Тип огнетушителя: " + getName();
    }

    @Override
    public String getDialogStageTitle() {
        return "Типа Огнетушителя";
    }
}
