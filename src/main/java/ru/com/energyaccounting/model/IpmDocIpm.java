package ru.com.energyaccounting.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

public class IpmDocIpm extends IPM {
    private final IntegerProperty doc_id;
    private final IntegerProperty ipm_id;

    public IpmDocIpm() {
        super();
        this.doc_id = new SimpleIntegerProperty();
        this.ipm_id = new SimpleIntegerProperty();
    }

    public int getDoc_id() {
        return doc_id.get();
    }

    public IntegerProperty doc_idProperty() {
        return doc_id;
    }

    public void setDoc_id(int doc_id) {
        this.doc_id.set(doc_id);
    }

    public int getIpm_id() {
        return ipm_id.get();
    }

    public IntegerProperty ipm_idProperty() {
        return ipm_id;
    }

    public void setIpm_id(int ipm_id) {
        this.ipm_id.set(ipm_id);
    }
}
