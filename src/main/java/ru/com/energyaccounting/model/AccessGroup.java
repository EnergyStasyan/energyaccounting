package ru.com.energyaccounting.model;

import javafx.beans.property.*;

import java.time.LocalDate;

/**
 * Created by UserSts on 06.07.2017.
 */
public class AccessGroup extends Root {

    private final StringProperty numberOfOrder;
    private final StringProperty numberOfAccessGroup;
    private final SimpleObjectProperty<LocalDate> orderDate;

    private final IntegerProperty directorId;
    private final StringProperty director;
    private final IntegerProperty personId;
    private final StringProperty person;
    //с какой даты приступать
    //директор
    //номер приказа
    //    номер группы
    // фио сотружника


    public AccessGroup() {
        this.numberOfOrder = new SimpleStringProperty();
        this.numberOfAccessGroup = new SimpleStringProperty();

        this.orderDate = new SimpleObjectProperty<>();
        this.director = new SimpleStringProperty();
        this.directorId = new SimpleIntegerProperty();
        this.person = new SimpleStringProperty();
        this.personId = new SimpleIntegerProperty();
    }

    public String getNumberOfOrder() {
        return numberOfOrder.get();
    }

    public void setNumberOfOrder(String numberOfOrder) {
        this.numberOfOrder.set(numberOfOrder);
    }

    public StringProperty numberOfOrderProperty() {
        return numberOfOrder;
    }

    public String getNumberOfAccessGroup() {
        return numberOfAccessGroup.get();
    }

    public void setNumberOfAccessGroup(String numberOfAccessGroup) {
        this.numberOfAccessGroup.set(numberOfAccessGroup);
    }

    public StringProperty numberOfAccessGroupProperty() {
        return numberOfAccessGroup;
    }

    public LocalDate getOrderDate() {
        return orderDate.get();
    }

    public void setOrderDate(LocalDate orderDate) {
        this.orderDate.set(orderDate);
    }

    public SimpleObjectProperty<LocalDate> orderDateProperty() {
        return orderDate;
    }

    public String getDirector() {
        return director.get();
    }

    public void setDirector(String director) {
        this.director.set(director);
    }

    public StringProperty directorProperty() {
        return director;
    }

    public String getPerson() {
        return person.get();
    }

    public void setPerson(String person) {
        this.person.set(person);
    }

    public StringProperty personProperty() {
        return person;
    }

    public int getDirectorId() {
        return directorId.get();
    }

    public void setDirectorId(int directorId) {
        this.directorId.set(directorId);
    }

    public IntegerProperty directorIdProperty() {
        return directorId;
    }

    public int getPersonId() {
        return personId.get();
    }

    public void setPersonId(int personId) {
        this.personId.set(personId);
    }

    public IntegerProperty personIdProperty() {
        return personId;
    }

    @Override
    public String toString() {
        return "Приказ номер: " + getNumberOfOrder() + " Сотрудник: " + getPerson() + " Группа допуска: "+ getNumberOfAccessGroup();
    }

    @Override
    public String getDialogStageTitle() {
        return "Группы допуска";
    }
}
