package ru.com.energyaccounting.model;

import javafx.beans.property.SimpleObjectProperty;

import java.time.LocalDateTime;

public abstract class Docs extends Root {
    private final SimpleObjectProperty<LocalDateTime> docDateTime;

    public Docs() {
        this.docDateTime = new SimpleObjectProperty<>();
    }

    public LocalDateTime getDocDateTime() {
        return docDateTime.get();
    }

    public SimpleObjectProperty<LocalDateTime> docDateTimeProperty() {
        return docDateTime;
    }

    public void setDocDateTime(LocalDateTime docDateTime) {
        this.docDateTime.set(docDateTime);
    }
}
