package ru.com.energyaccounting.model;


import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class OrderDocPerson extends Root {

    private final IntegerProperty orderDocId;
    private final StringProperty status;
    private final IntegerProperty statusId;
    private final IntegerProperty personId;
    private final StringProperty person;
    private final StringProperty additional;

    public OrderDocPerson() {
        this.orderDocId = new SimpleIntegerProperty();
        this.statusId = new SimpleIntegerProperty();
        this.personId = new SimpleIntegerProperty();
        this.status = new SimpleStringProperty();
        this.person = new SimpleStringProperty();
        this.additional = new SimpleStringProperty();
    }

    public String getAdditional() {
        return additional.get();
    }

    public void setAdditional(String additional) {
        this.additional.set(additional);
    }

    public StringProperty additionalProperty() {
        return additional;
    }

    public String getStatus() {
        return status.get();
    }

    public void setStatus(String status) {
        this.status.set(status);
    }

    public StringProperty statusProperty() {
        return status;
    }

    public int getStatusId() {
        return statusId.get();
    }

    public void setStatusId(int statusId) {
        this.statusId.set(statusId);
    }

    public IntegerProperty statusIdProperty() {
        return statusId;
    }

    public int getPersonId() {
        return personId.get();
    }

    public void setPersonId(int personId) {
        this.personId.set(personId);
    }

    public IntegerProperty personIdProperty() {
        return personId;
    }

    public String getPerson() {
        return person.get();
    }

    public void setPerson(String person) {
        this.person.set(person);
    }

    public StringProperty personProperty() {
        return person;
    }

    public int getOrderDocId() {
        return orderDocId.get();
    }

    public void setOrderDocId(int orderDocId) {
        this.orderDocId.set(orderDocId);
    }

    public IntegerProperty orderDocIdProperty() {
        return orderDocId;
    }

    @Override
    public String getDialogStageTitle() {
        return "";
    }
}
