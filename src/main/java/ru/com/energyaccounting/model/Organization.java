package ru.com.energyaccounting.model;

/**
 * Created by UserSts on 20.05.2017.
 */
public class Organization extends Root {

    @Override
    public String toString() {
        return "Организация: " + getName();
    }

    @Override
    public String getDialogStageTitle() {
        return "Организации";
    }
}
