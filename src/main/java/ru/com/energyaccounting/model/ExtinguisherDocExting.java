package ru.com.energyaccounting.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

public class ExtinguisherDocExting extends Extinguisher {
    private final IntegerProperty doc_id;
    private final IntegerProperty ext_id;

    public ExtinguisherDocExting() {
        super();
        this.doc_id = new SimpleIntegerProperty();
        this.ext_id = new SimpleIntegerProperty();
    }

    public int getDoc_id() {
        return doc_id.get();
    }

    public IntegerProperty doc_idProperty() {
        return doc_id;
    }

    public void setDoc_id(int doc_id) {
        this.doc_id.set(doc_id);
    }

    public int getExt_id() {
        return ext_id.get();
    }

    public IntegerProperty ext_idProperty() {
        return ext_id;
    }

    public void setExt_id(int ext_id) {
        this.ext_id.set(ext_id);
    }
}
