package ru.com.energyaccounting.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class SubstationElInstallationElEquipment extends Root {
    private final IntegerProperty substationId;
    private final StringProperty substation;
    private final IntegerProperty electricalInstallationsId;
    private final StringProperty electricalInstallation;
    private final StringProperty electricalEquipment;
    private final IntegerProperty electricalEquipmentId;

    public SubstationElInstallationElEquipment() {
        this.substationId =  new SimpleIntegerProperty();
        this.substation = new SimpleStringProperty();

        this.electricalEquipmentId = new SimpleIntegerProperty();
        this.electricalEquipment = new SimpleStringProperty();
        this.electricalInstallationsId = new SimpleIntegerProperty();
        this.electricalInstallation = new SimpleStringProperty();
    }

    public int getSubstationId() {
        return substationId.get();
    }

    public void setSubstationId(int substationId) {
        this.substationId.set(substationId);
    }

    public IntegerProperty substationIdProperty() {
        return substationId;
    }

    public String getSubstation() {
        return substation.get();
    }

    public void setSubstation(String substation) {
        this.substation.set(substation);
    }

    public StringProperty substationProperty() {
        return substation;
    }

    public int getElectricalInstallationsId() {
        return electricalInstallationsId.get();
    }

    public void setElectricalInstallationsId(int electricalInstallationsId) {
        this.electricalInstallationsId.set(electricalInstallationsId);
    }

    public IntegerProperty electricalInstallationsIdProperty() {
        return electricalInstallationsId;
    }

    public String getElectricalInstallation() {
        return electricalInstallation.get();
    }

    public void setElectricalInstallation(String electricalInstallation) {
        this.electricalInstallation.set(electricalInstallation);
    }

    public StringProperty electricalInstallationProperty() {
        return electricalInstallation;
    }

    public String getElectricalEquipment() {
        return electricalEquipment.get();
    }

    public void setElectricalEquipment(String electricalEquipment) {
        this.electricalEquipment.set(electricalEquipment);
    }

    public StringProperty electricalEquipmentProperty() {
        return electricalEquipment;
    }

    public int getElectricalEquipmentId() {
        return electricalEquipmentId.get();
    }

    public void setElectricalEquipmentId(int electricalEquipmentId) {
        this.electricalEquipmentId.set(electricalEquipmentId);
    }

    public IntegerProperty electricalEquipmentIdProperty() {
        return electricalEquipmentId;
    }

    @Override
    public String getDialogStageTitle() {
        return null;
    }
}
