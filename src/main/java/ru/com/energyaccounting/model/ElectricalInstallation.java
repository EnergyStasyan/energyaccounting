package ru.com.energyaccounting.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import ru.com.energyaccounting.dao.Identifier;

/**
 * Created by UserSts on 01.03.2017.
 * Электроустановка. Пример: РП-2, ТП-9
 */
public class ElectricalInstallation extends Root implements Identifier<Integer>{

    private final StringProperty voltageType;
    private final IntegerProperty vt_id;
    private final StringProperty shift;
    private final IntegerProperty shift_id;
    private final StringProperty substation;
    private final IntegerProperty substat_id;

    public ElectricalInstallation(){
        this.voltageType = new SimpleStringProperty();
        this.vt_id = new SimpleIntegerProperty();
        this.substation = new SimpleStringProperty();
        this.substat_id = new SimpleIntegerProperty();
        this.shift = new SimpleStringProperty();
        this.shift_id = new SimpleIntegerProperty();
    }


    public String getVoltageType() {
        return voltageType.get();
    }

    public void setVoltageType(String voltageType) {
        this.voltageType.set(voltageType);
    }

    public StringProperty voltageTypeProperty() {
        return voltageType;
    }

    public String getSubstation() {
        return substation.get();
    }

    public void setSubstation(String substation) {
        this.substation.set(substation);
    }

    public StringProperty substationProperty() {
        return substation;
    }

    public int getVt_id() {
        return vt_id.get();
    }

    public void setVt_id(int vt_id) {
        this.vt_id.set(vt_id);
    }

    public IntegerProperty vt_idProperty() {
        return vt_id;
    }

    public String getShift() {
        return shift.get();
    }

    public void setShift(String shift) {
        this.shift.set(shift);
    }

    public StringProperty shiftProperty() {
        return shift;
    }

    public int getShift_id() {
        return shift_id.get();
    }

    public void setShift_id(int shift_id) {
        this.shift_id.set(shift_id);
    }

    public IntegerProperty shift_idProperty() {
        return shift_id;
    }

    public int getSubstat_id() {
        return substat_id.get();
    }

    public void setSubstat_id(int substat_id) {
        this.substat_id.set(substat_id);
    }

    public IntegerProperty substat_idProperty() {
        return substat_id;
    }

    @Override
    public String toString() {
        return getName();
    }

    @Override
    public String getDialogStageTitle() {
        return "Электроустановки";
    }
}
