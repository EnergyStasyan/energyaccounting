package ru.com.energyaccounting.model;

import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ObservableList;

import java.time.LocalDateTime;

public class IpmDoc extends Docs {

    private final ListProperty<IpmDocIpm> ipmList;

    public IpmDoc() {
        this.ipmList = new SimpleListProperty<>();
    }

    public ObservableList<IpmDocIpm> getIpmList() {
        return ipmList.get();
    }

    public ListProperty<IpmDocIpm> ipmListProperty() {
        return ipmList;
    }

    public void setIpmList(ObservableList<IpmDocIpm> ipmList) {
        this.ipmList.set(ipmList);
    }


    @Override
    public String getDialogStageTitle() {
        return "Средств защит";
    }

    @Override
    public String toString() {
        return "Средства защит: №" + getId() + " от " + getDocDateTime();
    }
}
