package ru.com.energyaccounting.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Created by UserSts on 14.04.2017.
 */
public class Subdivision extends Root {

    private final IntegerProperty org_id;
    private final StringProperty organization;

    public Subdivision (){
        this.org_id = new SimpleIntegerProperty();
        this.organization = new SimpleStringProperty();
    }


    public int getOrg_id() {
        return org_id.get();
    }

    public void setOrg_id(int org_id) {
        this.org_id.set(org_id);
    }

    public IntegerProperty org_idProperty() {
        return org_id;
    }

    public String getOrganization() {
        return organization.get();
    }

    public void setOrganization(String organization) {
        this.organization.set(organization);
    }

    public StringProperty organizationProperty() {
        return organization;
    }

    @Override
    public String toString() {
        return "Подразделение: " + getName();
    }

    @Override
    public String getDialogStageTitle() {
        return "Подразделения";
    }
}
