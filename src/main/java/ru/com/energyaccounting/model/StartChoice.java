package ru.com.energyaccounting.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class StartChoice {

    private final IntegerProperty id;
    private final StringProperty baseName;
    private final StringProperty basePath;
    private final StringProperty filePath;
    private final StringProperty description;

    public StartChoice() {
        this.id = new SimpleIntegerProperty();
        this.baseName = new SimpleStringProperty();
        this.basePath = new SimpleStringProperty();
        this.filePath = new SimpleStringProperty();
        this.description = new SimpleStringProperty();
    }

    public int getId() {
        return id.get();
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public IntegerProperty idProperty() {
        return id;
    }

    public String getBaseName() {
        return baseName.get();
    }

    public void setBaseName(String baseName) {
        this.baseName.set(baseName);
    }

    public StringProperty baseNameProperty() {
        return baseName;
    }
    public String getBasePath() {
        return basePath.get();
    }

    public void setBasePath(String basePath) {
        this.basePath.set(basePath);
    }

    public StringProperty basePathProperty() {
        return basePath;
    }

    public String getDescription() {
        return description.get();
    }

    public void setDescription(String description) {
        this.description.set(description);
    }

    public StringProperty descriptionProperty() {
        return description;
    }

    public String getFilePath() {
        return filePath.get();
    }

    public void setFilePath(String filePath) {
        this.filePath.set(filePath);
    }

    public StringProperty filePathProperty() {
        return filePath;
    }

    @Override
    public String toString() {
        return getBaseName() + " : " + getFilePath();
    }
}
