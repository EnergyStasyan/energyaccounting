package ru.com.energyaccounting.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Created by UserSts on 23.05.2017.
 */
public class Extinguisher extends Root {
    private final StringProperty extinguisherType;
    private final IntegerProperty ext_type_id;
    private final StringProperty electricalInstallation;
    private final IntegerProperty el_install_id;
    private final StringProperty status;

    public Extinguisher() {
        super();
        this.extinguisherType = new SimpleStringProperty();
        this.ext_type_id = new SimpleIntegerProperty();
        this.electricalInstallation = new SimpleStringProperty();
        this.el_install_id = new SimpleIntegerProperty();
        this.status = new SimpleStringProperty();
    }

    public String getExtinguisherType() {
        return extinguisherType.get();
    }

    public void setExtinguisherType(String extinguisherType) {
        this.extinguisherType.set(extinguisherType);
    }

    public StringProperty extinguisherTypeProperty() {
        return extinguisherType;
    }

    public int getExt_type_id() {
        return ext_type_id.get();
    }

    public void setExt_type_id(int ext_type_id) {
        this.ext_type_id.set(ext_type_id);
    }

    public IntegerProperty ext_type_idProperty() {
        return ext_type_id;
    }

    public String getElectricalInstallation() {
        return electricalInstallation.get();
    }

    public void setElectricalInstallation(String electricalInstallation) {
        this.electricalInstallation.set(electricalInstallation);
    }

    public StringProperty electricalInstallationProperty() {
        return electricalInstallation;
    }

    public int getEl_install_id() {
        return el_install_id.get();
    }

    public void setEl_install_id(int el_install_id) {
        this.el_install_id.set(el_install_id);
    }

    public IntegerProperty el_install_idProperty() {
        return el_install_id;
    }

    public String getStatus() {
        return status.get();
    }

    public void setStatus(String status) {
        this.status.set(status);
    }

    public StringProperty statusProperty() {
        return status;
    }

    @Override
    public String toString() {
        return "Огнетушитель: " + getExtinguisherType();
    }

    @Override
    public String getDialogStageTitle() {
        return "Огнетушителя";
    }
}
