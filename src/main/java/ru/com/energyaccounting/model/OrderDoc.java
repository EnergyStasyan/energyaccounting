package ru.com.energyaccounting.model;

import javafx.beans.property.*;
import javafx.collections.ObservableList;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

/**
 * Created by UserSts on 26.06.2017.
 */
public class OrderDoc extends Root {

    private final StringProperty number;

    private final StringProperty substation;
    private final IntegerProperty substatId;

    private final StringProperty subdivision;
    private final IntegerProperty subdivId;

    private final StringProperty organization;
    private final IntegerProperty orgId;


    private final ObjectProperty<LocalDateTime> dateTimeOfIssue;
    private final ObjectProperty<LocalDate> dateOfIssue;
    private final ObjectProperty<LocalTime> timeOfIssue;

    private final ObjectProperty<LocalDateTime> dateTimeStart;
    private final ObjectProperty<LocalDate> dateStart;
    private final ObjectProperty<LocalTime> timeStart;

    private final ObjectProperty<LocalDateTime> dateTimeEnd;
    private final ObjectProperty<LocalDate> dateEnd;
    private final ObjectProperty<LocalTime> timeEnd;

    private final IntegerProperty issueWorkPermitManId; //выдающий наряд
    private final StringProperty issueWorkPermitManNameAccessGroup;

    private final IntegerProperty viewerId;     //наблюдающий
    private final StringProperty viewerNameAccessGroup;

    private final IntegerProperty performingAuthorityId; //ответсвенный руководитель работ
    private final StringProperty performingAuthorityNameAccessGroup;

    private final IntegerProperty allowingManId; //допускающий
    private final StringProperty allowingManNameAccessGroup;

    private final IntegerProperty foremanId; //производитель работ
    private final StringProperty foremanNameAccessGroup;

    private final ListProperty<OrderDocPerson> crewMember; //член бригады
    //private final MapProperty<MapProperty, String> crewMemberExtra; //член бригады

    private final StringProperty instructTo; //поручается

    public OrderDoc() {
        this.number = new SimpleStringProperty();

        this.substation = new SimpleStringProperty();
        this.substatId = new SimpleIntegerProperty();

        this.subdivision = new SimpleStringProperty();
        this.subdivId = new SimpleIntegerProperty();

        this.organization = new SimpleStringProperty();
        this.orgId = new SimpleIntegerProperty();

        this.dateTimeOfIssue = new SimpleObjectProperty<>();
        this.timeOfIssue = new SimpleObjectProperty<>();
        this.dateOfIssue = new SimpleObjectProperty<>();

        this.dateTimeStart = new SimpleObjectProperty<>();
        this.dateStart = new SimpleObjectProperty<>();
        this.timeStart = new SimpleObjectProperty<>();

        this.dateTimeEnd = new SimpleObjectProperty<>();
        this.dateEnd = new SimpleObjectProperty<>();
        this.timeEnd = new SimpleObjectProperty<>();

        this.issueWorkPermitManNameAccessGroup = new SimpleStringProperty();
        this.issueWorkPermitManId = new SimpleIntegerProperty();

        this.viewerNameAccessGroup = new SimpleStringProperty();
        this.viewerId = new SimpleIntegerProperty();

        this.performingAuthorityNameAccessGroup = new SimpleStringProperty();
        this.performingAuthorityId = new SimpleIntegerProperty();

        this.foremanNameAccessGroup = new SimpleStringProperty();
        this.foremanId = new SimpleIntegerProperty();

        this.allowingManNameAccessGroup = new SimpleStringProperty();
        this.allowingManId = new SimpleIntegerProperty();

        this.crewMember = new SimpleListProperty<>();

        this.instructTo = new SimpleStringProperty();
    }

    public String getNumber() {
        return number.get();
    }

    public void setNumber(String number) {
        this.number.set(number);
    }

    public StringProperty numberProperty() {
        return number;
    }

    public String getSubstation() {
        return substation.get();
    }

    public void setSubstation(String substation) {
        this.substation.set(substation);
    }

    public StringProperty substationProperty() {
        return substation;
    }

    public int getSubstatId() {
        return substatId.get();
    }

    public void setSubstatId(int substatId) {
        this.substatId.set(substatId);
    }

    public IntegerProperty substatIdProperty() {
        return substatId;
    }

    public String getSubdivision() {
        return subdivision.get();
    }

    public void setSubdivision(String subdivision) {
        this.subdivision.set(subdivision);
    }

    public StringProperty subdivisionProperty() {
        return subdivision;
    }

    public int getSubdivId() {
        return subdivId.get();
    }

    public void setSubdivId(int subdivId) {
        this.subdivId.set(subdivId);
    }

    public IntegerProperty subdivIdProperty() {
        return subdivId;
    }

    public String getOrganization() {
        return organization.get();
    }

    public void setOrganization(String organization) {
        this.organization.set(organization);
    }

    public StringProperty organizationProperty() {
        return organization;
    }

    public int getOrgId() {
        return orgId.get();
    }

    public void setOrgId(int orgId) {
        this.orgId.set(orgId);
    }

    public IntegerProperty orgIdProperty() {
        return orgId;
    }

    public LocalDateTime getDateTimeOfIssue() {
        return dateTimeOfIssue.get();
    }

    public void setDateTimeOfIssue(LocalDateTime dateTimeOfIssue) {
        this.dateTimeOfIssue.set(dateTimeOfIssue);
    }

    public ObjectProperty<LocalDateTime> dateTimeOfIssueProperty() {
        return dateTimeOfIssue;
    }

    public LocalDate getDateOfIssue() {
        return dateOfIssue.get();
    }

    public void setDateOfIssue(LocalDate dateOfIssue) {
        this.dateOfIssue.set(dateOfIssue);
    }

    public ObjectProperty<LocalDate> dateOfIssueProperty() {
        return dateOfIssue;
    }

    public LocalTime getTimeOfIssue() {
        return timeOfIssue.get();
    }

    public void setTimeOfIssue(LocalTime timeOfIssue) {
        this.timeOfIssue.set(timeOfIssue);
    }

    public ObjectProperty<LocalTime> timeOfIssueProperty() {
        return timeOfIssue;
    }

    public LocalDate getDateStart() {
        return dateStart.get();
    }

    public void setDateStart(LocalDate dateStart) {
        this.dateStart.set(dateStart);
    }

    public ObjectProperty<LocalDate> dateStartProperty() {
        return dateStart;
    }

    public LocalTime getTimeStart() {
        return timeStart.get();
    }

    public void setTimeStart(LocalTime timeStart) {
        this.timeStart.set(timeStart);
    }

    public ObjectProperty<LocalTime> timeStartProperty() {
        return timeStart;
    }

    public LocalDate getDateEnd() {
        return dateEnd.get();
    }

    public void setDateEnd(LocalDate dateEnd) {
        this.dateEnd.set(dateEnd);
    }

    public ObjectProperty<LocalDate> dateEndProperty() {
        return dateEnd;
    }

    public LocalTime getTimeEnd() {
        return timeEnd.get();
    }

    public void setTimeEnd(LocalTime timeEnd) {
        this.timeEnd.set(timeEnd);
    }

    public ObjectProperty<LocalTime> timeEndProperty() {
        return timeEnd;
    }

    public LocalDateTime getDateTimeStart() {
        return dateTimeStart.get();
    }

    public void setDateTimeStart(LocalDateTime dateTimeStart) {
        this.dateTimeStart.set(dateTimeStart);
    }

    public ObjectProperty<LocalDateTime> dateTimeStartProperty() {
        return dateTimeStart;
    }

    public LocalDateTime getDateTimeEnd() {
        return dateTimeEnd.get();
    }

    public void setDateTimeEnd(LocalDateTime dateTimeEnd) {
        this.dateTimeEnd.set(dateTimeEnd);
    }

    public ObjectProperty<LocalDateTime> dateTimeEndProperty() {
        return dateTimeEnd;
    }

    public int getIssueWorkPermitManId() {
        return issueWorkPermitManId.get();
    }

    public void setIssueWorkPermitManId(int issueWorkPermitManId) {
        this.issueWorkPermitManId.set(issueWorkPermitManId);
    }

    public IntegerProperty issueWorkPermitManIdProperty() {
        return issueWorkPermitManId;
    }

    public String getIssueWorkPermitManNameAccessGroup() {
        return issueWorkPermitManNameAccessGroup.get();
    }

    public void setIssueWorkPermitManNameAccessGroup(String issueWorkPermitManNameAccessGroup) {
        this.issueWorkPermitManNameAccessGroup.set(issueWorkPermitManNameAccessGroup);
    }

    public StringProperty issueWorkPermitManNameAccessGroupProperty() {
        return issueWorkPermitManNameAccessGroup;
    }

    public int getViewerId() {
        return viewerId.get();
    }

    public void setViewerId(int viewerId) {
        this.viewerId.set(viewerId);
    }

    public IntegerProperty viewerIdProperty() {
        return viewerId;
    }

    public String getViewerNameAccessGroup() {
        return viewerNameAccessGroup.get();
    }

    public void setViewerNameAccessGroup(String viewerNameAccessGroup) {
        this.viewerNameAccessGroup.set(viewerNameAccessGroup);
    }

    public StringProperty viewerNameAccessGroupProperty() {
        return viewerNameAccessGroup;
    }

    public int getPerformingAuthorityId() {
        return performingAuthorityId.get();
    }

    public void setPerformingAuthorityId(int performingAuthorityId) {
        this.performingAuthorityId.set(performingAuthorityId);
    }

    public IntegerProperty performingAuthorityIdProperty() {
        return performingAuthorityId;
    }

    public String getPerformingAuthorityNameAccessGroup() {
        return performingAuthorityNameAccessGroup.get();
    }

    public void setPerformingAuthorityNameAccessGroup(String performingAuthorityNameAccessGroup) {
        this.performingAuthorityNameAccessGroup.set(performingAuthorityNameAccessGroup);
    }

    public StringProperty performingAuthorityNameAccessGroupProperty() {
        return performingAuthorityNameAccessGroup;
    }

    public int getAllowingManId() {
        return allowingManId.get();
    }

    public void setAllowingManId(int allowingManId) {
        this.allowingManId.set(allowingManId);
    }

    public IntegerProperty allowingManIdProperty() {
        return allowingManId;
    }

    public String getAllowingManNameAccessGroup() {
        return allowingManNameAccessGroup.get();
    }

    public void setAllowingManNameAccessGroup(String allowingManNameAccessGroup) {
        this.allowingManNameAccessGroup.set(allowingManNameAccessGroup);
    }

    public StringProperty allowingManNameAccessGroupProperty() {
        return allowingManNameAccessGroup;
    }

    public int getForemanId() {
        return foremanId.get();
    }

    public void setForemanId(int foremanId) {
        this.foremanId.set(foremanId);
    }

    public IntegerProperty foremanIdProperty() {
        return foremanId;
    }

    public String getForemanNameAccessGroup() {
        return foremanNameAccessGroup.get();
    }

    public void setForemanNameAccessGroup(String foremanNameAccessGroup) {
        this.foremanNameAccessGroup.set(foremanNameAccessGroup);
    }

    public StringProperty foremanNameAccessGroupProperty() {
        return foremanNameAccessGroup;
    }



    public String getInstructTo() {
        return instructTo.get();
    }

    public void setInstructTo(String instructTo) {
        this.instructTo.set(instructTo);
    }

    public StringProperty instructToProperty() {
        return instructTo;
    }

    public ObservableList<OrderDocPerson> getCrewMember() {
        return crewMember.get();
    }

    public void setCrewMember(ObservableList<OrderDocPerson> crewMember) {
        this.crewMember.set(crewMember);
    }

    public ListProperty<OrderDocPerson> crewMemberProperty() {
        return crewMember;
    }

    @Override
    public String toString() {
        return "Наряд-допуск № " + getNumber();
    }

    @Override
    public String getDialogStageTitle() {
        return "Наряд-допуска";
    }
}
