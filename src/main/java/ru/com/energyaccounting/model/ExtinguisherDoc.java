package ru.com.energyaccounting.model;

public class ExtinguisherDoc extends Docs {

    @Override
    public String getDialogStageTitle() {
        return "Огнетушителей";
    }

    @Override
    public String toString() {
        return "Документ Огнетушители №" + getId() + " от " + getDocDateTime().toLocalDate() +" "+ getDocDateTime().toLocalTime();
    }
}
