package ru.com.energyaccounting.model;

/**
 * Created by UserSts on 22.05.2017.
 */
public class IPMType extends Root  {

    @Override
    public String toString() {
        return "Тип СИЗ: " + getName();
    }

    @Override
    public String getDialogStageTitle() {
        return "Типа СИЗ";
    }
}
