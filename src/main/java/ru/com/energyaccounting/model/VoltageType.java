package ru.com.energyaccounting.model;

/**
 * Created by UserSts on 30.05.2017.
 */
public class VoltageType extends Root {
    @Override
    public String toString() {
        return "Класс напряжения; " + getName() ;
    }

    @Override
    public String getDialogStageTitle() {
        return "Типа напряжения";
    }
}
