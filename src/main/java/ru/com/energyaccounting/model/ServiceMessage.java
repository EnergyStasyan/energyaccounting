package ru.com.energyaccounting.model;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.time.LocalDateTime;

/**
 * Created by UserSts on 22.03.2017.
 */
public class ServiceMessage extends Root {

    private final SimpleObjectProperty<LocalDateTime> eventDateTime;
    private final StringProperty messageType;


    public ServiceMessage() {
        this(null);
    }

    public ServiceMessage(String description){
        this.eventDateTime = new SimpleObjectProperty<LocalDateTime>();
        this.messageType = new SimpleStringProperty();
    }


    public LocalDateTime getEventDateTime() {
        return eventDateTime.get();
    }

    public void setEventDateTime(LocalDateTime eventDateTime) {
        this.eventDateTime.set(eventDateTime);
    }

    public SimpleObjectProperty<LocalDateTime> eventDateTimeProperty() {
        return eventDateTime;
    }

    public String getMessageType() {
        return messageType.get();
    }

    public void setMessageType(String messageType) {
        this.messageType.set(messageType);
    }

    public StringProperty messageTypeProperty() {
        return messageType;
    }

    @Override
    public String getDialogStageTitle() {
        return "";
    }
}
