package ru.com.energyaccounting.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Created by UserSts on 19.05.2017.
 */
public class Substation extends Root {

    private final StringProperty organization;
    private final IntegerProperty org_id;

    public Substation() {
        this.organization = new SimpleStringProperty();
        this.org_id = new SimpleIntegerProperty();
    }

    public String getOrganization() {
        return organization.get();
    }

    public void setOrganization(String organization) {
        this.organization.set(organization);
    }

    public StringProperty organizationProperty() {
        return organization;
    }

    public int getOrg_id() {
        return org_id.get();
    }

    public void setOrg_id(int org_id) {
        this.org_id.set(org_id);
    }

    public IntegerProperty org_idProperty() {
        return org_id;
    }

    @Override
    public String toString() {
        return "Подстанция: " + getName();
    }

    @Override
    public String getDialogStageTitle() {
        return "Подстанции";
    }
}
