package ru.com.energyaccounting.model;

import javafx.beans.property.*;
import ru.com.energyaccounting.dao.Identifier;

import java.time.LocalDate;

/**
 * Created by UserSts on 20.05.2017.
 */
public abstract class Root implements Identifier<Integer> {
    private final IntegerProperty id;
    private final StringProperty name;
    private final StringProperty description;
    private final StringProperty inventoryNumber;
    private final SimpleObjectProperty<LocalDate> nextTestDate;
    private final SimpleObjectProperty<LocalDate> lastTestDate;

    public Root() {
        this.id = new SimpleIntegerProperty();
        this.name = new SimpleStringProperty();
        this.inventoryNumber = new SimpleStringProperty();
        this.description = new SimpleStringProperty();
        this.nextTestDate = new SimpleObjectProperty<>();
        this.lastTestDate = new SimpleObjectProperty<>();
    }

    @Override
    public Integer getId() {
        return id.get();
    }

    public IntegerProperty idProperty() {
        return id;
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getDescription() {
        return description.get();
    }

    public StringProperty descriptionProperty() {
        return description;
    }

    public void setDescription(String description) {
        this.description.set(description);
    }

    public LocalDate getNextTestDate() {
        return nextTestDate.get();
    }

    public SimpleObjectProperty<LocalDate> nextTestDateProperty() {
        return nextTestDate;
    }

    public void setNextTestDate(LocalDate nextTestDate) {
        this.nextTestDate.set(nextTestDate);
    }

    public LocalDate getLastTestDate() {
        return lastTestDate.get();
    }

    public SimpleObjectProperty<LocalDate> lastTestDateProperty() {
        return lastTestDate;
    }

    public void setLastTestDate(LocalDate lastTestDate) {
        this.lastTestDate.set(lastTestDate);
    }

    public String getInventoryNumber() {
        return inventoryNumber.get();
    }

    public StringProperty inventoryNumberProperty() {
        return inventoryNumber;
    }

    public void setInventoryNumber(String inventoryNumber) {
        this.inventoryNumber.set(inventoryNumber);
    }
}
