package ru.com.energyaccounting.model;

/**
 * Created by UserSts on 26.06.2017.
 */
public class Position extends Root {
    @Override
    public String toString() {
        return getName();
    }

    @Override
    public String getDialogStageTitle() {
        return "Должности";
    }
}
