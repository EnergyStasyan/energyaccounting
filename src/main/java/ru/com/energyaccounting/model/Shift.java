package ru.com.energyaccounting.model;

/**
 * Created by UserSts on 30.05.2017.
 */
public class Shift extends Root {
    @Override
    public String toString() {
        return "Смена: " + getName();
    }

    @Override
    public String getDialogStageTitle() {
        return "Смены";
    }
}
