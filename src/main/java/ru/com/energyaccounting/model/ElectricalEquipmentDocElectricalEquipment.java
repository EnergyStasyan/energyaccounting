package ru.com.energyaccounting.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

public class ElectricalEquipmentDocElectricalEquipment extends ElectricalEquipment {
    private final IntegerProperty doc_id;
    private final IntegerProperty electricalEquipment_id;

    public ElectricalEquipmentDocElectricalEquipment() {
        super();
        this.doc_id = new SimpleIntegerProperty();
        this.electricalEquipment_id = new SimpleIntegerProperty();
    }

    public int getDoc_id() {
        return doc_id.get();
    }

    public IntegerProperty doc_idProperty() {
        return doc_id;
    }

    public void setDoc_id(int doc_id) {
        this.doc_id.set(doc_id);
    }

    public int getElectricalEquipment_id() {
        return electricalEquipment_id.get();
    }

    public IntegerProperty electricalEquipment_idProperty() {
        return electricalEquipment_id;
    }

    public void setElectricalEquipment_id(int electricalEquipment_id) {
        this.electricalEquipment_id.set(electricalEquipment_id);
    }
}
