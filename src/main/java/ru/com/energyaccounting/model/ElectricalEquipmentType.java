package ru.com.energyaccounting.model;

/**
 * Тип Электрооборудования. Пример: трансформатор
 * Created by UserSts on 13.01.2021.
 */
public class ElectricalEquipmentType extends Root{

    @Override
    public String toString() {
        return "Тип эл.оборудования: " + getName();
    }

    @Override
    public String getDialogStageTitle() {
        return "Типа эл.оборудования";
    }
}
