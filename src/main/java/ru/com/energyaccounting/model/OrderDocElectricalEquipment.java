package ru.com.energyaccounting.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class OrderDocElectricalEquipment extends Root {

    private final IntegerProperty orderDocId;

    private final StringProperty electricalEquipment;
    private final IntegerProperty electricalEquipmentId;
    private final StringProperty additional;



    public OrderDocElectricalEquipment() {
        this.orderDocId =  new SimpleIntegerProperty();


        this.electricalEquipmentId = new SimpleIntegerProperty();
        this.electricalEquipment = new SimpleStringProperty();

        this.additional = new SimpleStringProperty();

    }

    public int getOrderDocId() {
        return orderDocId.get();
    }

    public void setOrderDocId(int orderDocId) {
        this.orderDocId.set(orderDocId);
    }

    public IntegerProperty orderDocIdProperty() {
        return orderDocId;
    }

    public String getElectricalEquipment() {
        return electricalEquipment.get();
    }

    public void setElectricalEquipment(String electricalEquipment) {
        this.electricalEquipment.set(electricalEquipment);
    }

    public StringProperty electricalEquipmentProperty() {
        return electricalEquipment;
    }

    public int getElectricalEquipmentId() {
        return electricalEquipmentId.get();
    }

    public void setElectricalEquipmentId(int electricalEquipmentId) {
        this.electricalEquipmentId.set(electricalEquipmentId);
    }

    public IntegerProperty electricalEquipmentIdProperty() {
        return electricalEquipmentId;
    }

    public String getAdditional() {
        return additional.get();
    }

    public void setAdditional(String additional) {
        this.additional.set(additional);
    }

    public StringProperty additionalProperty() {
        return additional;
    }

    @Override
    public String getDialogStageTitle() {
        return null;
    }

}
