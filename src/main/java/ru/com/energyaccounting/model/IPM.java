package ru.com.energyaccounting.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Created by UserSts on 21.05.2017.
 */
public class IPM extends Root{

    private final StringProperty impType;
    private final IntegerProperty imp_type_id;
    private final StringProperty ElectricalInstallation;
    private final IntegerProperty el_install_id;
    private final StringProperty voltageType;
    private final IntegerProperty vt_id;
    private final StringProperty status;


    public IPM() {
        this.impType = new SimpleStringProperty();
        this.imp_type_id = new SimpleIntegerProperty();
        this.ElectricalInstallation = new SimpleStringProperty();
        this.el_install_id = new SimpleIntegerProperty();
        this.voltageType = new SimpleStringProperty();
        this.vt_id = new SimpleIntegerProperty();
        this.status = new SimpleStringProperty();
    }

    public String getImpType() {
        return impType.get();
    }

    public void setImpType(String impType) {
        this.impType.set(impType);
    }

    public StringProperty impTypeProperty() {
        return impType;
    }

    public int getImp_type_id() {
        return imp_type_id.get();
    }

    public void setImp_type_id(int imp_type_id) {
        this.imp_type_id.set(imp_type_id);
    }

    public IntegerProperty imp_type_idProperty() {
        return imp_type_id;
    }

    public String getElectricalInstallation() {
        return ElectricalInstallation.get();
    }

    public void setElectricalInstallation(String electricalInstallation) {
        this.ElectricalInstallation.set(electricalInstallation);
    }

    public StringProperty electricalInstallationProperty() {
        return ElectricalInstallation;
    }

    public int getEl_install_id() {
        return el_install_id.get();
    }

    public void setEl_install_id(int el_install_id) {
        this.el_install_id.set(el_install_id);
    }

    public IntegerProperty el_install_idProperty() {
        return el_install_id;
    }

    public String getVoltageType() {
        return voltageType.get();
    }

    public void setVoltageType(String voltageType) {
        this.voltageType.set(voltageType);
    }

    public StringProperty voltageTypeProperty() {
        return voltageType;
    }

    public int getVt_id() {
        return vt_id.get();
    }

    public void setVt_id(int vt_id) {
        this.vt_id.set(vt_id);
    }

    public IntegerProperty vt_idProperty() {
        return vt_id;
    }

    public String getStatus() {
        return status.get();
    }

    public void setStatus(String status) {
        this.status.set(status);
    }

    public StringProperty statusProperty() {
        return status;
    }

    @Override
    public String toString() {
        return "СИЗ: " + getImpType() + ", " + getVoltageType()  + ", " + getElectricalInstallation() ;
    }

    @Override
    public String getDialogStageTitle() {
        return "СИЗ";
    }
}
