package ru.com.energyaccounting.model;

/**
 * Created by UserSts on 19.05.2017.
 */
public enum  EnumDictionary {
    SUBDIVISION
    , ORGANIZATION
    , SUBSTATION
    , VOLTAGE_TYPE
    , ELECTRICAL_INSTALLATION
    , ELECTRICAL_EQUIPMENT
    , PERSON_FULL
    , PERSON_SHORT
    ,IPM
    ,IPM_TYPE
    , EXTINGUISHERS
    , EXTINGUISHER_TYPE
    , POSITION
}
