package ru.com.energyaccounting.model;

import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.ObservableList;

public class ElectricalEquipmentDoc extends Docs {

    private final ListProperty<ElectricalEquipmentDocElectricalEquipment> electricalEquipmentDocList;

    public ElectricalEquipmentDoc() {
        this.electricalEquipmentDocList = new SimpleListProperty<>();
    }

    public ObservableList<ElectricalEquipmentDocElectricalEquipment> getElectricalEquipmentDocList() {
        return electricalEquipmentDocList.get();
    }

    public ListProperty<ElectricalEquipmentDocElectricalEquipment> electricalEquipmentDocListProperty() {
        return electricalEquipmentDocList;
    }

    public void setElectricalEquipmentDocList(ObservableList<ElectricalEquipmentDocElectricalEquipment> electricalEquipmentDocList) {
        this.electricalEquipmentDocList.set(electricalEquipmentDocList);
    }


    @Override
    public String getDialogStageTitle() {
        return "Электрооборудования";
    }

    @Override
    public String toString() {
        return "Электрооборудования: №" + getId() + " от " + getDocDateTime();
    }
}
