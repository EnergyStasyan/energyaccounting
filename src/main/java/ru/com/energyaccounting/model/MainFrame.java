package ru.com.energyaccounting.model;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import ru.com.energyaccounting.dao.Identifier;

import java.time.LocalDate;

/**
 * Created by UserSts on 01.06.2017.
 */
public class MainFrame implements Identifier<Integer> {
    //persons
    private final StringProperty nameSurname;
    private final ObjectProperty<LocalDate> birthday;

    //IPMs
    private final StringProperty eiIPM;
    private final StringProperty IPMType;
    private final ObjectProperty<LocalDate> nextTestDateIPM;

    //extinguishers
    private final StringProperty eiExt;
    private final StringProperty extType;
    private final ObjectProperty<LocalDate> nextTestDateExt;

    //main info
    private final ObjectProperty<LocalDate> date;
    private final StringProperty type;
    private final StringProperty description;



    public MainFrame() {
        this.nameSurname = new SimpleStringProperty();
        this.birthday = new SimpleObjectProperty<>();

        this.eiIPM = new SimpleStringProperty();
        this.IPMType = new SimpleStringProperty();
        this.nextTestDateIPM = new SimpleObjectProperty<>();

        this.eiExt = new SimpleStringProperty();
        this.extType = new SimpleStringProperty();
        this.nextTestDateExt = new SimpleObjectProperty<>();

        this.date = new SimpleObjectProperty<>();
        this.type = new SimpleStringProperty();
        this.description = new SimpleStringProperty();
    }

    @Override
    public Integer getId() {
        return null;
    }

    public String getNameSurname() {
        return nameSurname.get();
    }

    public void setNameSurname(String nameSurname) {
        this.nameSurname.set(nameSurname);
    }

    public StringProperty nameSurnameProperty() {
        return nameSurname;
    }

    public LocalDate getBirthday() {
        return birthday.get();
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday.set(birthday);
    }

    public ObjectProperty<LocalDate> birthdayProperty() {
        return birthday;
    }

    public String getEiIPM() {
        return eiIPM.get();
    }

    public void setEiIPM(String eiIPM) {
        this.eiIPM.set(eiIPM);
    }

    public StringProperty eiIPMProperty() {
        return eiIPM;
    }

    public String getIPMType() {
        return IPMType.get();
    }

    public void setIPMType(String IPMType) {
        this.IPMType.set(IPMType);
    }

    public StringProperty IPMTypeProperty() {
        return IPMType;
    }

    public LocalDate getNextTestDateIPM() {
        return nextTestDateIPM.get();
    }

    public void setNextTestDateIPM(LocalDate nextTestDateIPM) {
        this.nextTestDateIPM.set(nextTestDateIPM);
    }

    public ObjectProperty<LocalDate> nextTestDateIPMProperty() {
        return nextTestDateIPM;
    }

    public String getEiExt() {
        return eiExt.get();
    }

    public void setEiExt(String eiExt) {
        this.eiExt.set(eiExt);
    }

    public StringProperty eiExtProperty() {
        return eiExt;
    }

    public String getExtType() {
        return extType.get();
    }

    public void setExtType(String extType) {
        this.extType.set(extType);
    }

    public StringProperty extTypeProperty() {
        return extType;
    }

    public LocalDate getNextTestDateExt() {
        return nextTestDateExt.get();
    }

    public void setNextTestDateExt(LocalDate nextTestDateExt) {
        this.nextTestDateExt.set(nextTestDateExt);
    }

    public ObjectProperty<LocalDate> nextTestDateExtProperty() {
        return nextTestDateExt;
    }

    public LocalDate getDate() {
        return date.get();
    }

    public void setDate(LocalDate date) {
        this.date.set(date);
    }

    public ObjectProperty<LocalDate> dateProperty() {
        return date;
    }

    public String getType() {
        return type.get();
    }

    public void setType(String type) {
        this.type.set(type);
    }

    public StringProperty typeProperty() {
        return type;
    }

    public String getDescription() {
        return description.get();
    }

    public void setDescription(String description) {
        this.description.set(description);
    }

    public StringProperty descriptionProperty() {
        return description;
    }

    @Override
    public String getDialogStageTitle() {
        return "";
    }
}
