package ru.com.energyaccounting.model;

import ru.com.energyaccounting.dao.DaoFactory;
import ru.com.energyaccounting.dao.DaoFactoryInterface;
import ru.com.energyaccounting.dao.GenericDao;
import ru.com.energyaccounting.dao.PersistException;
import ru.com.energyaccounting.util.DBUtilAuditStatic;
import ru.com.energyaccounting.util.DBUtilStatic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by UserSts on 23.03.2017.
 */
public class ServiceMessageDAOStatic {
    private DaoFactoryInterface<Connection> factory;
    private Connection connection;
    private GenericDao<ServiceMessage, Integer> dao;

    public ServiceMessageDAOStatic() {
        try {
            factory = new DaoFactory();
            connection = factory.getContext();
            dao = factory.getDao(connection, ServiceMessage.class);

        } catch (PersistException e) {
            e.printStackTrace();
        }
    }

    public static void insertServiceMessage(String messageType, String description) {

        String sqlQueryInsert = "INSERT INTO service_messages (message_type, description) VALUES (?,?)";

        try (Connection conn = DBUtilAuditStatic.dbConnect(true)) {
            if (conn != null) {
                PreparedStatement ps = conn.prepareStatement(sqlQueryInsert);
                ps.setString(1, messageType);
                ps.setString(2, description);
                ps.execute();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
