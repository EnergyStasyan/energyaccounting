package ru.com.energyaccounting.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Электрооборудование. Пример: ячейка 8 "ТП-4, 2"
 * Created by UserSts on 15.03.2017.
 */
public class ElectricalEquipment extends ElectricalInstallation{

    private final StringProperty electricalInstallation;
    private final IntegerProperty el_install_id;
    private final StringProperty electricalEquipmentType;
    private final IntegerProperty electrical_equipment_type_id;
    private final StringProperty status;


    public ElectricalEquipment() {
        this.electricalInstallation = new SimpleStringProperty();
        this.el_install_id = new SimpleIntegerProperty();
        this.electricalEquipmentType = new SimpleStringProperty();
        this.electrical_equipment_type_id = new SimpleIntegerProperty();
        this.status = new SimpleStringProperty();
    }

    public String getElectricalInstallation() {
        return electricalInstallation.get();
    }

    public void setElectricalInstallation(String electricalInstallation) {
        this.electricalInstallation.set(electricalInstallation);
    }

    public StringProperty electricalInstallationProperty() {
        return electricalInstallation;
    }

    public int getEl_install_id() {
        return el_install_id.get();
    }

    public void setEl_install_id(int el_install_id) {
        this.el_install_id.set(el_install_id);
    }

    public IntegerProperty el_install_idProperty() {
        return el_install_id;
    }

    public String getElectricalEquipmentType() {
        return electricalEquipmentType.get();
    }

    public StringProperty electricalEquipmentTypeProperty() {
        return electricalEquipmentType;
    }

    public void setElectricalEquipmentType(String electricalEquipmentType) {
        this.electricalEquipmentType.set(electricalEquipmentType);
    }

    public int getElectrical_equipment_type_id() {
        return electrical_equipment_type_id.get();
    }

    public IntegerProperty electrical_equipment_type_idProperty() {
        return electrical_equipment_type_id;
    }

    public void setElectrical_equipment_type_id(int electrical_equipment_type_id) {
        this.electrical_equipment_type_id.set(electrical_equipment_type_id);
    }

    public String getStatus() {
        return status.get();
    }

    public StringProperty statusProperty() {
        return status;
    }

    public void setStatus(String status) {
        this.status.set(status);
    }

    @Override
    public String toString() {
        return getName();
    }

    @Override
    public String getDialogStageTitle() {
        return "Эл.оборудования";
    }
}
