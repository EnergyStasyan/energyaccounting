package ru.com.energyaccounting.model;

import javafx.beans.property.*;

import java.time.LocalDate;

/**
 * Created by UserSts on 05.02.2017.
 */
public class Person extends Root{
    private final IntegerProperty personId;
    private final StringProperty surname;
    private final StringProperty name;
    private final StringProperty middleName;

    private final ObjectProperty<LocalDate> birthdayDate;
    private final ObjectProperty<LocalDate> hireDate;
    private final ObjectProperty<LocalDate> dismissalDate;

    private final StringProperty accessGroup;
    private final StringProperty phone;
    private final StringProperty subdivision;
    private final IntegerProperty subdiv_id;
    private final StringProperty position;
    private final IntegerProperty position_id;

    private final FloatProperty salary;



    public Person() {
        this(null, null);
    }

    public Person(String name, String surname) {

        this.personId = new SimpleIntegerProperty();

        this.surname = new SimpleStringProperty();
        this.middleName = new SimpleStringProperty();
        this.name = new SimpleStringProperty();

        this.birthdayDate = new SimpleObjectProperty<>();
        this.hireDate = new SimpleObjectProperty<>();
        this.dismissalDate = new SimpleObjectProperty<>();

        this.accessGroup = new SimpleStringProperty();


        this.phone = new SimpleStringProperty();
        this.subdivision = new SimpleStringProperty();
        this.subdiv_id = new SimpleIntegerProperty();
        this.position = new SimpleStringProperty();
        this.position_id = new SimpleIntegerProperty();

        this.salary = new SimpleFloatProperty();

    }


    public int getPersonId() {
        return personId.get();
    }

    public void setPersonId(int personId) {
        this.personId.set(personId);
    }

    public IntegerProperty personIdProperty() {
        return personId;
    }

    public float getSalary() {
        return salary.get();
    }

    public void setSalary(float salary) {
        this.salary.set(salary);
    }

    public FloatProperty salaryProperty() {
        return salary;
    }

    public String getSurname() {
        return surname.get();
    }

    public void setSurname(String surname) {
        this.surname.set(surname);
    }

    public StringProperty surnameProperty() {
        return surname;
    }

    public String getName() {
        return name.get();
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public StringProperty nameProperty() {
        return name;
    }

    public String getMiddleName() {
        return middleName.get();
    }

    public void setMiddleName(String middleName) {
        this.middleName.set(middleName);
    }

    public StringProperty middleNameProperty() {
        return middleName;
    }

    public LocalDate getBirthdayDate() {

        return birthdayDate.get();
    }

    public void setBirthdayDate(LocalDate birthdayDate) {
        this.birthdayDate.set(birthdayDate);
    }

    public ObjectProperty<LocalDate> birthdayDateProperty() {

        return birthdayDate;
    }

    public LocalDate getHireDate() {
        return hireDate.get();
    }

    public void setHireDate(LocalDate hireDate) {
        this.hireDate.set(hireDate);
    }

    public ObjectProperty<LocalDate> hireDateProperty() {
        return hireDate;
    }

    public LocalDate getDismissalDate() {
        return dismissalDate.get();
    }

    public void setDismissalDate(LocalDate dismissalDate) {
        this.dismissalDate.set(dismissalDate);
    }

    public ObjectProperty<LocalDate> dismissalDateProperty() {
        return dismissalDate;
    }

    public String getAccessGroup() {
        return accessGroup.get();
    }

    public void setAccessGroup(String accessGroup) {
        this.accessGroup.set(accessGroup);
    }

    public StringProperty accessGroupProperty() {
        return accessGroup;
    }

    public String getPhone() {
        return phone.get();
    }

    public void setPhone(String phone) {
        this.phone.set(phone);
    }

    public StringProperty phoneProperty() {
        return phone;
    }

    public String getSubdivision() {
        return subdivision.get();
    }

    public void setSubdivision(String subdivision) {
        this.subdivision.set(subdivision);
    }

    public StringProperty subdivisionProperty() {
        return subdivision;
    }

    public int getSubdiv_id() {
        return subdiv_id.get();
    }

    public void setSubdiv_id(int subdiv_id) {
        this.subdiv_id.set(subdiv_id);
    }

    public IntegerProperty subdiv_idProperty() {
        return subdiv_id;
    }

    public String getPosition() {
        return position.get();
    }

    public void setPosition(String position) {
        this.position.set(position);
    }

    public StringProperty positionProperty() {
        return position;
    }

    public int getPosition_id() {
        return position_id.get();
    }

    public void setPosition_id(int position_id) {
        this.position_id.set(position_id);
    }

    public IntegerProperty position_idProperty() {
        return position_id;
    }

    /**
     * Для конкантенации применен StringBuffer
     * возвращает  Фамилия И.О.
     * Если отчества нет, то возвращает Фамилия И.
     * используется тенарное ИЛИ
     * @return sb.toString()
     */
    public String surnameAndNM() {
        StringBuffer sb = new StringBuffer();
        sb
                .append(getSurname()).append(" ")
                .append(getName().charAt(0))
                .append(".")
                .append(((getMiddleName() != null) ? getMiddleName().charAt(0) + "." : ""));

        return sb.toString();
    }

    @Override
    public String toString() {
        return "Сотрудник:" +
                "\nТабельный номер: " + getPersonId() +
                ", Фамилия: " + getSurname() +
                ", Имя: " + getName();
    }

    @Override
    public String getDialogStageTitle() {
        return "Сотрудника";
    }
}

