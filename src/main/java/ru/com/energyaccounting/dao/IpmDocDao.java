package ru.com.energyaccounting.dao;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import ru.com.energyaccounting.model.IpmDoc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;


public class IpmDocDao extends AbstractJDBCDao<IpmDoc, Integer> {
    public IpmDocDao(Connection connection) {
        super(connection);
    }

    @Override
    public String getSelectQuery() {
        return "SELECT * FROM ipm_docs";
    }

    @Override
    public String getCreateQuery() {
        return "INSERT INTO ipm_docs (ipm_date_time, description) VALUES (?, ?)";
    }

    @Override
    public String getUpdateQuery() {
        return "UPDATE ipm_docs SET ipm_date_time = ?, description = ? WHERE id = ?";
    }

    @Override
    public String getDeleteQuery() {
        return "DELETE FROM ipm_docs WHERE id = ?;";
    }

    @Override
    protected ObservableList<IpmDoc> parseResultSet(ResultSet rs) throws PersistException {
        ObservableList<IpmDoc> result = FXCollections.observableArrayList();
        try {
            while (rs.next()){
                IpmDoc ipmDoc = new IpmDoc();
                ipmDoc.setId(rs.getInt("id"));
                try {
                    ipmDoc.setDocDateTime(LocalDateTime.parse(rs.getString("ipm_date_time"), dtfDateTime));

                }catch (DateTimeParseException e){
                    ipmDoc.setDocDateTime(LocalDateTime.of(01,01,1,00,00));
                }
                ipmDoc.setDescription(rs.getString("description"));

                result.add(ipmDoc);
            }
        }catch (SQLException e){
            e.printStackTrace();
            throw new PersistException(e);
        }

        return result;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement ps, IpmDoc ipmDoc) throws PersistException {
        try {
            if (ipmDoc.getDocDateTime() != null) {
                String dateTime = ipmDoc.getDocDateTime().format(dtfDateTime);
                ps.setString(1, dateTime);
            }
            ps.setString(2, ipmDoc.getDescription());



        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement ps, IpmDoc ipmDoc) throws PersistException {
        try {
            if (ipmDoc.getDocDateTime() != null) {
                String dateTime = ipmDoc.getDocDateTime().format(dtfDateTime);
                ps.setString(1, dateTime);
            }
            ps.setString(2, ipmDoc.getDescription());
            ps.setInt(3, ipmDoc.getId());

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void prepareStatementForDelete(PreparedStatement ps, IpmDoc ipmDoc) throws PersistException {

    }
}
