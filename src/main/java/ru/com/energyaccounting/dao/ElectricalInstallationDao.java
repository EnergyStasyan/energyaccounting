package ru.com.energyaccounting.dao;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import ru.com.energyaccounting.model.ElectricalInstallation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by UserSts on 11.05.2017.
 */
public class ElectricalInstallationDao extends AbstractJDBCDao<ElectricalInstallation, Integer> {

    public ElectricalInstallationDao(Connection connection) {
        super(connection);
    }

    @Override
    public ElectricalInstallation create() throws PersistException {
        ElectricalInstallation e = new ElectricalInstallation();
        return persist(e);
    }

    @Override
    public String getSelectQuery() {
        return "SELECT * FROM electrical_installations_view;";
    }

    @Override
    public String getCreateQuery() {
        return "INSERT INTO electrical_installations (name, vt_id, substat_id, description) VALUES (?,?,?,?)";
    }

    @Override
    public String getUpdateQuery() {
        return "UPDATE electrical_installations SET name = ?, vt_id = ?, substat_id = ?, description = ? WHERE id = ?";
    }

    @Override
    public String getDeleteQuery() {
        return "DELETE FROM electrical_installations WHERE id = ?";
    }

    @Override
    protected ObservableList<ElectricalInstallation> parseResultSet(ResultSet rs) throws PersistException {
        ObservableList<ElectricalInstallation> result = FXCollections.observableArrayList();

        try {
            while (rs.next()){
                ElectricalInstallation e = new ElectricalInstallation();
                e.setId(rs.getInt("id"));
                e.setName(rs.getString("name"));
                e.setVoltageType(rs.getString("voltageType"));
                e.setVt_id(rs.getInt("vt_id"));
                e.setSubstation(rs.getString("substation"));
                e.setSubstat_id(rs.getInt("substat_id"));
                e.setDescription(rs.getString("description"));
                result.add(e);
            }
        }catch (SQLException e){
            throw new PersistException(e);
        }

        return result;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement ps, ElectricalInstallation object) throws PersistException {
        try {
            ps.setString(1, object.getName());
            ps.setInt(2, object.getVt_id());
            ps.setInt(3, object.getSubstat_id());
            ps.setString(4, object.getDescription());
        }catch (SQLException e){
            throw new PersistException(e);
        }
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement ps, ElectricalInstallation object) throws PersistException {
        try {
            ps.setString(1, object.getName());
            ps.setInt(2, object.getVt_id());
            ps.setInt(3, object.getSubstat_id());
            ps.setString(4, object.getDescription());
            ps.setInt(5, object.getId());
        }catch (SQLException e){
            throw new PersistException(e);
        }
    }

    @Override
    protected void prepareStatementForDelete(PreparedStatement ps, ElectricalInstallation object) throws PersistException {

    }
}
