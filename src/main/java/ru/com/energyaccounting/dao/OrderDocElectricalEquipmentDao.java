package ru.com.energyaccounting.dao;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import ru.com.energyaccounting.model.OrderDocElectricalEquipment;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class OrderDocElectricalEquipmentDao extends AbstractJDBCDao<OrderDocElectricalEquipment, Integer> {
    public OrderDocElectricalEquipmentDao(Connection connection) {
        super(connection);
    }

    @Override
    public String getSelectQuery() {
        return "SELECT * FROM order_doc_electrical_equipment_view";
    }

    @Override
    public String getCreateQuery() {
        return null;
    }

    @Override
    public String getUpdateQuery() {
        return null;
    }

    @Override
    public String getDeleteQuery() {
        return null;
    }

    @Override
    protected ObservableList<OrderDocElectricalEquipment> parseResultSet(ResultSet rs) throws PersistException {
        ObservableList<OrderDocElectricalEquipment> result = FXCollections.observableArrayList();
        try {
            while (rs.next()){
                OrderDocElectricalEquipment oo = new OrderDocElectricalEquipment();

                oo.setOrderDocId(rs.getInt("id"));
                oo.setElectricalEquipment(rs.getString("electrical_equipment"));
                oo.setAdditional(rs.getString("additional"));
                result.add(oo);
            }
        }catch (SQLException e){
            e.getStackTrace();
            throw new PersistException(e);
        }

        return result;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement ps, OrderDocElectricalEquipment object) throws PersistException {

    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement ps, OrderDocElectricalEquipment object) throws PersistException {

    }

    @Override
    protected void prepareStatementForDelete(PreparedStatement ps, OrderDocElectricalEquipment object) throws PersistException {

    }
}
