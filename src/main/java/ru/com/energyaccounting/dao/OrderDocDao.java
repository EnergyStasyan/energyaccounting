package ru.com.energyaccounting.dao;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import ru.com.energyaccounting.model.OrderDoc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.DateTimeException;
import java.time.LocalDateTime;

/**
 * Created by UserSts on 26.06.2017.
 */
public class OrderDocDao extends AbstractJDBCDao<OrderDoc,Integer> {
    public OrderDocDao(Connection connection) {
        super(connection);
    }

    @Override
    public String getSelectQuery() {
        return "SELECT * FROM order_docs_view";
    }

    @Override
    public String getCreateQuery() {
        return "INSERT INTO order_docs (number, subdiv_id, substat_id, date_time_of_issue, date_time_start, date_time_end, instruct_to, description ) VALUES (?,?,?,?,?,?,?,?);"; //8
    }

    @Override
    public String getUpdateQuery() {
        return "UPDATE order_docs SET number = ?, subdiv_id = ?, substat_id = ?, date_time_of_issue = ?, date_time_start = ?, date_time_end = ?, instruct_to = ?, description = ? WHERE id = ?;";
    }

    @Override
    public String getDeleteQuery() {
        return "DELETE FROM order_docs WHERE id = ?;";
    }

    @Override
    protected ObservableList<OrderDoc> parseResultSet(ResultSet rs) throws PersistException {
        ObservableList<OrderDoc> result = FXCollections.observableArrayList();
        try {
            while (rs.next()){
                OrderDoc o = new OrderDoc();
                o.setId(rs.getInt("id"));
                o.setNumber(rs.getString("number"));
                o.setInstructTo(rs.getString("instruct_to"));
                o.setDescription(rs.getString("description"));
                //description
                try {
                    o.setDateTimeOfIssue(LocalDateTime.parse(rs.getString("date_time_of_issue"), dtfDateTime));
                    o.setDateOfIssue(o.getDateTimeOfIssue().toLocalDate());
                    o.setTimeOfIssue(o.getDateTimeOfIssue().toLocalTime());
                }catch (NullPointerException | DateTimeException e){
                    o.setDateTimeOfIssue(null);
                    log.warning("Ошибка парсинга даты и времени выдачи наряда: " + e);
                }
                try {
                    o.setDateTimeStart(LocalDateTime.parse(rs.getString("date_time_start"), dtfDateTime));
                    o.setDateStart(o.getDateTimeStart().toLocalDate());
                    o.setTimeStart(o.getDateTimeStart().toLocalTime());
                    // TODO: 22.07.2017 сделать разбивку на время и дату
                }catch (NullPointerException | DateTimeException e){
                    o.setDateTimeStart(null);
                    log.warning("Ошибка парсинга даты и времени окончания работ наряда: " + e);
                }
                try {
                    o.setDateTimeEnd(LocalDateTime.parse(rs.getString("date_time_end"), dtfDateTime));
                    o.setDateEnd(o.getDateTimeEnd().toLocalDate());
                    o.setTimeEnd(o.getDateTimeEnd().toLocalTime());
                    // TODO: 22.07.2017 сделать разбивку на время и дату
                }catch (NullPointerException | DateTimeException e){
                    o.setDateTimeEnd(null);
                    log.warning("Ошибка парсинга даты и времени начала работ наряда: " + e);
                }

                o.setOrganization(rs.getString("organization"));
                o.setSubdivision(rs.getString("subdivision"));
                o.setSubdivId(rs.getInt("subdiv_id"));

                o.setSubstation(rs.getString("substation"));
                o.setSubstatId(rs.getInt("substat_id"));

                result.add(o);
            }
        }catch (SQLException e){
            e.printStackTrace();
            throw new PersistException(e);
        }

        return result;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement ps, OrderDoc orderDoc) throws PersistException {
        try {
            ps.setString(1, orderDoc.getNumber());
            ps.setInt(2, orderDoc.getSubdivId());
            ps.setInt(3, orderDoc.getSubstatId());
            if (orderDoc.getDateOfIssue() != null){
                String dateTimeTmp = orderDoc.getDateOfIssue() + " " + orderDoc.getTimeOfIssue() + ":00";
                ps.setString(4, dateTimeTmp);
            }
            if (orderDoc.getDateStart() != null){
                String dateTimeTmp = orderDoc.getDateStart() + " " + orderDoc.getTimeStart()+ ":00";
                ps.setString(5, dateTimeTmp);
            }
            if (orderDoc.getDateEnd() != null){
                String dateTimeTmp = orderDoc.getDateEnd() + " " + orderDoc.getTimeEnd()+ ":00";
                ps.setString(6, dateTimeTmp);
            }
            ps.setString(7, orderDoc.getInstructTo());
            ps.setString(8, orderDoc.getDescription());

        }catch (SQLException e){
            throw new PersistException(e);
        }
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement ps, OrderDoc orderDoc) throws PersistException {
        try {
            ps.setString(1, orderDoc.getNumber());
            ps.setInt(2, orderDoc.getSubdivId());
            ps.setInt(3, orderDoc.getSubstatId());
            if (orderDoc.getDateOfIssue() != null){
                String dateTimeTmp = orderDoc.getDateOfIssue() + " " + orderDoc.getTimeOfIssue() + ":00";
                ps.setString(4, dateTimeTmp);
            }
            if (orderDoc.getDateStart() != null){
                String dateTimeTmp = orderDoc.getDateStart() + " " + orderDoc.getTimeStart() + ":00";
                ps.setString(5, dateTimeTmp);
            }
            if (orderDoc.getDateEnd() != null){
                String dateTimeTmp = orderDoc.getDateEnd() + " " + orderDoc.getTimeEnd() + ":00";
                ps.setString(6, dateTimeTmp);
            }
            ps.setString(7, orderDoc.getInstructTo());
            ps.setString(8, orderDoc.getDescription());
            ps.setInt(9, orderDoc.getId());
        }catch (SQLException e){
            throw new PersistException(e);
        }
    }

    @Override
    protected void prepareStatementForDelete(PreparedStatement ps, OrderDoc orderDoc) throws PersistException {

    }
}
