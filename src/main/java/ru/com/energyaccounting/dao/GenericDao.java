package ru.com.energyaccounting.dao;

import javafx.collections.ObservableList;

import java.io.Serializable;
import java.sql.Connection;

/**
 *  пример: GenericDao |Person, PK extends Serializable>
 *  выяснить хачем PK
 */
public interface GenericDao<E, PK extends Serializable> {


    E create() throws PersistException;

    /**
     * Создание объекта и запись его в бд(создание -> запись)
     * @param object
     * @return
     * @throws PersistException
     */
    E persist(E object) throws PersistException;
    E getByPk(int key) throws PersistException;
    ObservableList<E> getOLByPk(int key) throws PersistException;
    void update(E object) throws PersistException;
    void delete(E object) throws PersistException;
    ObservableList<E> getAll() throws PersistException;
    ObservableList<E> getAll(Connection connection) throws PersistException;

    /**
     *
     * @param sql выборка SELECT
     * @param who switch
     * @return ObservableList
     * @throws PersistException
     */
    ObservableList<E> getAll(String sql, String who) throws PersistException;
}
