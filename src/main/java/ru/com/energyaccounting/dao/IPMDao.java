package ru.com.energyaccounting.dao;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import ru.com.energyaccounting.model.IPM;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;

/**
 * Created by UserSts on 22.05.2017.
 */
public class IPMDao extends AbstractJDBCDao<IPM, Integer> {
    public IPMDao(Connection connection) {
        super(connection);
    }

    @Override
    public IPM create() throws PersistException {
        IPM i = new IPM();
        return persist(i);
    }


    @Override
    public String getSelectQuery() {
        return "SELECT * FROM ipms_view";
    }

    @Override
    public String getCreateQuery() {
        return "INSERT INTO ipms (inventory_number, ipm_type_id,  vt_id, el_install_id) VALUES (?,?,?,?); ";
    }

    @Override
    public String getUpdateQuery() {
        return "UPDATE ipms SET inventory_number = ?, ipm_type_id = ?, vt_id = ?, el_install_id = ? WHERE id = ?";
    }

    @Override
    public String getDeleteQuery() {
        return "DELETE FROM ipms WHERE id = ?";
    }

    @Override
    protected ObservableList<IPM> parseResultSet(ResultSet rs) throws PersistException {
        ObservableList<IPM> result = FXCollections.observableArrayList();
         try {
             while (rs.next()){
                 IPM i = new IPM();
                 i.setId(rs.getInt("id"));
                 i.setImpType(rs.getString("ipmType"));
                 i.setImp_type_id(rs.getInt("ipm_type_id"));
                 i.setVoltageType(rs.getString("voltageType"));
                 i.setVt_id(rs.getInt("vt_id"));
                 i.setInventoryNumber(rs.getString("inventory_number")); //может быть и String
                 i.setElectricalInstallation(rs.getString("electricalInstallation"));
                 i.setEl_install_id(rs.getInt("el_install_id"));
                 i.setStatus(rs.getString("status"));
                 i.setDescription(rs.getString("description"));
                 try {
                     i.setLastTestDate(LocalDate.parse(rs.getString("last_test_date"), dtfDate));
                 }catch (NullPointerException |DateTimeParseException e){
                     i.setLastTestDate(null);
                 }try {
                     i.setNextTestDate(LocalDate.parse(rs.getString("next_test_date"), dtfDate));
                 }catch (NullPointerException |DateTimeParseException e){
                     i.setNextTestDate(null);
                 }
                 result.add(i);
             }
         }catch (SQLException e){
             throw new PersistException(e);
         }


        return result;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement ps, IPM ipm) throws PersistException {
        try {
            ps.setString(1,ipm.getInventoryNumber());
            ps.setInt(2, ipm.getImp_type_id());
            ps.setInt(3, ipm.getVt_id());
            ps.setInt(4, ipm.getEl_install_id());

        }catch (SQLException e){
            throw new PersistException(e);
        }
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement ps, IPM ipm) throws PersistException {
        try {
            ps.setString(1,ipm.getInventoryNumber());
            ps.setInt(2, ipm.getImp_type_id());
            ps.setInt(3, ipm.getVt_id());
            ps.setInt(4, ipm.getEl_install_id());
            ps.setInt(5, ipm.getId());
        }catch (SQLException e){
            throw new PersistException(e);
        }
    }

    @Override
    protected void prepareStatementForDelete(PreparedStatement ps, IPM ipm) throws PersistException {

    }
}
