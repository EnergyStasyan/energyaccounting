package ru.com.energyaccounting.dao;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import ru.com.energyaccounting.model.ServiceMessage;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;

/**
 * Created by UserSts on 04.05.2017.
 */
public class ServiceMessageDao extends AbstractJDBCDao<ServiceMessage, Integer> {

    public ServiceMessageDao(Connection connection) {
        super(connection);
    }

    @Override
    public ServiceMessage create() throws PersistException {
        ServiceMessage s = new ServiceMessage();
        return persist(s);
    }

    @Override
    public String getSelectQuery() {
        return "SELECT * FROM service_messages ORDER BY event_date_time DESC ";
    }

    @Override
    public String getCreateQuery() {
        return "INSERT INTO service_messages (message_type, description) VALUES (?,?)";
    }

    @Override
    public String getUpdateQuery() {
        return null;
    }

    @Override
    public String getDeleteQuery() {
        return null;
    }

    @Override
    protected ObservableList<ServiceMessage> parseResultSet(ResultSet rs) throws PersistException {
        ObservableList<ServiceMessage> result = FXCollections.observableArrayList();
        try {
            while (rs.next()){
                ServiceMessage sm = new ServiceMessage();
                sm.setId(rs.getInt("id"));
                sm.setEventDateTime(LocalDateTime.parse(rs.getString("event_date_time"),dtfDateTime));
                sm.setMessageType(rs.getString("message_type"));
                sm.setDescription(rs.getString("description"));

                result.add(sm);
            }
        }catch (SQLException e){
            throw new PersistException(e);
        }

        return result;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement ps, ServiceMessage object) throws PersistException {
        try {
            ps.setString(1,object.getMessageType());
            ps.setString(2,object.getDescription());

        }catch (SQLException e){
            throw new PersistException(e);
        }
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement ps, ServiceMessage object) throws PersistException {

    }

    @Override
    protected void prepareStatementForDelete(PreparedStatement ps, ServiceMessage object) throws PersistException {

    }
}
