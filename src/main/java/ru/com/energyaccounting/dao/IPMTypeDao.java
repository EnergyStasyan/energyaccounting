package ru.com.energyaccounting.dao;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import ru.com.energyaccounting.model.IPMType;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by UserSts on 22.05.2017.
 */
public class IPMTypeDao extends AbstractJDBCDao<IPMType, Integer>{

    public IPMTypeDao(Connection connection) {
        super(connection);
    }

    @Override
    public IPMType create() throws PersistException {
        IPMType i = new IPMType();
        return persist(i);
    }

    @Override
    public String getSelectQuery() {
        return "SELECT id, name, description FROM ipm_types";
    }

    @Override
    public String getCreateQuery() {
        return "INSERT INTO ipm_types (name,description) VALUES (?,?)";
    }

    @Override
    public String getUpdateQuery() {
        return "UPDATE ipm_types SET name = ?, description = ? WHERE id = ?";
    }

    @Override
    public String getDeleteQuery() {
        return "DELETE FROM ipm_types WHERE id = ?";
    }

    @Override
    protected ObservableList<IPMType> parseResultSet(ResultSet rs) throws PersistException {
        ObservableList<IPMType> result = FXCollections.observableArrayList();
         try {
             while (rs.next()){
                 IPMType i = new IPMType();
                 i.setId(rs.getInt("id"));
                 i.setName(rs.getString("name"));
                 i.setDescription(rs.getString("description"));
                 result.add(i);
             }

         }catch (SQLException e){
             throw new PersistException(e);
         }

        return result;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement ps, IPMType ipmType) throws PersistException {
        try {
            ps.setString(1, ipmType.getName());
            ps.setString(2, ipmType.getDescription());
        }catch (SQLException e){
            throw new PersistException(e);
        }
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement ps, IPMType ipmType) throws PersistException {
        try {
            ps.setString(1, ipmType.getName());
            ps.setString(2, ipmType.getDescription());
            ps.setInt(3, ipmType.getId());
        }catch (SQLException e){
            throw new PersistException(e);
        }

    }

    @Override
    protected void prepareStatementForDelete(PreparedStatement ps, IPMType ipmType) throws PersistException {

    }
}
