package ru.com.energyaccounting.dao;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import ru.com.energyaccounting.model.SubstationElInstallationElEquipment;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SubstationElInstallationElEquipmentDao extends AbstractJDBCDao<SubstationElInstallationElEquipment, Integer>{
    public SubstationElInstallationElEquipmentDao(Connection connection) {
        super(connection);
    }

    @Override
    public String getSelectQuery() {
        return "SELECT * FROM sub_ee_ei_view";
    }

    @Override
    public String getCreateQuery() {
        return null;
    }

    @Override
    public String getUpdateQuery() {
        return null;
    }

    @Override
    public String getDeleteQuery() {
        return null;
    }

    @Override
    protected ObservableList<SubstationElInstallationElEquipment> parseResultSet(ResultSet rs) throws PersistException {
        ObservableList<SubstationElInstallationElEquipment> result = FXCollections.observableArrayList();
        try {
            while (rs.next()){
                SubstationElInstallationElEquipment s = new SubstationElInstallationElEquipment();
                s.setSubstationId(rs.getInt("sub_id"));
                s.setSubstation(rs.getString("substation"));
                s.setElectricalInstallationsId(rs.getInt("ei_id"));
                s.setElectricalInstallation(rs.getString("electrical_installation"));
                s.setElectricalEquipmentId(rs.getInt("ee_id"));
                s.setElectricalEquipment(rs.getString("electrical_equipment"));

                result.add(s);
            }
        }catch (SQLException e){
            e.printStackTrace();
            throw new PersistException(e);
        }
        return result;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement ps, SubstationElInstallationElEquipment object) throws PersistException {

    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement ps, SubstationElInstallationElEquipment object) throws PersistException {

    }

    @Override
    protected void prepareStatementForDelete(PreparedStatement ps, SubstationElInstallationElEquipment object) throws PersistException {

    }
}
