package ru.com.energyaccounting.dao;

import ru.com.energyaccounting.model.*;
import ru.com.energyaccounting.util.AddressDBSingleton;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by UserSts on 02.05.2017.
 */
public class DaoFactory implements DaoFactoryInterface<Connection> {


    public static final String JDBC_DRIVER = "org.sqlite.JDBC";
    //public static final String connStr = "jdbc:sqlite:EA.db";
    public static final String connStr = "jdbc:sqlite:";


    private Map<Class, DaoCreator> creators;

    public DaoFactory() {
        try {
            Class.forName(JDBC_DRIVER);
        }catch (ClassNotFoundException e){
            e.printStackTrace();
        }

        creators = new HashMap<>();
        creators.put(Person.class,                  (DaoCreator<Connection>) PersonDao::new);
        creators.put(ServiceMessage.class,          (DaoCreator<Connection>) ServiceMessageDao::new);
        creators.put(ElectricalInstallation.class,  (DaoCreator<Connection>) ElectricalInstallationDao::new);

        creators.put(ElectricalEquipment.class,       (DaoCreator<Connection>) ElectricalEquipmentDao::new);
        creators.put(ElectricalEquipmentType.class,   (DaoCreator<Connection>) ElectricalEquipmentTypeDao::new);
        creators.put(ElectricalEquipmentDoc.class,   (DaoCreator<Connection>) ElectricalEquipmentDocDao::new);
        creators.put(ElectricalEquipmentDocElectricalEquipment.class,   (DaoCreator<Connection>) ElectricalEquipmentDocElectricalEquipmentDao::new);

        creators.put(Subdivision.class,             (DaoCreator<Connection>) SubdivisionDao::new);
        creators.put(Substation.class,              (DaoCreator<Connection>) SubstationDao::new);
        creators.put(Organization.class,            (DaoCreator<Connection>) OrganizationDao::new);
        creators.put(IPMType.class,                 (DaoCreator<Connection>) IPMTypeDao::new);
        creators.put(IPM.class,                     (DaoCreator<Connection>) IPMDao::new);
        creators.put(Extinguisher.class,            (DaoCreator<Connection>) ExtinguisherDao::new);
        creators.put(ExtinguisherType.class,        (DaoCreator<Connection>) ExtinguisherTypeDao::new);
        creators.put(Shift.class,                   (DaoCreator<Connection>) ShiftDao::new);
        creators.put(VoltageType.class,             (DaoCreator<Connection>) VoltageTypeDao::new);
        creators.put(MainFrame.class,               (DaoCreator<Connection>) MainFrameDao::new);
        creators.put(Position.class,                (DaoCreator<Connection>) PositionDao::new);
        creators.put(AccessGroup.class,             (DaoCreator<Connection>) AccessGroupDao::new);
        creators.put(OrderDoc.class,                (DaoCreator<Connection>) OrderDocDao::new);
        creators.put(OrderDocPerson.class,                (DaoCreator<Connection>) OrderDocPersonDao::new);
        creators.put(OrderDocElectricalEquipment.class,    (DaoCreator<Connection>) OrderDocElectricalEquipmentDao::new);
        creators.put(SubstationElInstallationElEquipment.class, (DaoCreator<Connection>) SubstationElInstallationElEquipmentDao::new);
        creators.put(IpmDoc.class,                  (DaoCreator<Connection>) IpmDocDao::new);
        creators.put(IpmDocIpm.class,               (DaoCreator<Connection>) IpmDocIpmDao::new);
        creators.put(ExtinguisherDoc.class,         (DaoCreator<Connection>) ExtinguisherDocDao::new);
        creators.put(ExtinguisherDocExting.class,   (DaoCreator<Connection>) ExtinguisherDocExtingDao::new);

        // TODO: 02.05.2017 дописать все классы объектов

    }

    @Override
    public Connection getContext() throws PersistException {
        Connection connection;
        String basePath = AddressDBSingleton.getInstance().getStartChoice().getFilePath();
        try {
            connection = DriverManager.getConnection(connStr + basePath);
        }catch (SQLException e){
            throw new PersistException(e);
        }
        return connection;
    }

    @Override
    public GenericDao getDao(Connection context, Class dtoClass) throws PersistException {
        DaoCreator creator = creators.get(dtoClass);
        if (creator == null) {
            throw new PersistException("Dao object for "+ dtoClass + " not found in ConnectionHashMap");
        }


        return creator.create(context);
    }
}
