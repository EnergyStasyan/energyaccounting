package ru.com.energyaccounting.dao;

import ru.com.energyaccounting.controller.docs.ExtinguisherDocAddOrEditController;
import ru.com.energyaccounting.model.Root;

import java.sql.Connection;
import java.util.logging.Logger;

public class CRUDDao<E extends Root> {
    private static final Logger LOG = Logger.getLogger(CRUDDao.class.getName());

    private E objectToDao;
    private E objectFromDao;
    private GenericDao dao;

//    https://docs.oracle.com/javase/tutorial/extra/generics/wildcards.html
//    засовываем коллекцию или один объект


    public CRUDDao(E object) {
        this.objectToDao = object;
        try {
            DaoFactoryInterface<Connection> factory = new DaoFactory();
            Connection connection = factory.getContext();
            dao = factory.getDao(connection, object.getClass());
        } catch (PersistException e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unchecked")
    public void objectPersistUpdate() throws PersistException {
        if (objectToDao.getId() == 0) {
            LOG.info("попытка записать");
            objectFromDao = (E) dao.persist(this.objectToDao);
        } else {
            LOG.info("попытка обновить");
            dao.update(this.objectToDao);
        }
    }

    @SuppressWarnings("unchecked")
    public void remove() throws PersistException { // FIXME: 04.06.2018  использовать BATCH
        if (objectToDao.getId() > 0) {
            dao.delete(objectToDao);
        }
    }

    public E getObjectFromDao() {
        return objectFromDao;
    }
}
