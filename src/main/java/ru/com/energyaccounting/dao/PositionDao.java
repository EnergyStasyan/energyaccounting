package ru.com.energyaccounting.dao;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import ru.com.energyaccounting.model.Position;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by UserSts on 26.06.2017.
 */
public class PositionDao extends AbstractJDBCDao<Position, Integer>{
    public PositionDao(Connection connection) {
        super(connection);
    }
    @Override
    public Position create() throws PersistException {
        Position p = new Position();
        return persist(p);
    }

    @Override
    public String getSelectQuery() {
        return "SELECT * FROM positions";
    }

    @Override
    public String getCreateQuery() {
        return "INSERT INTO positions (name, description) VALUES (?, ?)";
    }

    @Override
    public String getUpdateQuery() {
        return "UPDATE positions SET name = ?, description = ? WHERE id = ?";
    }

    @Override
    public String getDeleteQuery() {
        return "DELETE FROM positions WHERE id = ?";
    }

    @Override
    protected ObservableList<Position> parseResultSet(ResultSet rs) throws PersistException {
        ObservableList<Position> result = FXCollections.observableArrayList();
        try {
            while (rs.next()){
                Position o = new Position();
                o.setId(rs.getInt("id"));
                o.setName(rs.getString("name"));
                o.setDescription(rs.getString("description"));
                result.add(o);
            }
        }catch (SQLException e){
            throw new PersistException(e);
        }
        return result;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement ps, Position position) throws PersistException {
        try {
            ps.setString(1, position.getName());
            ps.setString(2, position.getDescription());

        }catch (SQLException e){
            throw new PersistException(e);
        }
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement ps, Position position) throws PersistException {
        try {
            ps.setString(1, position.getName());
            ps.setString(2, position.getDescription());
            ps.setInt(3, position.getId());

        }catch (SQLException e){
            throw new PersistException(e);
        }
    }

    @Override
    protected void prepareStatementForDelete(PreparedStatement ps, Position position) throws PersistException {

    }
}
