package ru.com.energyaccounting.dao;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import ru.com.energyaccounting.model.Subdivision;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by UserSts on 12.05.2017.
 */
public class SubdivisionDao extends AbstractJDBCDao<Subdivision, Integer>{

    public SubdivisionDao(Connection connection) {
        super(connection);
    }

    @Override
    public Subdivision create() throws PersistException {
        Subdivision s = new Subdivision();
        return persist(s);
    }

    @Override
    public String getSelectQuery() {
        return "SELECT * FROM subdivisions_view";
    }

    @Override
    public String getCreateQuery() {
        return "INSERT INTO subdivisions ( name, org_id) VALUES (?,?)";
    }

    @Override
    public String getUpdateQuery() {
        return "UPDATE subdivisions SET name = ?, org_id = ? WHERE id = ?";
    }

    @Override
    public String getDeleteQuery() {
        return "DELETE FROM subdivisions WHERE id = ?";
    }

    @Override
    protected ObservableList<Subdivision> parseResultSet(ResultSet rs) throws PersistException {
       ObservableList<Subdivision> result = FXCollections.observableArrayList();
       try {
           while (rs.next()){
               Subdivision s = new Subdivision();
               s.setId(rs.getInt("id"));
               s.setName(rs.getString("name"));
               s.setOrg_id(rs.getInt("org_id"));
               s.setOrganization(rs.getString("organization"));
               result.add(s);
           }
       }catch (SQLException e){
           throw new PersistException(e);
       }
        return result;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement ps, Subdivision subdivision) throws PersistException {
        try{
           // ps.setInt(1, subdivision.getId());
            ps.setString(1, subdivision.getName());
            ps.setInt(2, subdivision.getOrg_id());
        }catch (SQLException e){
            throw new PersistException(e);
        }
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement ps, Subdivision subdivision) throws PersistException {
        try{
            ps.setString(1, subdivision.getName());
            ps.setInt(2, subdivision.getOrg_id());
            ps.setInt(3, subdivision.getId());
        }catch (SQLException e){
            throw new PersistException(e);
        }
    }

    @Override
    protected void prepareStatementForDelete(PreparedStatement ps, Subdivision subdivision) throws PersistException {

    }
}
