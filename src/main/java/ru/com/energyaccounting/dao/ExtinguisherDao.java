package ru.com.energyaccounting.dao;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import ru.com.energyaccounting.model.Extinguisher;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;

/**
 * Created by UserSts on 23.05.2017.
 */
public class ExtinguisherDao extends AbstractJDBCDao<Extinguisher, Integer> {
    public ExtinguisherDao(Connection connection) {
        super(connection);
    }

    @Override
    public Extinguisher create() throws PersistException {
        Extinguisher e = new Extinguisher();
        return persist(e);
    }

    @Override
    public String getSelectQuery() {
        return "SELECT * FROM extinguishers_view";
    }

    @Override
    public String getCreateQuery() {
        return "INSERT INTO extinguishers (ext_type_id,inventory_number , el_install_id) VALUES (?,?,?)";
    }

    @Override
    public String getUpdateQuery() {
        return "UPDATE extinguishers SET ext_type_id = ?, inventory_number = ?, el_install_id = ? WHERE id = ?;";
    }

    @Override
    public String getDeleteQuery() {
        return "DELETE FROM extinguishers WHERE id = ?;";
    }

    @Override
    protected ObservableList<Extinguisher> parseResultSet(ResultSet rs) throws PersistException {
        ObservableList<Extinguisher> result = FXCollections.observableArrayList();
        try {
            while (rs.next()) {
                Extinguisher e = new Extinguisher();
                e.setId(rs.getInt("id"));
                e.setInventoryNumber(rs.getString("inventory_number"));
                e.setExt_type_id(rs.getInt("ext_type_id"));
                e.setExtinguisherType(rs.getString("extType"));
                try {
                    e.setNextTestDate(LocalDate.parse(rs.getString("next_test_date"), dtfDate));
                } catch (NullPointerException | DateTimeParseException n) {
                    e.setNextTestDate(null);
                }
                try {
                    e.setLastTestDate(LocalDate.parse(rs.getString("last_test_date"), dtfDate));
                } catch (NullPointerException | DateTimeParseException n) {
                    e.setLastTestDate(null);
                }
                e.setEl_install_id(rs.getInt("el_install_id"));
                e.setElectricalInstallation(rs.getString("electricalInstallation"));
                e.setStatus(rs.getString("status"));
                e.setDescription(rs.getString("description"));
                result.add(e);
            }
        } catch (SQLException e) {
            throw new PersistException(e);
        }
        return result;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement ps, Extinguisher extinguisher) throws PersistException {
// TODO: 29.05.2017 доделать
        try {
            ps.setInt(1, extinguisher.getExt_type_id());
            ps.setString(2, extinguisher.getInventoryNumber());
            ps.setInt(3, extinguisher.getEl_install_id());
        } catch (SQLException e) {
            throw new PersistException(e);
        }
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement ps, Extinguisher extinguisher) throws PersistException {
        try {
            ps.setInt(1, extinguisher.getExt_type_id());
            ps.setString(2, extinguisher.getInventoryNumber());
            ps.setInt(3, extinguisher.getEl_install_id());
            ps.setInt(4, extinguisher.getId());
        } catch (SQLException e) {
            throw new PersistException(e);
        }
    }

    @Override
    protected void prepareStatementForDelete(PreparedStatement ps, Extinguisher extinguisher) throws PersistException {

    }
}
