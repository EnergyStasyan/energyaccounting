package ru.com.energyaccounting.dao;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import ru.com.energyaccounting.model.ElectricalEquipmentType;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by UserSts on 13.01.2021.
 */
public class ElectricalEquipmentTypeDao extends AbstractJDBCDao<ElectricalEquipmentType, Integer>{

    public ElectricalEquipmentTypeDao(Connection connection) {
        super(connection);
    }

    @Override
    public ElectricalEquipmentType create() throws PersistException {
        ElectricalEquipmentType i = new ElectricalEquipmentType();
        return persist(i);
    }

    @Override
    public String getSelectQuery() {
        return "SELECT id, name, description FROM electrical_equipment_types";
    }

    @Override
    public String getCreateQuery() {
        return "INSERT INTO electrical_equipment_types (name,description) VALUES (?,?)";
    }

    @Override
    public String getUpdateQuery() {
        return "UPDATE electrical_equipment_types SET name = ?, description = ? WHERE id = ?";
    }

    @Override
    public String getDeleteQuery() {
        return "DELETE FROM electrical_equipment_types WHERE id = ?";
    }

    @Override
    protected ObservableList<ElectricalEquipmentType> parseResultSet(ResultSet rs) throws PersistException {
        ObservableList<ElectricalEquipmentType> result = FXCollections.observableArrayList();
         try {
             while (rs.next()){
                 ElectricalEquipmentType i = new ElectricalEquipmentType();
                 i.setId(rs.getInt("id"));
                 i.setName(rs.getString("name"));
                 i.setDescription(rs.getString("description"));
                 result.add(i);
             }

         }catch (SQLException e){
             throw new PersistException(e);
         }

        return result;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement ps, ElectricalEquipmentType electricalEquipmentType) throws PersistException {
        try {
            ps.setString(1, electricalEquipmentType.getName());
            ps.setString(2, electricalEquipmentType.getDescription());
        }catch (SQLException e){
            throw new PersistException(e);
        }
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement ps, ElectricalEquipmentType ElectricalEquipmentType) throws PersistException {
        try {
            ps.setString(1, ElectricalEquipmentType.getName());
            ps.setString(2, ElectricalEquipmentType.getDescription());
            ps.setInt(3, ElectricalEquipmentType.getId());
        }catch (SQLException e){
            throw new PersistException(e);
        }

    }

    @Override
    protected void prepareStatementForDelete(PreparedStatement ps, ElectricalEquipmentType ElectricalEquipmentType) throws PersistException {

    }
}
