package ru.com.energyaccounting.dao;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import ru.com.energyaccounting.model.ElectricalEquipment;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;

/**
 * Created by UserSts on 12.05.2017.
 */
public class ElectricalEquipmentDao extends AbstractJDBCDao<ElectricalEquipment, Integer> {
    public ElectricalEquipmentDao(Connection connection) {
        super(connection);
    }

    @Override
    public ElectricalEquipment create() throws PersistException {
        ElectricalEquipment e = new ElectricalEquipment();
        return persist(e);
    }

    @Override
    public String getSelectQuery() {
        return "SELECT * FROM electrical_equipments_view";
    }

    @Override
    public String getCreateQuery() {
        return "INSERT INTO electrical_equipments (inventory_number, name, vt_id, electrical_equipment_type_id, el_install_id, description) VALUES (?,?,?,?,?,?)";
    }

    @Override
    public String getUpdateQuery() {
        return "UPDATE electrical_equipments SET inventory_number = ?,name = ?, vt_id = ?, electrical_equipment_type_id = ?, el_install_id = ?, description = ? WHERE id = ? ";
    }

    @Override
    public String getDeleteQuery() {
        return "DELETE FROM electrical_equipments WHERE id = ?";
    }

    @Override
    protected ObservableList<ElectricalEquipment> parseResultSet(ResultSet rs) throws PersistException {
        ObservableList<ElectricalEquipment> result = FXCollections.observableArrayList();
        try {
            while (rs.next()){
                ElectricalEquipment ei = new ElectricalEquipment();
                ei.setId(rs.getInt("id")); //получаем индекс
                ei.setInventoryNumber(rs.getString("inventory_number"));
                ei.setName(rs.getString("name"));            //получаем имя электроустановки
                ei.setVoltageType(rs.getString("voltageType")); //получаем класс напряжения из базы
                ei.setVt_id(rs.getInt("vt_id"));
                ei.setElectricalInstallation(rs.getString("electricalInstallation"));    //получаем 'электроустановку
                ei.setEl_install_id(rs.getInt("el_install_id"));
                ei.setElectricalEquipmentType(rs.getString("electricalEquipmentType"));    //получаем тип электрооборудования
                ei.setElectrical_equipment_type_id(rs.getInt("electrical_equipment_type_id"));
                ei.setDescription(rs.getString("description"));                //получаем примечание
                ei.setStatus(rs.getString("status"));

                try {
                    ei.setLastTestDate(LocalDate.parse(rs.getString("last_test_date"), dtfDate));
                }catch (NullPointerException | DateTimeParseException e){
                    ei.setLastTestDate(null);
                }try {
                    ei.setNextTestDate(LocalDate.parse(rs.getString("next_test_date"), dtfDate));
                }catch (NullPointerException |DateTimeParseException e){
                    ei.setNextTestDate(null);
                }
                result.add(ei);
            }
        }catch (SQLException e){
            throw new PersistException(e);
        }
        return result;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement ps, ElectricalEquipment electricalEquipment) throws PersistException {
        try {
            ps.setString(1, electricalEquipment.getInventoryNumber());
            ps.setString(2, electricalEquipment.getName());
            ps.setInt(3, electricalEquipment.getVt_id());
            ps.setInt(4, electricalEquipment.getElectrical_equipment_type_id());
            ps.setInt(5, electricalEquipment.getEl_install_id());
            ps.setString(6, electricalEquipment.getDescription());
        }catch (SQLException e){
            throw new PersistException(e);
        }
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement ps, ElectricalEquipment electricalEquipment) throws PersistException {
        try {
            ps.setString(1, electricalEquipment.getInventoryNumber());
            ps.setString(2, electricalEquipment.getName());
            ps.setInt(3, electricalEquipment.getVt_id());
            ps.setInt(4, electricalEquipment.getElectrical_equipment_type_id());
            ps.setInt(5, electricalEquipment.getEl_install_id());
            ps.setString(6, electricalEquipment.getDescription());
            ps.setInt(7, electricalEquipment.getId());
        }catch (SQLException e){
            throw new PersistException(e);
        }
    }

    @Override
    protected void prepareStatementForDelete(PreparedStatement ps, ElectricalEquipment object) throws PersistException {

    }
}
