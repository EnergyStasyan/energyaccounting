package ru.com.energyaccounting.dao;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import ru.com.energyaccounting.model.AccessGroup;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.DateTimeException;
import java.time.LocalDate;

/**
 * Created by UserSts on 07.07.2017.
 */
public class AccessGroupDao extends AbstractJDBCDao<AccessGroup, Integer> {
    public AccessGroupDao(Connection connection) {
        super(connection);
    }


    @Override
    public String getSelectQuery() {
        return "SELECT * FROM access_groups_view;";
    }

    @Override
    public String getCreateQuery() {
        return "INSERT INTO access_groups (number_of_order, number_of_access_group, order_date, last_test_date, next_test_date, director_id, person_id, description) VALUES (?,?,?,?,?,?,?,?);"; //8 + 2
    }

    @Override
    public String getUpdateQuery() {
        return "UPDATE access_groups SET number_of_order = ?, number_of_access_group = ?, order_date = ?, last_test_date = ?, next_test_date = ?, director_id = ?, person_id = ?, description = ? WHERE id = ?; "; //9
    }

    @Override
    public String getDeleteQuery() {
        return "DELETE FROM access_groups WHERE id = ?";
    }

    @Override
    protected ObservableList<AccessGroup> parseResultSet(ResultSet rs) throws PersistException {
        ObservableList<AccessGroup> result = FXCollections.observableArrayList();
        try {
            while (rs.next()){
                AccessGroup a = new AccessGroup();
                a.setId(rs.getInt("id"));
                a.setNumberOfOrder(rs.getString("number_of_order"));
                a.setNumberOfAccessGroup(rs.getString("number_of_access_group"));
                a.setDirectorId(rs.getInt("director_id"));
                a.setDirector(rs.getString("director"));
                a.setPersonId(rs.getInt("person_id"));
                a.setPerson(rs.getString("person"));
                a.setDescription(rs.getString("description"));
                try {
                    a.setLastTestDate(LocalDate.parse(rs.getString("last_test_date"), dtfDate));
                }catch (NullPointerException | DateTimeException e){
                    a.setLastTestDate(null);
                    log.warning("Ошибка парсинга даты последнего экзамена: " + e);
                }
                try {
                    a.setNextTestDate(LocalDate.parse(rs.getString("next_test_date"), dtfDate));
                }catch (NullPointerException | DateTimeException e){
                    a.setNextTestDate(null);
                    log.warning("Ошибка парсинга даты следующего экзамена: " + e);
                }
                try {
                    a.setOrderDate(LocalDate.parse(rs.getString("order_date"), dtfDate));
                }catch (NullPointerException | DateTimeException e){
                    a.setOrderDate(null);
                    log.warning("Ошибка парсинга даты приказа экзамена: " + e);
                }

                result.add(a);
            }
        }catch (SQLException e){
            throw new PersistException(e);
        }

        return result;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement ps, AccessGroup accessGroup) throws PersistException {

        try{
            ps.setString(1, accessGroup.getNumberOfOrder());
            ps.setString(2, accessGroup.getNumberOfAccessGroup());
            if (accessGroup.getOrderDate() != null){
                ps.setString(3, accessGroup.getOrderDate().toString());
            }
            if (accessGroup.getLastTestDate() != null){
                ps.setString(4, accessGroup.getLastTestDate().toString());
            }
            if (accessGroup.getNextTestDate() != null){
                ps.setString(5, accessGroup.getNextTestDate().toString());
            }
            ps.setInt(6, accessGroup.getDirectorId());
            ps.setInt(7, accessGroup.getPersonId());
            ps.setString(8, accessGroup.getDescription());
//            ps.setInt(9, accessGroup.getNumberOfAccessGroup());
//            ps.setInt(10, accessGroup.getPersonId());

        }catch (SQLException e){
            throw new PersistException(e);
        }

    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement ps, AccessGroup accessGroup) throws PersistException {
        try{
            ps.setString(1, accessGroup.getNumberOfOrder());
            ps.setString(2, accessGroup.getNumberOfAccessGroup());
            if (accessGroup.getOrderDate() != null){
                ps.setString(3, accessGroup.getOrderDate().toString());
            }
            if (accessGroup.getLastTestDate() != null){
                ps.setString(4, accessGroup.getLastTestDate().toString());
            }
            if (accessGroup.getNextTestDate() != null){
                ps.setString(5, accessGroup.getNextTestDate().toString());
            }
            ps.setInt(6, accessGroup.getDirectorId());
            ps.setInt(7, accessGroup.getPersonId());
            ps.setString(8, accessGroup.getDescription());
            ps.setInt(9, accessGroup.getId());
//            ps.setInt(10, accessGroup.getNumberOfAccessGroup());
//            ps.setInt(11, accessGroup.getPersonId());
        }catch (SQLException e){
            throw new PersistException(e);
        }

    }

    @Override
    protected void prepareStatementForDelete(PreparedStatement ps, AccessGroup accessGroup) throws PersistException {

    }
}
