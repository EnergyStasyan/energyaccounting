package ru.com.energyaccounting.dao;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import ru.com.energyaccounting.model.IpmDocIpm;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.DateTimeException;
import java.time.LocalDate;

public class IpmDocIpmDao extends AbstractJDBCDao<IpmDocIpm, Integer> {

    public IpmDocIpmDao(Connection connection) {
        super(connection);
    }

    @Override
    public String getSelectQuery() {
        return "SELECT * FROM ipm_doc_ipm_view";
    }

    @Override
    public String getCreateQuery() {
        return " INSERT INTO ipm_doc_ipm (ipm_doc_id, ipm_id, last_test, next_test, status, description ) VALUES ( ?, ?, ?, ?, ?, ?);";
    }

    @Override
    public String getUpdateQuery() {
        return "UPDATE ipm_doc_ipm SET ipm_doc_id = ?, ipm_id = ?, last_test = ?, next_test = ?, status = ? ,description = ? WHERE id = ?";
    }

    @Override
    public String getDeleteQuery() {
        return "DELETE FROM ipm_doc_ipm WHERE id = ?;";
    }

    @Override
    protected ObservableList<IpmDocIpm> parseResultSet(ResultSet rs) throws PersistException {
        ObservableList<IpmDocIpm> result = FXCollections.observableArrayList();
        try {
            while (rs.next()){
                IpmDocIpm ipm = new IpmDocIpm();
                ipm.setDoc_id(rs.getInt("doc_id"));
                ipm.setId(rs.getInt("id"));
                ipm.setIpm_id(rs.getInt("ipm_id"));
                ipm.setName(rs.getString("name"));
                ipm.setInventoryNumber(rs.getString("inventory_number"));

                try {
                    ipm.setLastTestDate(LocalDate.parse(rs.getString("last_test")));
                }catch (NullPointerException | DateTimeException e){
                    ipm.setLastTestDate(null);
                    log.warning("Ошибка парсинга даты последнего теста: " + e);
                }

                try {
                    ipm.setNextTestDate(LocalDate.parse(rs.getString("next_test")));
                }catch (NullPointerException | DateTimeException e){
                    ipm.setNextTestDate(null);
                    log.warning("Ошибка парсинга даты следующего теста: " + e);
                }

                ipm.setStatus(rs.getString("status"));
                ipm.setDescription(rs.getString("description"));

                result.add(ipm);
            }

        }catch (SQLException e){
            e.printStackTrace();
        }


        return result;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement ps, IpmDocIpm ipmDocIpm) throws PersistException {
        try {
            ps.setInt(1, ipmDocIpm.getDoc_id());
            ps.setInt(2, ipmDocIpm.getIpm_id());

            if (ipmDocIpm.getLastTestDate()!=null){
                ps.setString(3, ipmDocIpm.getLastTestDate().toString());
            }
            if (ipmDocIpm.getNextTestDate()!=null){
                ps.setString(4, ipmDocIpm.getNextTestDate().toString());
            }
            ps.setString(5, ipmDocIpm.getStatus());
            ps.setString(6, ipmDocIpm.getDescription());


        } catch (SQLException e) {
            throw new PersistException(e);
        }
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement ps, IpmDocIpm ipmDocIpm) throws PersistException {
        try {
            ps.setInt(1, ipmDocIpm.getDoc_id());
            ps.setInt(2, ipmDocIpm.getIpm_id());

            if (ipmDocIpm.getLastTestDate()!=null){
                ps.setString(3, ipmDocIpm.getLastTestDate().toString());
            }
            if (ipmDocIpm.getNextTestDate()!=null){
                ps.setString(4, ipmDocIpm.getNextTestDate().toString());
            }
            ps.setString(5, ipmDocIpm.getStatus());
            ps.setString(6, ipmDocIpm.getDescription());
            ps.setInt(7, ipmDocIpm.getId());


        } catch (SQLException e) {
            throw new PersistException(e);
        }
    }

    @Override
    protected void prepareStatementForDelete(PreparedStatement ps, IpmDocIpm ipmDocIpm) throws PersistException {

    }
}
