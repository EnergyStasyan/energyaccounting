package ru.com.energyaccounting.dao;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import ru.com.energyaccounting.model.Substation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by UserSts on 19.05.2017.
 */
public class SubstationDao extends AbstractJDBCDao<Substation, Integer> {

    public SubstationDao(Connection connection) {
        super(connection);
    }

    @Override
    public Substation create() throws PersistException {
      Substation s = new Substation();
      return persist(s);
    }

    @Override
    public String getSelectQuery() {
        return "SELECT * FROM substations_view;";
    }

    @Override
    public String getCreateQuery() {
        return "INSERT INTO substations(name, org_id) VALUES (?, ?)";
    }

    @Override
    public String getUpdateQuery() {
        return "UPDATE substations SET name = ?, org_id = ? WHERE id = ?";
    }

    @Override
    public String getDeleteQuery() {
        return "DELETE FROM substations WHERE id = ?";
    }

    @Override
    protected ObservableList<Substation> parseResultSet(ResultSet rs) throws PersistException {
        ObservableList<Substation> result = FXCollections.observableArrayList();
        try {
            while (rs.next()){
                Substation s = new Substation();
                s.setId(rs.getInt("id"));
                s.setName(rs.getString("name"));
                s.setOrg_id(rs.getInt("org_id"));
                s.setOrganization(rs.getString("organization"));
                result.add(s);
            }

        }catch (SQLException e){
            e.printStackTrace();
        }


        return result;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement ps, Substation substation) throws PersistException {
       try {
           ps.setString(1, substation.getName());
           ps.setInt(2, substation.getOrg_id());
       }catch (SQLException e){
           throw new PersistException(e);
       }
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement ps, Substation substation) throws PersistException {
        try {
            ps.setString(1, substation.getName());
            ps.setInt(2, substation.getOrg_id());
            ps.setInt(3, substation.getId());

        }catch (SQLException e){
            throw new PersistException(e);
        }
    }

    @Override
    protected void prepareStatementForDelete(PreparedStatement ps, Substation substation) throws PersistException {

    }
}
