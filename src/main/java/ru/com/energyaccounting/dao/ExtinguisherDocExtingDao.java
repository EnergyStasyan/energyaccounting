package ru.com.energyaccounting.dao;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import ru.com.energyaccounting.model.ExtinguisherDocExting;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.DateTimeException;
import java.time.LocalDate;

public class ExtinguisherDocExtingDao extends AbstractJDBCDao<ExtinguisherDocExting, Integer> {

    public ExtinguisherDocExtingDao(Connection connection) {
        super(connection);
    }

    @Override
    public String getSelectQuery() {
       return "SELECT * FROM extinguisher_doc_exting_view";

    }

    @Override
    public String getCreateQuery() {
        return " INSERT INTO extinguisher_doc_exting (ext_doc_id, ext_id, last_test, next_test, status, description ) VALUES ( ?, ?, ?, ?, ?, ?);";

    }

    @Override
    public String getUpdateQuery() {
        return "UPDATE extinguisher_doc_exting SET ext_doc_id = ?, ext_id = ?, last_test = ?, next_test = ?, status = ? ,description = ? WHERE id = ?";

    }

    @Override
    public String getDeleteQuery() {
        return "DELETE FROM extinguisher_doc_exting WHERE id = ?;";

    }

    @Override
    protected ObservableList<ExtinguisherDocExting> parseResultSet(ResultSet rs) throws PersistException {
        ObservableList<ExtinguisherDocExting> result = FXCollections.observableArrayList();
        try {
            while (rs.next()){
                ExtinguisherDocExting ede = new ExtinguisherDocExting();
                ede.setDoc_id(rs.getInt("doc_id"));
                ede.setId(rs.getInt("id"));
                ede.setExt_id(rs.getInt("ext_id"));
                ede.setName(rs.getString("name"));
                ede.setInventoryNumber(rs.getString("inventory_number"));

                try {
                    ede.setLastTestDate(LocalDate.parse(rs.getString("last_test")));
                }catch (NullPointerException | DateTimeException e){
                    ede.setLastTestDate(null);
                    log.warning("Ошибка парсинга даты последнего теста: " + e);
                }

                try {
                    ede.setNextTestDate(LocalDate.parse(rs.getString("next_test")));
                }catch (NullPointerException | DateTimeException e){
                    ede.setNextTestDate(null);
                    log.warning("Ошибка парсинга даты следующего теста: " + e);
                }

                ede.setStatus(rs.getString("status"));
                ede.setDescription(rs.getString("description"));

                result.add(ede);
            }

        }catch (SQLException e){
            e.printStackTrace();
        }
        return result;

    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement ps, ExtinguisherDocExting ede) throws PersistException {
        try {
            ps.setInt(1, ede.getDoc_id());
            ps.setInt(2, ede.getExt_id());

            if (ede.getLastTestDate()!=null){
                ps.setString(3, ede.getLastTestDate().toString());
            }
            if (ede.getNextTestDate()!=null){
                ps.setString(4, ede.getNextTestDate().toString());
            }
            ps.setString(5, ede.getStatus());
            ps.setString(6, ede.getDescription());


        } catch (SQLException e) {
            throw new PersistException(e);
        }
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement ps, ExtinguisherDocExting ede) throws PersistException {
        try {
            ps.setInt(1, ede.getDoc_id());
            ps.setInt(2, ede.getExt_id());

            if (ede.getLastTestDate()!=null){
                ps.setString(3, ede.getLastTestDate().toString());
            }
            if (ede.getNextTestDate()!=null){
                ps.setString(4, ede.getNextTestDate().toString());
            }
            ps.setString(5, ede.getStatus());
            ps.setString(6, ede.getDescription());
            ps.setInt(7, ede.getId());


        } catch (SQLException e) {
            throw new PersistException(e);
        }
    }

    @Override
    protected void prepareStatementForDelete(PreparedStatement ps, ExtinguisherDocExting object) throws PersistException {

    }
}
