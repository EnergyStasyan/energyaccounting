package ru.com.energyaccounting.dao;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import ru.com.energyaccounting.model.ExtinguisherType;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by UserSts on 29.05.2017.
 */
public class ExtinguisherTypeDao extends AbstractJDBCDao<ExtinguisherType, Integer> {
    public ExtinguisherTypeDao(Connection connection) {
        super(connection);
    }

    @Override
    public ExtinguisherType create() throws PersistException {
        ExtinguisherType e = new ExtinguisherType();
        return persist(e);
    }

    @Override
    public String getSelectQuery() {
        return "SELECT * FROM extinguisher_types";
    }

    @Override
    public String getCreateQuery() {
        return "INSERT INTO extinguisher_types (name, description) VALUES (?,?)";
    }

    @Override
    public String getUpdateQuery() {
        return "UPDATE extinguisher_types SET name = ?, description = ? WHERE id = ?";
    }

    @Override
    public String getDeleteQuery() {
        return "DELETE FROM extinguisher_types WHERE id = ?";
    }

    @Override
    protected ObservableList<ExtinguisherType> parseResultSet(ResultSet rs) throws PersistException {
        ObservableList<ExtinguisherType> result = FXCollections.observableArrayList();
        try {
            while (rs.next()){
                ExtinguisherType e = new ExtinguisherType();
                e.setId(rs.getInt("id"));
                e.setName(rs.getString("name"));
                e.setDescription(rs.getString("description"));
                result.add(e);
            }
        }catch (SQLException e){
            throw new PersistException(e);
        }

        return result;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement ps, ExtinguisherType extinguisherType) throws PersistException {
        try {
            ps.setString(1, extinguisherType.getName());
            ps.setString(2, extinguisherType.getDescription());
        }catch (SQLException e){
            throw new PersistException(e);
        }
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement ps, ExtinguisherType extinguisherType) throws PersistException {
        try {
            ps.setString(1, extinguisherType.getName());
            ps.setString(2, extinguisherType.getDescription());
            ps.setInt(3, extinguisherType.getId());
        }catch (SQLException e){
            throw new PersistException(e);
        }
    }

    @Override
    protected void prepareStatementForDelete(PreparedStatement ps, ExtinguisherType extinguisherType) throws PersistException {

    }
}
