package ru.com.energyaccounting.dao;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import ru.com.energyaccounting.model.ElectricalEquipmentDoc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;


public class ElectricalEquipmentDocDao extends AbstractJDBCDao<ElectricalEquipmentDoc, Integer> {
    public ElectricalEquipmentDocDao(Connection connection) {
        super(connection);
    }

    @Override
    public String getSelectQuery() {
        return "SELECT * FROM electrical_equipment_docs";
    }

    @Override
    public String getCreateQuery() {
        return "INSERT INTO electrical_equipment_docs (electrical_equipment_date_time, description) VALUES (?, ?)";
    }

    @Override
    public String getUpdateQuery() {
        return "UPDATE electrical_equipment_docs SET electrical_equipment_date_time = ?, description = ? WHERE id = ?";
    }

    @Override
    public String getDeleteQuery() {
        return "DELETE FROM electrical_equipment_docs WHERE id = ?;";
    }

    @Override
    protected ObservableList<ElectricalEquipmentDoc> parseResultSet(ResultSet rs) throws PersistException {
        ObservableList<ElectricalEquipmentDoc> result = FXCollections.observableArrayList();
        try {
            while (rs.next()){
                ElectricalEquipmentDoc electricalEquipmentDoc = new ElectricalEquipmentDoc();
                electricalEquipmentDoc.setId(rs.getInt("id"));
                try {
                    electricalEquipmentDoc.setDocDateTime(LocalDateTime.parse(rs.getString("electrical_equipment_date_time"), dtfDateTime));

                }catch (DateTimeParseException e){
                    electricalEquipmentDoc.setDocDateTime(LocalDateTime.of(01,01,1,00,00));
                }
                electricalEquipmentDoc.setDescription(rs.getString("description"));

                result.add(electricalEquipmentDoc);
            }
        }catch (SQLException e){
            e.printStackTrace();
            throw new PersistException(e);
        }

        return result;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement ps, ElectricalEquipmentDoc electricalEquipmentDoc) throws PersistException {
        try {
            if (electricalEquipmentDoc.getDocDateTime() != null) {
                String dateTime = electricalEquipmentDoc.getDocDateTime().format(dtfDateTime);
                ps.setString(1, dateTime);
            }
            ps.setString(2, electricalEquipmentDoc.getDescription());



        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement ps, ElectricalEquipmentDoc electricalEquipmentDoc) throws PersistException {
        try {
            if (electricalEquipmentDoc.getDocDateTime() != null) {
                String dateTime = electricalEquipmentDoc.getDocDateTime().format(dtfDateTime);
                ps.setString(1, dateTime);
            }
            ps.setString(2, electricalEquipmentDoc.getDescription());
            ps.setInt(3, electricalEquipmentDoc.getId());

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void prepareStatementForDelete(PreparedStatement ps, ElectricalEquipmentDoc electricalEquipmentDoc) throws PersistException {

    }
}
