package ru.com.energyaccounting.dao;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import ru.com.energyaccounting.model.Organization;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by UserSts on 20.05.2017.
 */
public class OrganizationDao extends AbstractJDBCDao<Organization, Integer> {

    public OrganizationDao(Connection connection) {
        super(connection);
    }

    @Override
    public Organization create() throws PersistException {
        Organization o = new Organization();
        return persist(o);
    }

    @Override
    public String getSelectQuery() {
        return "SELECT * FROM organizations";
    }

    @Override
    public String getCreateQuery() {
        return "INSERT INTO organizations (name, description) VALUES (?, ?)";
    }

    @Override
    public String getUpdateQuery() {
        return "UPDATE organizations SET name = ?, description = ? WHERE id = ?";
    }

    @Override
    public String getDeleteQuery() {
        return "DELETE FROM organizations WHERE id = ?";
    }

    @Override
    protected ObservableList<Organization> parseResultSet(ResultSet rs) throws PersistException {
        ObservableList<Organization> result = FXCollections.observableArrayList();
        try {
            while (rs.next()){
                Organization o = new Organization();
                o.setId(rs.getInt("id"));
                o.setName(rs.getString("name"));
                o.setDescription(rs.getString("description"));
                result.add(o);
            }
        }catch (SQLException e){
            throw new PersistException(e);
        }
        return result;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement ps, Organization organization) throws PersistException {
        try {
            ps.setString(1, organization.getName());
            ps.setString(2, organization.getDescription());

        }catch (SQLException e){
            throw new PersistException(e);
        }
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement ps, Organization organization) throws PersistException {
        try {
            ps.setString(1, organization.getName());
            ps.setString(2, organization.getDescription());
            ps.setInt(3, organization.getId());

        }catch (SQLException e){
            throw new PersistException(e);
        }
    }

    @Override
    protected void prepareStatementForDelete(PreparedStatement ps, Organization organization) throws PersistException {

    }
}
