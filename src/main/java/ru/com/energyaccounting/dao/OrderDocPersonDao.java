package ru.com.energyaccounting.dao;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import ru.com.energyaccounting.model.OrderDocPerson;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by UserSts on 02.08.2017.
 */
public class OrderDocPersonDao extends AbstractJDBCDao<OrderDocPerson, Integer>{
    public OrderDocPersonDao(Connection connection) {
        super(connection);
    }

    @Override
    public String getSelectQuery() {
        return "SELECT * FROM order_doc_person_view";
    }

    @Override
    public String getCreateQuery() {
        return "INSERT INTO order_doc_person (order_doc_id, status_id, person_id, additional) VALUES (?,?,?,?);";
    }

    @Override
    public String getUpdateQuery() {
        return " UPDATE order_doc_person SET order_doc_id = ?, status_id = ?, person_id = ?, additional = ? WHERE id = ?";
    }

    @Override
    public String getDeleteQuery() {
        return "DELETE FROM order_doc_person WHERE id = ?;";
    }

    @Override
    protected ObservableList<OrderDocPerson> parseResultSet(ResultSet rs) throws PersistException {
        ObservableList<OrderDocPerson> result = FXCollections.observableArrayList();
        try {
            while (rs.next()){
                OrderDocPerson o = new OrderDocPerson();
                o.setId(rs.getInt("od_person_numb"));
                o.setOrderDocId(rs.getInt("doc_id")); //костыль, не трогать - так необходимо чтобы AbstractJDBCDAo.getOLByPK отработал, так как там WHERE doc_id = ?,
                o.setPersonId(rs.getInt("person_id"));
                o.setPerson(rs.getString("person"));
                o.setAdditional(rs.getString("additional"));
                o.setStatusId(rs.getInt("status_id"));
                o.setStatus(rs.getString("status"));

                result.add(o);
            }
        }catch (SQLException e){
            e.getStackTrace();
            throw new PersistException(e);
        }

        return result;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement ps, OrderDocPerson orderDocPerson) throws PersistException {
        try{
            ps.setInt(1, orderDocPerson.getOrderDocId());
            ps.setInt(2, orderDocPerson.getStatusId());
            ps.setInt(3, orderDocPerson.getPersonId());
            ps.setString(4, orderDocPerson.getAdditional());

        }catch (SQLException e){
            throw new PersistException(e);
        }
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement ps, OrderDocPerson orderDocPerson) throws PersistException {
        try {
            ps.setInt(1, orderDocPerson.getOrderDocId());
            ps.setInt(2, orderDocPerson.getStatusId());
            ps.setInt(3, orderDocPerson.getPersonId());
            ps.setString(4, orderDocPerson.getAdditional());
            ps.setInt(5, orderDocPerson.getId());
        }catch (SQLException e){
            throw new PersistException(e);
        }
    }

    @Override
    protected void prepareStatementForDelete(PreparedStatement ps, OrderDocPerson object) throws PersistException {

    }
}
