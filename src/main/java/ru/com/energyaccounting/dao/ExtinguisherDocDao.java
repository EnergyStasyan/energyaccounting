package ru.com.energyaccounting.dao;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import ru.com.energyaccounting.model.ExtinguisherDoc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;

public class ExtinguisherDocDao extends AbstractJDBCDao<ExtinguisherDoc, Integer> {
    public ExtinguisherDocDao(Connection connection) {
        super(connection);
    }

    @Override
    public String getSelectQuery() {
        return "SELECT * FROM extinguisher_docs";
    }

    @Override
    public String getCreateQuery() {
        return "INSERT INTO extinguisher_docs (extinguisher_doc_date_time, description) VALUES (?, ?)";
    }

    @Override
    public String getUpdateQuery() {
        return "UPDATE extinguisher_docs SET extinguisher_doc_date_time = ?, description = ? WHERE id = ?";
    }

    @Override
    public String getDeleteQuery() {
        return "DELETE FROM extinguisher_docs WHERE id = ?";
    }

    @Override
    protected ObservableList<ExtinguisherDoc> parseResultSet(ResultSet rs) throws PersistException {
        ObservableList<ExtinguisherDoc> result = FXCollections.observableArrayList();
        try {
            while (rs.next()){
                ExtinguisherDoc extinguisherDoc = new ExtinguisherDoc();
                extinguisherDoc.setId(rs.getInt("id"));
                try {
                    extinguisherDoc.setDocDateTime(LocalDateTime.parse(rs.getString("extinguisher_doc_date_time"), dtfDateTime));

                }catch (DateTimeParseException e){
                    extinguisherDoc.setDocDateTime(LocalDateTime.of(01,01,1,00,00));
                }
                extinguisherDoc.setDescription(rs.getString("description"));

                result.add(extinguisherDoc);
            }
        }catch (SQLException e){
            e.printStackTrace();
            throw new PersistException(e);
        }

        return result;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement ps, ExtinguisherDoc extinguisherDoc) throws PersistException {
        try {
            if (extinguisherDoc.getDocDateTime() != null) {
                String dateTime = extinguisherDoc.getDocDateTime().format(dtfDateTime);
                ps.setString(1, dateTime);
            }
            ps.setString(2, extinguisherDoc.getDescription());
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement ps, ExtinguisherDoc extinguisherDoc) throws PersistException {
        try {
            if (extinguisherDoc.getDocDateTime() != null) {
                String dateTime = extinguisherDoc.getDocDateTime().format(dtfDateTime);
                ps.setString(1, dateTime);
            }
            ps.setString(2, extinguisherDoc.getDescription());
            ps.setInt(3, extinguisherDoc.getId());

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void prepareStatementForDelete(PreparedStatement ps, ExtinguisherDoc object) throws PersistException {

    }
}
