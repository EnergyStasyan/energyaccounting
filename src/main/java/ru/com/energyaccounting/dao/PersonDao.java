package ru.com.energyaccounting.dao;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import ru.com.energyaccounting.model.Person;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;

/**
 * Created by UserSts on 02.05.2017.
 */
public class PersonDao extends AbstractJDBCDao<Person, Integer> {

    //Данная строка нужна для парсинга из SQLite(String -> Date), так как в SQLite нет типа ДАТА
    //DateTimeFormatter dtfDate = DateTimeFormatter.ISO_LOCAL_DATE;

    public PersonDao(Connection connection) {
        super(connection);
    }



    @Override
    public Person create() throws PersistException {
        Person p = new Person();
        return persist(p);
    }

    @Override
    public String getSelectQuery() {
        return "SELECT * FROM persons_view;";
    }

    @Override
    public String getCreateQuery() {
        return "INSERT INTO persons (personnel_number,surname,name,middle_name,phone,birthday_date,hire_date,dismissal_date,subdiv_id,salary,position_id) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
    }

    @Override
    public String getUpdateQuery() {
        return "UPDATE persons SET personnel_number = ?,surname = ?,name = ?,middle_name = ?,phone = ?,birthday_date = ?,hire_date = ?,dismissal_date = ?,subdiv_id = ?, salary = ?, position_id = ? WHERE id = ?";
    }

    @Override
    public String getDeleteQuery() {
        return "DELETE FROM persons WHERE id = ?";
    }

    @Override
    protected ObservableList<Person> parseResultSet(ResultSet rs) throws PersistException {
        ObservableList<Person> result = FXCollections.observableArrayList();
        try {
            while (rs.next()){
               Person p = new Person();
               p.setId(rs.getInt("id"));
               p.setPersonId(rs.getInt("personnel_number"));
               p.setSurname(rs.getString("surname"));
               p.setMiddleName(rs.getString("middle_name"));
               p.setName(rs.getString("name"));

                /*
                Если дата не пустая мы ее парсим
                если дата не соответсвтует формату, то мы ее обнуляем
                */
                try {
                   p.setBirthdayDate(LocalDate.parse(rs.getString("birthday_date"), dtfDate));
               }catch (NullPointerException |DateTimeParseException e){
                   p.setBirthdayDate(null);
                   log.warning("Ошибка парсинга даты: " + e);
               }try {
                   p.setHireDate(LocalDate.parse(rs.getString("hire_date"), dtfDate));
               }catch (NullPointerException |DateTimeParseException e){
                   p.setHireDate(null);
                   log.warning("Ошибка парсинга даты: " + e);
               }try {
                   p.setDismissalDate(LocalDate.parse(rs.getString("dismissal_date"), dtfDate));
               }catch (NullPointerException |DateTimeParseException e){
                   p.setDismissalDate(null);
                   log.warning("Ошибка парсинга даты: " + e);
               }
               try {
                   p.setNextTestDate(LocalDate.parse(rs.getString("next_test_date"), dtfDate));
               }catch (NullPointerException |DateTimeParseException e){
                   p.setNextTestDate(null);
                   log.warning("Ошибка парсинга даты: " + e);
               }

               p.setPhone(rs.getString("phone"));
               p.setSalary(rs.getFloat("salary"));
               p.setSubdivision(rs.getString("subdivisions"));
               p.setSubdiv_id(rs.getInt("subdiv_id"));
               p.setPosition(rs.getString("position"));
               p.setPosition_id(rs.getInt("position_id"));
               p.setAccessGroup(rs.getString("access_group"));
               result.add(p);
            }

        }catch (SQLException e){
            throw new PersistException(e);
        }
        return result;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement ps, Person person) throws PersistException {
        try {
            ps.setInt(1, person.getPersonId());
            ps.setString(2, person.getSurname());
            ps.setString(3, person.getName());
            ps.setString(4, person.getMiddleName());
            ps.setString(5, person.getPhone());

            if (person.getBirthdayDate() !=null){
                ps.setString(6, (person.getBirthdayDate().toString()));
            }
            if (person.getHireDate() !=null){
                ps.setString(7, (person.getHireDate().toString()));
            }
            if (person.getDismissalDate() != null){
                ps.setString(8, (person.getDismissalDate().toString()));
            }
            ps.setInt(9, person.getSubdiv_id());
            ps.setFloat(10, person.getSalary());
            ps.setInt(11, person.getPosition_id());

            if (person.getId() != 0) { //используется в том случае, когда редактируем сотрудника
                ps.setInt(12, person.getId());
            }
        }catch (SQLException e){
            throw new PersistException(e);
        }
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement ps, Person person) throws PersistException {
        try {
            ps.setInt(1, person.getPersonId());
            ps.setString(2, person.getSurname());
            ps.setString(3, person.getName());
            ps.setString(4, person.getMiddleName());
            ps.setString(5, person.getPhone());

            if (person.getBirthdayDate() !=null){
                ps.setString(6, (person.getBirthdayDate().toString()));
            }
            if (person.getHireDate() !=null){
                ps.setString(7, (person.getHireDate().toString()));
            }
            if (person.getDismissalDate() != null){
                ps.setString(8, (person.getDismissalDate().toString()));
            }
            ps.setInt(9, person.getSubdiv_id());
            ps.setFloat(10, person.getSalary());
            ps.setInt(11, person.getPosition_id());
            if (person.getId() != 0) { //используется в том случае, когда редактируем сотрудника
                ps.setInt(12, person.getId());
            }
        }catch (SQLException e){
            throw new PersistException(e);
        }
    }

    @Override
    protected void prepareStatementForDelete(PreparedStatement ps, Person object) throws PersistException {

    }
}
