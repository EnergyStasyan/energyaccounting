package ru.com.energyaccounting.dao;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import ru.com.energyaccounting.model.VoltageType;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by UserSts on 30.05.2017.
 */
public class VoltageTypeDao extends AbstractJDBCDao<VoltageType, Integer>{

    public VoltageTypeDao(Connection connection) {
        super(connection);
    }

    @Override
    public VoltageType create() throws PersistException {
        VoltageType v = new VoltageType();
        return persist(v);
    }

    @Override
    public String getSelectQuery() {
        return "SELECT * FROM voltage_types;";
    }

    @Override
    public String getCreateQuery() {
        return "INSERT INTO voltage_types (name, description) VALUES (?, ?)";
    }

    @Override
    public String getUpdateQuery() {
        return "UPDATE voltage_types SET name = ?, description = ? WHERE id = ?";
    }

    @Override
    public String getDeleteQuery() {
        return "DELETE FROM voltage_types WHERE id = ?";
    }

    @Override
    protected ObservableList<VoltageType> parseResultSet(ResultSet rs) throws PersistException {
        ObservableList<VoltageType> result = FXCollections.observableArrayList();
        try {
            while (rs.next()) {
                VoltageType v = new VoltageType();
                v.setId(rs.getInt("id"));
                v.setName(rs.getString("name"));
                v.setDescription(rs.getString("description"));
                result.add(v);
            }
        }catch (SQLException e){
            throw new PersistException(e);
        }
        return result;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement ps, VoltageType voltageType) throws PersistException {
        try {
            ps.setString(1, voltageType.getName());
            ps.setString(2, voltageType.getDescription());

        }catch (SQLException e){
            throw new PersistException(e);
        }
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement ps, VoltageType voltageType) throws PersistException {
        try {
            ps.setString(1, voltageType.getName());
            ps.setString(2, voltageType.getDescription());
            ps.setInt(3, voltageType.getId());

        }catch (SQLException e){
            throw new PersistException(e);
        }
    }

    @Override
    protected void prepareStatementForDelete(PreparedStatement ps, VoltageType voltageType) throws PersistException {

    }
}
