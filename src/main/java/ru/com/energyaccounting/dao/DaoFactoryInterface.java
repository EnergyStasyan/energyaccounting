package ru.com.energyaccounting.dao;

/**
 * Created by UserSts on 02.05.2017.
 */
public interface DaoFactoryInterface<C> {

    /*
    необходимо заменить и удалить getContext
     */

    C getContext() throws PersistException;

    GenericDao getDao(C context, Class dtoClass) throws PersistException;

    interface DaoCreator<C>{
        GenericDao create(C context);
    }


}
