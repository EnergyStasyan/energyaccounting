package ru.com.energyaccounting.dao;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import ru.com.energyaccounting.model.ElectricalEquipmentDocElectricalEquipment;
import ru.com.energyaccounting.model.IpmDocIpm;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.DateTimeException;
import java.time.LocalDate;

public class ElectricalEquipmentDocElectricalEquipmentDao extends AbstractJDBCDao<ElectricalEquipmentDocElectricalEquipment, Integer> {

    public ElectricalEquipmentDocElectricalEquipmentDao(Connection connection) {
        super(connection);
    }

    @Override
    public String getSelectQuery() {
        return "SELECT * FROM electrical_equipment_doc_electrical_equipment_view";
    }
//electrical_equipment_doc_electrical_equipment_view
    @Override
    public String getCreateQuery() {
        return " INSERT INTO electrical_equipment_doc_electrical_equipment (electrical_equipment_doc_id, electrical_equipment_id, last_test, next_test, status, description ) VALUES ( ?, ?, ?, ?, ?, ?);";
    }

    @Override
    public String getUpdateQuery() {
        return "UPDATE electrical_equipment_doc_electrical_equipment SET electrical_equipment_doc_id = ?, electrical_equipment_id = ?, last_test = ?, next_test = ?, status = ? ,description = ? WHERE id = ?";
    }

    @Override
    public String getDeleteQuery() {
        return "DELETE FROM electrical_equipment_doc_electrical_equipment WHERE id = ?;";
    }

    @Override
    protected ObservableList<ElectricalEquipmentDocElectricalEquipment> parseResultSet(ResultSet rs) throws PersistException {
        ObservableList<ElectricalEquipmentDocElectricalEquipment> result = FXCollections.observableArrayList();
        try {
            while (rs.next()) {
                ElectricalEquipmentDocElectricalEquipment eedee = new ElectricalEquipmentDocElectricalEquipment();
                eedee.setDoc_id(rs.getInt("doc_id"));
                eedee.setId(rs.getInt("id"));
                eedee.setElectricalEquipment_id(rs.getInt("electrical_equipment_id"));
                eedee.setName(rs.getString("name"));
                eedee.setInventoryNumber(rs.getString("inventory_number"));

                try {
                    eedee.setLastTestDate(LocalDate.parse(rs.getString("last_test")));
                } catch (NullPointerException | DateTimeException e) {
                    eedee.setLastTestDate(null);
                    log.warning("Ошибка парсинга даты последнего теста: " + e);
                }

                try {
                    eedee.setNextTestDate(LocalDate.parse(rs.getString("next_test")));
                } catch (NullPointerException | DateTimeException e) {
                    eedee.setNextTestDate(null);
                    log.warning("Ошибка парсинга даты следующего теста: " + e);
                }

                eedee.setStatus(rs.getString("status"));
                eedee.setDescription(rs.getString("description"));

                result.add(eedee);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }


        return result;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement ps, ElectricalEquipmentDocElectricalEquipment docElectricalEquipment) throws PersistException {
        try {
            ps.setInt(1, docElectricalEquipment.getDoc_id());
            ps.setInt(2, docElectricalEquipment.getElectricalEquipment_id());

            if (docElectricalEquipment.getLastTestDate() != null) {
                ps.setString(3, docElectricalEquipment.getLastTestDate().toString());
            }
            if (docElectricalEquipment.getNextTestDate() != null) {
                ps.setString(4, docElectricalEquipment.getNextTestDate().toString());
            }
            ps.setString(5, docElectricalEquipment.getStatus());
            ps.setString(6, docElectricalEquipment.getDescription());


        } catch (SQLException e) {
            throw new PersistException(e);
        }
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement ps, ElectricalEquipmentDocElectricalEquipment docElectricalEquipment) throws PersistException {
        try {
            ps.setInt(1, docElectricalEquipment.getDoc_id());
            ps.setInt(2, docElectricalEquipment.getElectricalEquipment_id());

            if (docElectricalEquipment.getLastTestDate() != null) {
                ps.setString(3, docElectricalEquipment.getLastTestDate().toString());
            }
            if (docElectricalEquipment.getNextTestDate() != null) {
                ps.setString(4, docElectricalEquipment.getNextTestDate().toString());
            }
            ps.setString(5, docElectricalEquipment.getStatus());
            ps.setString(6, docElectricalEquipment.getDescription());
            ps.setInt(7, docElectricalEquipment.getId());


        } catch (SQLException e) {
            throw new PersistException(e);
        }
    }

    @Override
    protected void prepareStatementForDelete(PreparedStatement ps, ElectricalEquipmentDocElectricalEquipment docElectricalEquipment) throws PersistException {

    }
}
