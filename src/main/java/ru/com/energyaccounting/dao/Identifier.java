package ru.com.energyaccounting.dao;

import java.io.Serializable;

/**
 * Created by UserSts on 02.05.2017.
 */
public interface Identifier<PK extends Serializable> {
    PK getId();
    String getDialogStageTitle();
}
