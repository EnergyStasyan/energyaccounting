package ru.com.energyaccounting.dao;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import ru.com.energyaccounting.model.MainFrame;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;

/**
 * Created by UserSts on 01.06.2017.
 */
public class MainFrameDao extends AbstractJDBCDao<MainFrame, Integer> {

    public MainFrameDao(Connection connection) {
        super(connection);
    }


    @Override
    public String getSelectQuery() {
        return "SELECT * FROM mf_view";
    }

    @Override
    public String getCreateQuery() {
        return null;
    }

    @Override
    public String getUpdateQuery() {
        return null;
    }

    @Override
    public String getDeleteQuery() {
        return null;
    }


    protected ObservableList<MainFrame> parseResultSet(ResultSet rs, String who) throws PersistException {
        ObservableList<MainFrame> result = FXCollections.observableArrayList();
        try {
            while (rs.next()){
                MainFrame m = new MainFrame();
                switch (who) {  // FIXME: 02.06.2017 Сделать через ENUM
                    case "person":
                        m.setNameSurname(rs.getString("name"));
                        try {
                            m.setBirthday(LocalDate.parse(rs.getString("birthday"), dtfDate));
                        } catch (NullPointerException | DateTimeParseException e) {
                            m.setBirthday(null);
                        }
                        break;
                    case "IPM":
                        m.setEiIPM(rs.getString("eiIPM"));
                        m.setIPMType(rs.getString("IPMType"));
                        try {
                            m.setNextTestDateIPM(LocalDate.parse(rs.getString("nextTestDateIpm"), dtfDate));
                        } catch (NullPointerException | DateTimeParseException e) {
                            m.setNextTestDateIPM(null);
                        }
                        break;
                    case "extinguisher":
                        m.setEiExt(rs.getString("eiExt"));
                        m.setExtType(rs.getString("extType"));
                        try {
                            m.setNextTestDateExt(LocalDate.parse(rs.getString("dateExt"), dtfDate));
                        } catch (NullPointerException | DateTimeParseException e) {
                            m.setNextTestDateExt(null);
                        }
                        break;
                    default:
                        throw new PersistException("Ошибка парсинга:" );
                }
                    result.add(m);


            }
        }catch (SQLException e){
            throw new PersistException(e);
        }
        return result;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement ps, MainFrame object) throws PersistException {

    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement ps, MainFrame object) throws PersistException {

    }

    @Override
    protected void prepareStatementForDelete(PreparedStatement ps, MainFrame object) throws PersistException {

    }

    @Override
    protected ObservableList<MainFrame> parseResultSet(ResultSet rs) throws PersistException {
        ObservableList<MainFrame> result = FXCollections.observableArrayList();
        try {
            while (rs.next()){
                MainFrame mf = new MainFrame();
                try {
                    mf.setDate(LocalDate.parse(rs.getString("dates"), dtfDate));
                }catch (NullPointerException | DateTimeParseException e){
                    mf.setDate(null);
                    log.info("Ошибка парсинга даты: " + e);
                }
                mf.setType(rs.getString("type"));
                mf.setDescription(rs.getString("description"));
                result.add(mf);
            }

        }catch (SQLException e){
            throw new PersistException(e);
        }

        return result;
    }
}
