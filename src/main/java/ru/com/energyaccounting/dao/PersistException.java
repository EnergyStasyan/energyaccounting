package ru.com.energyaccounting.dao;

import ru.com.energyaccounting.controller.ServiceMessagesController;

/**
 * Created by UserSts on 02.05.2017.
 */
public class PersistException extends Exception {
    public PersistException() {}

    public PersistException(String message) {
        super(message);
        ServiceMessagesController.addErrorServiceMessage(message);
    }

    public PersistException(String message, Throwable cause) {
        super(message, cause);
    }

    public PersistException(Throwable cause) {
        super(cause);
        ServiceMessagesController.addErrorServiceMessage(cause.getMessage());
    }

    public PersistException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
