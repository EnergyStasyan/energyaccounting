package ru.com.energyaccounting.dao;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import ru.com.energyaccounting.model.Shift;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by UserSts on 30.05.2017.
 */
public class ShiftDao extends AbstractJDBCDao<Shift, Integer> {

    public ShiftDao(Connection connection) {
        super(connection);
    }

    @Override
    public Shift create() throws PersistException {
        Shift s = new Shift();
        return persist(s);
    }

    @Override
    public String getSelectQuery() {
        return "SELECT * FROM shifts;";
    }

    @Override
    public String getCreateQuery() {
        return "INSERT INTO shifts (name, description) VALUES (?, ?)";
    }

    @Override
    public String getUpdateQuery() {
        return "UPDATE shifts SET name = ?, description = ? WHERE id = ?";
    }

    @Override
    public String getDeleteQuery() {
        return "DELETE FROM shifts WHERE id = ?";
    }

    @Override
    protected ObservableList<Shift> parseResultSet(ResultSet rs) throws PersistException {
       ObservableList<Shift> result = FXCollections.observableArrayList();
       try {
           while (rs.next()){
               Shift s = new Shift();
               s.setId(rs.getInt("id"));
               s.setName(rs.getString("name"));
               s.setDescription(rs.getString("description"));
               result.add(s);
           }
       }catch (SQLException e){
           throw new PersistException(e);
       }
        return result;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement ps, Shift shift) throws PersistException {
        try {
            ps.setString(1, shift.getName());
            ps.setString(2, shift.getDescription());

        }catch (SQLException e){
            throw new PersistException(e);
        }
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement ps, Shift shift) throws PersistException {
        try {
            ps.setString(1, shift.getName());
            ps.setString(2, shift.getDescription());
            ps.setInt(3, shift.getId());

        }catch (SQLException e){
            throw new PersistException(e);
        }
    }

    @Override
    protected void prepareStatementForDelete(PreparedStatement ps, Shift shift) throws PersistException {

    }
}
