package ru.com.energyaccounting.dao;

import javafx.collections.ObservableList;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.format.DateTimeFormatter;
import java.util.logging.Logger;

/**
 * Created by UserSts on 02.05.2017.
 */
public abstract class AbstractJDBCDao<T extends Identifier<PK>, PK extends Integer> implements GenericDao<T, PK> {

    public final static DateTimeFormatter dtfDate = DateTimeFormatter.ISO_LOCAL_DATE;
    public final static DateTimeFormatter dtfDateTime = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    public final static DateTimeFormatter dtfTime = DateTimeFormatter.ofPattern("HH:mm");
    static final Logger log = Logger.getLogger("AbstractJDBCDao");
    private Connection connection;

    public AbstractJDBCDao(Connection connection) {
        this.connection = connection;
    }

    /**
     * Возвращает sql запрос для получения всех записей.
     * <p/>
     * SELECT * FROM [Table]
     */
    public abstract String getSelectQuery();

    /**
     * Возвращает sql запрос для вставки новой записи в базу данных.
     * <p/>
     * INSERT INTO [Table] ([column, column, ...]) VALUES (?, ?, ...);
     */
    public abstract String getCreateQuery();

    /**
     * Возвращает sql запрос для обновления записи.
     * <p/>
     * UPDATE [Table] SET [column = ?, column = ?, ...] WHERE id = ?;
     */
    public abstract String getUpdateQuery();

    /**
     * Возвращает sql запрос для удаления записи из базы данных.
     * <p/>
     * DELETE FROM [Table] WHERE id= ?;
     */
    public abstract String getDeleteQuery();

    /**
     * Парсим в читаемый JavaFX формат List
     *
     * @param rs
     * @return
     * @throws PersistException
     */
    protected abstract ObservableList<T> parseResultSet(ResultSet rs) throws PersistException;

    protected abstract void prepareStatementForInsert(PreparedStatement ps, T object) throws PersistException;

    protected abstract void prepareStatementForUpdate(PreparedStatement ps, T object) throws PersistException;

    /**
     * данный метод не нужен в SQL, так как удаление просисходит через ic void delete(T object) throws PersistException
     * @param ps
     * @param object
     * @throws PersistException
     */
    protected abstract void prepareStatementForDelete(PreparedStatement ps, T object) throws PersistException;

    @Override
    public T create() throws PersistException {
        return null;
    }

    @Override
    public T persist(T object) throws PersistException {
        log.info("Сохраняем объект в BD: " + object.toString());
        if (object.getId() == null) {
            throw new PersistException("Object is already persist");
        }
        T persistInstance = null;
        String sql = getCreateQuery();
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            prepareStatementForInsert(ps, object);
            int count = ps.executeUpdate();
            if (count > 1) {
                throw new PersistException("On persist modify more then 1 record: " + count);
            }
        }catch (NullPointerException e){
            throw new PersistException("Невозможна запись в БД: пустой метод Persist");
        } catch (SQLException e) {
            throw new PersistException(e);
        }

        sql = getSelectQuery() + " WHERE id = last_insert_rowid();";
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ResultSet rs = ps.executeQuery();
            ObservableList<T> observableList = parseResultSet(rs);

            if (observableList != null && observableList.size() ==1) {
                persistInstance = observableList.iterator().next();
            }

        } catch (SQLException e) {
            throw new PersistException(e);
        }
        return persistInstance;
    }

    @Override
    public T getByPk(int key) throws PersistException {
        ObservableList<T> observableList;
        String sql = getSelectQuery();
        sql +=" WHERE id = ?";
        try(PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setInt(1, key);
            ResultSet rs = ps.executeQuery();
            observableList = parseResultSet(rs);
        }catch (NullPointerException e){
            throw new PersistException("Невозможно чтение из БД: пустой метод getByPK");
        }catch (SQLException e){
            throw new PersistException(e);
        }
        if (observableList == null ||observableList.size() == 0){
            return null;
        }
        if (observableList.size() > 1){
            throw new PersistException("Received more than one record");
        }
        return observableList.iterator().next();
    }

    @Override
    public void update(T object) throws PersistException {

        String sql = getUpdateQuery();
        try(PreparedStatement ps = connection.prepareStatement( sql)) {
            prepareStatementForUpdate(ps, object);
            int count = ps.executeUpdate();
            if (count != 1) {
                throw new PersistException("On update modify more then 1 record: " + count);
            }
        }catch (NullPointerException e){
            throw new PersistException("Невозможна запись в БД: пустой метод Update");
        }catch (SQLException  e){
            throw new PersistException(e);
        }
    }

    @Override
    public void delete(T object) throws PersistException {
        log.info("Удаляем объект");
        String sql = getDeleteQuery();
        try(PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setObject(1, object.getId());
            // FIXME: 28.02.2018 нет каскадного удаления!!!!

            int count = ps.executeUpdate();
            if (count != 1){
                throw new PersistException("On delete modify more then 1 record: " + count);
            }
        }catch (SQLException e){
            throw new PersistException(e);
        }
    }

    @Override
    public ObservableList<T> getOLByPk(int key) throws PersistException {
        ObservableList<T> result;
        String sql = getSelectQuery();
        sql +=" WHERE doc_id = ?";
        try(PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setInt(1, key);
            ResultSet rs = ps.executeQuery();
            result = parseResultSet(rs);
        }catch (NullPointerException e){
            throw new PersistException("Невозможно чтение из БД: пустой метод getOLByPk");
        }catch (SQLException e){
            throw new PersistException(e);
        }
        return result;
    }

    @Override
    public ObservableList<T> getAll() throws PersistException {
        log.info("Пытаемся получить объект");
        /* TODO: 02.05.2017  добавить CachedRowSetImpl и проверить как оно работает
            CachedRowSetImpl crs = null;
            stmt = conn.createStatement();
            resultSet = stmt.executeQuery(queryStmt);
            crs = new CachedRowSetImpl();
            crs.populate(resultSet);
        */

        ObservableList<T> observableList;
        String sql = getSelectQuery();
        try(PreparedStatement ps = connection.prepareStatement(sql)) {
            ResultSet rs = ps.executeQuery();
            observableList = parseResultSet(rs);

            log.info("ResultSet: " + observableList.size());
        }catch (NullPointerException e){
            throw new PersistException("Невозможно чтение БД: пустой метод getAll");
        }catch (SQLException e){
            throw new PersistException(e);
        }


        return observableList;
    }

    @Override
    public ObservableList<T> getAll(Connection connection) throws PersistException {
        log.info("Пытаемся получить объект");

        ObservableList<T> observableList;
        String sql = getSelectQuery();
        try(PreparedStatement ps = connection.prepareStatement(sql)) {
            ResultSet rs = ps.executeQuery();
            observableList = parseResultSet(rs);

            log.info("ResultSet: " + observableList.size());
        }catch (NullPointerException e){
            throw new PersistException("Невозможно чтение БД: пустой метод getAll");
        }catch (SQLException e){
            throw new PersistException(e);
        }


        return observableList;
    }

    @Override
    public ObservableList<T> getAll(String sql, String who) {
        return null;
    }

    public Connection getConnection() {
        return connection;
    }
}
