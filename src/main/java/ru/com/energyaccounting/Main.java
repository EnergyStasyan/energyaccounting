package ru.com.energyaccounting;
/**
 * Created by UserSts on 05.02.2017.
 */

import javafx.application.Application;
import javafx.application.HostServices;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import ru.com.energyaccounting.controller.StartChoiceBaseController;
import ru.com.energyaccounting.dao.PersistException;

import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Logger;


public class Main extends Application {
    private static HostServices hostServices;

    public static final HashMap<String, String> fxmlHashMap = new HashMap<>();
    public static final String mStartChoiceBaseLayout = "fxml/StartChoiceBaseLayout.fxml";
    public static final String mStartChoicePropertyLayout = "fxml/StartChoicePropertyLayout.fxml";

    public static final String mRootLayout = "fxml/RootLayout.fxml";
    public static final String mSideMenuLayout = "fxml/SideMenuLayout.fxml";
    public static final String mBlankLayout = "fxml/BlankLayout.fxml";
    public static final String mServiceMessageLayout = "fxml/ServiceMessageLayout.fxml";
    public static final String mServiceMessagesMenuLayout = "fxml/ServiceMessageMenuLayout.fxml";

    public static final String mMainLayout = "fxml/MainFrameLayout.fxml";

    public static final String mPersonMenuLayout = "fxml/PersonMenuLayout.fxml";
    public static final String mPersonEditOrAddLayout = "fxml/PersonAddOrEditLayout.fxml";
    public static final String mSelectMenuLayout = "fxml/SelectMenuLayout.fxml"; //меню выбора

    public static final String mSubstationElInstallationElEquipmentTreeLayout = "fxml/SubstationElInstallationElEquipmentTreeLayout.fxml"; //меню выбора

    public static final String mElectricalInstallationsMenuLayout = "fxml/ElectricalInstallationMenuLayout.fxml";
    public static final String mElectricalInstallationsAddOrEditLayout = "fxml/ElectricalInstallationAddOrEditLayout.fxml";

    public static final String mElectricalEquipmentsMenuLayout = "fxml/ElectricalEquipmentMenuLayout.fxml";
    public static final String mElectricalEquipmentsAddOrEditLayout = "fxml/ElectricalEquipmentAddOrEditLayout.fxml";

    public static final String mElectricalEquipmentTypesMenuLayout = "fxml/ElectricalEquipmentTypesMenuLayout.fxml";
    public static final String mElectricalEquipmentTypesAddOrEditLayout = "fxml/ElectricalEquipmentTypesAddOrEditLayout.fxml";

    public static final String mSubdivisionsMenuLayout = "fxml/SubdivisionMenuLayout.fxml";
    public static final String mSubdivisionAddOrEditLayout = "fxml/SubdivisionAddOrEditLayout.fxml";

    public static final String mSubstationsMenuLayout = "fxml/SubstationMenuLayout.fxml";
    public static final String mSubstationAddOrEditLayout = "fxml/SubstationAddOrEditLayout.fxml";

    public static final String mOrganizationsMenuLayout = "fxml/OrganizationMenuLayout.fxml";
    public static final String mOrganizationAddOrEditLayout = "fxml/OrganizationAddOrEditLayout.fxml";

    public static final String mIpmTypesMenuLayout = "fxml/IPMTypeMenuLayout.fxml";
    public static final String mIpmTypeAddOrEditLayout = "fxml/IPMTypeAddOrEditLayout.fxml";

    public static final String mIpmsMenuLayout = "fxml/IPMMenuLayout.fxml";
    public static final String mIpmAddOrEditLayout = "fxml/IPMAddOrEditLayout.fxml";

    public static final String mExtinguisherTypeMenuLayout = "fxml/ExtinguisherTypeMenuLayout.fxml";
    public static final String mExtinguisherTypeAddOrEditLayout = "fxml/ExtinguisherTypeAddOrEditLayout.fxml";

    public static final String mExtinguisherMenuLayout = "fxml/ExtinguisherMenuLayout.fxml";
    public static final String mExtinguisherAddOrEditLayout = "fxml/ExtinguisherAddOrEditLayout.fxml";

    public static final String mShiftMenuLayout = "fxml/ShiftMenuLayout.fxml";
    public static final String mShiftAddOrEditLayout = "fxml/ShiftAddOrEditLayout.fxml";

    public static final String mVoltageTypeMenuLayout = "fxml/VoltageTypeMenuLayout.fxml";
    public static final String mVoltageTypeAddOrEditLayout = "fxml/VoltageTypeAddOrEditLayout.fxml";

    public static final String mPositionMenuLayout = "fxml/PositionMenuLayout.fxml";
    public static final String mPositionAddOrEditLayout = "fxml/PositionAddOrEditLayout.fxml";

    public static final String mOrderDocMenuLayout = "fxml/OrderDocMenuLayout.fxml";
    public static final String mOrderDocAddOrEditLayout = "fxml/OrderDocAddOrEditLayout.fxml";

    public static final String mAccessGroupMenuLayout = "fxml/AccessGroupMenuLayout.fxml";
    public static final String mAccessGroupAddOrEditLayout = "fxml/AccessGroupAddOrEditLayout.fxml";
    public static final String mIpmDocMenuLayout = "fxml/IpmDocMenuLayout.fxml";
    public static final String mElectricalEquipmentDocMenuLayout = "fxml/ElectricalEquipmentDocMenuLayout.fxml";
    public static final String mElectricalEquipmentDocAddOrEditLayout = "fxml/ElectricalEquipmentDocAddOrEditLayout.fxml";
    public static final String mIpmDocAddOrEditLayout = "fxml/IpmDocAddOrEditLayout.fxml";
    public static final String mExtinguisherDocMenuLayout = "fxml/ExtinguisherDocMenuLayout.fxml";
    public static final String mExtinguisherDocAddOrEditLayout = "fxml/ExtinguisherDocAddOrEditLayout.fxml";

    public static final String ico = "images/icons/earth_16.png";
    public static final String logo = "images/icons/earth_128.png";
    private final static Logger LOG = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(StartChoiceBaseController.class.getClassLoader().getResource(Main.mStartChoiceBaseLayout));
            AnchorPane page = loader.load();

            Stage dialogSEIEEStage = new Stage();
            dialogSEIEEStage.setTitle("Выбор базы");
            dialogSEIEEStage.initModality(Modality.APPLICATION_MODAL); // WINDOW_MODAL -> APPLICATION_MODAL иначе можно открывать неограниченное количество окон
            dialogSEIEEStage.initOwner(primaryStage); // баз данной строки окно исчезает
            dialogSEIEEStage.getIcons().add(new Image(Main.ico));
            Scene scene = new Scene(page);
            dialogSEIEEStage.setScene(scene);
            StartChoiceBaseController controller = loader.getController();

            dialogSEIEEStage.setOnHidden(event -> {
                controller.saveProperty();
                LOG.info("Настройки записаны");
            });

            controller.setPrimaryStage(dialogSEIEEStage);
            hostServices = getHostServices();

            dialogSEIEEStage.showAndWait();


        } catch (IOException e) {
            LOG.warning("Открытие окна невозможно " + e.getMessage());
            e.printStackTrace();
            throw new PersistException("Открытие окна невозможно " + e.getMessage());
        }
    }

    //уродлевое решение юзать статические поля
    public static HostServices getHostService() {
        return hostServices;
    }
}
