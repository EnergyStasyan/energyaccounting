package ru.com.energyaccounting.util;

import ru.com.energyaccounting.dao.DaoFactory;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Logger;

import static ru.com.energyaccounting.util.DBUtilStatic.loadText;

public class DBUtilAuditStatic {
    public static final String bdName = "EA-audit.db";
    private static final Logger LOG = Logger.getLogger(DBUtilAuditStatic.class.getName());
    private static Connection conn = null;

    public static Connection dbConnect (boolean isReturn) {
        Connection result = null;

        try {
            Class.forName(DaoFactory.JDBC_DRIVER);
            String basePath = AddressDBSingleton.getInstance().getStartChoice().getBasePath() + bdName;
            File file = new File(basePath);
            conn = DriverManager.getConnection(DaoFactory.connStr + basePath);
            if (isReturn){
                result = conn;
            }
            if (file.length() == 0) {
                LOG.info(bdName + " не существует");
                createSqlDB(conn);
            }
        }catch (Exception e){
            LOG.info("connection failed\n" + e.getMessage() +"\n"+ e);
        }
        return result;
    }

    public static void dbDisconnect() {

        try {
            if (conn != null && !conn.isClosed()){
                conn.close();
            }
        }catch (SQLException e){
            LOG.info("cannot close connection:"+ e.getMessage()+"\n"+ e);
        }
    }

    //создание шаблона базы данных
    public static void createSqlDB(Connection conn) throws SQLException {

        LOG.info("try load file createAuditBase");
        String sqlCreateDB = loadText("createAuditBase.sql");

        Statement stmt = null;
        try {

            stmt = conn.createStatement();
            stmt.executeUpdate(sqlCreateDB);
            LOG.info("Таблицы БД createAuditBase созданы");

        }catch (Exception e){
            e.printStackTrace();
            LOG.warning(e.getMessage());
        }finally {
            if (stmt != null){
                stmt.close();
            }
        }
    }

}
