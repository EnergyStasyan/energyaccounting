package ru.com.energyaccounting.util;

import ru.com.energyaccounting.model.StartChoice;

public class AddressDBSingleton {

    private static AddressDBSingleton instance;
    private StartChoice startChoice;
    private AddressDBSingleton() {}

    public static AddressDBSingleton getInstance(){
        if (instance == null) {
            instance = new AddressDBSingleton();
        }
        return instance;
    }

    public StartChoice getStartChoice() {
        return startChoice;
    }

    public void setStartChoice(StartChoice startChoice) {
        this.startChoice = startChoice;
    }
}
