package ru.com.energyaccounting.util;

import ru.com.energyaccounting.controller.StartChoiceBaseController;
import ru.com.energyaccounting.model.StartChoice;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;
import java.util.logging.Logger;

public class PropertyUtils {
    private final static String env = "env.properties";
    private final static Logger LOG = Logger.getLogger(PropertyUtils.class.getName());

    public static Properties readFromProperties(String propertyFile) {
        Properties properties = new Properties();
        try (InputStreamReader fis = new InputStreamReader(new FileInputStream(propertyFile), "cp1251")) {
            properties.load(fis);

        } catch (IOException e) {
            LOG.info("Файл " + propertyFile + " отсутствует");
        } catch (NullPointerException e) {
            LOG.info("Файл " + propertyFile + " пустой или невозможно прочитать, ошибка: " + e.getMessage());
        } catch (Exception e) {
            LOG.info("Неизвестная ошибка");
            e.printStackTrace();
        }
        return properties;
    }

    public static Properties readFromResourceProperties() {
        Properties properties = new Properties();
        try (
                InputStream is = PropertyUtils.class.getClassLoader().getResourceAsStream(env);
                InputStreamReader fis = new InputStreamReader(is)
        ) {
            properties.load(fis);
        } catch (IOException e) {
            LOG.info("Файл " + env + " отсутствует");
        } catch (NullPointerException e) {
            LOG.info("Файл " + env + " пустой или невозможно прочитать, ошибка: " + e.getMessage());
        } catch (Exception e) {
            LOG.info("Неизвестная ошибка");
            e.printStackTrace();
        }
        return properties;
    }
}
