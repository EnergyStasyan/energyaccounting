package ru.com.energyaccounting.util;

import com.sun.rowset.CachedRowSetImpl;
import ru.com.energyaccounting.dao.DaoFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * Класс предназначен для вызова статичных методов, из-за отсутствия в данный момент альтернативы, регистрации ошибок и событий в БД и сервисном меню
 */
public class DBUtilStatic {
    public static final String bdName = "EA.db";
    private static final Logger LOG = Logger.getLogger(DBUtilStatic.class.getName());
    private static Connection conn = null;

    /**
     * устанавливаем соединение с базой, соединение возвращаем для PrepareStatement
     *
     * @return Connection
     */
    public static Connection dbConnect() {
        try {
            try {
                Class.forName(DaoFactory.JDBC_DRIVER);
            } catch (ClassNotFoundException e) {
                LOG.warning(DaoFactory.JDBC_DRIVER + "JDBC driver is missed");
                throw e;
            }
            LOG.info("driver is registered");

            try {
                String basePath = AddressDBSingleton.getInstance().getStartChoice().getFilePath();
                LOG.info("basePath: " + basePath);
                conn = DriverManager.getConnection(DaoFactory.connStr + basePath);
            } catch (SQLException e) {
                LOG.warning(DaoFactory.connStr + "connection failed");
                throw e;
            }
        } catch (Exception e) {/*NOP*/}
        return conn;
    }


    public static void dbDisconnect() {
        try {
            if (conn != null && !conn.isClosed()) {
                conn.close();
            }
        } catch (SQLException e) {
            LOG.info("cannot close connection");
            e.printStackTrace();
        }
    }

    /**
     * Выполняет запрос в базу SQL и возвращяет CachedRowSetImpl(кешированную ResultSet)
     *
     * @param queryStmt
     * @return CachedRowSetImpl
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public static ResultSet dbExecuteQuery(String queryStmt) throws SQLException, ClassNotFoundException {
        Statement stmt = null;
        ResultSet resultSet = null;
        CachedRowSetImpl crs = null;

        try {
            stmt = dbConnect().createStatement();
            LOG.info(queryStmt);

            resultSet = stmt.executeQuery(queryStmt);
            crs = new CachedRowSetImpl();
            crs.populate(resultSet);

        } catch (SQLException e) {
            LOG.info("Problem occurred at executeQuery operation : " + e);
            throw e;
        } catch (Exception e) {
            LOG.info("неизвестная ошибка dbExecuteQuery");

        } finally {
            if (resultSet != null) {
                resultSet.close();
            }
            if (stmt != null) {
                stmt.close();
            }
            dbDisconnect();
        }
        return crs;
    }

    private static String getMigrationHistoryQuery() {
        return "INSERT INTO migration_history ( major_version, minor_version, file_number, comment, date_applied) VALUES(?,?,?,?,?)";
    }

    public static void createRecordAtMigrationHistory() {
        Properties properties = PropertyUtils.readFromResourceProperties();

        try (PreparedStatement ps = dbConnect().prepareStatement(getMigrationHistoryQuery())) {
            ps.setString(1, properties.getProperty("major.version"));
            ps.setString(2, properties.getProperty("minor.version"));
            ps.setString(3, properties.getProperty("file.number"));
            ps.setString(4, properties.getProperty("comment"));
            ps.setString(5, LocalDateTime.now().toString());
            ps.executeUpdate();

        } catch (Exception e) {
            LOG.warning(e.getMessage());
        } finally {
            dbDisconnect();
        }
    }


    //создание шаблона базы данных
    public static void createSqlDB() throws SQLException {
        LOG.info("try load file createBase");
        String sqlCreateDB = loadText("createBase.sql");
        try (Statement stmt = dbConnect().createStatement()) {
            stmt.executeUpdate(sqlCreateDB);
            LOG.info("Таблицы БД созданы");
        } finally {
            dbDisconnect();
        }
    }

    protected static String loadText(String nameFile) {
        StringBuilder sb = new StringBuilder();
        try {
            InputStream is = DBUtilStatic.class.getClassLoader().getResourceAsStream(nameFile);
            BufferedReader br = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8));
            while (true) {
                String line = br.readLine();
                if (line == null) {
                    break;
                }
                sb.append(line).append("\n");
            }
            return sb.toString();
        } catch (IOException e) {
            LOG.warning(e.getMessage());
            e.printStackTrace();
            return "";
        }
    }

    public static void demoDataBD() throws SQLException {
        String sqlDemoDB = loadText("demoDatas.sql");
        try (Statement stmt = dbConnect().createStatement()) {
            stmt.executeUpdate(sqlDemoDB);
            LOG.info("Демо данные в БД добавлены");
        } finally {
            dbDisconnect();
        }
    }
}
